PGDMP     &    )                z            db_siakad_stak     12.6 (Ubuntu 12.6-1.pgdg16.04+1)    13.3 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    20664    db_siakad_stak    DATABASE     c   CREATE DATABASE db_siakad_stak WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE db_siakad_stak;
                postgres    false                        2615    20665    pmb    SCHEMA        CREATE SCHEMA pmb;
    DROP SCHEMA pmb;
                postgres    false            &           1255    20666 &   add_city(character varying, character)    FUNCTION     �   CREATE FUNCTION public.add_city(city character varying, state character) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
      INSERT INTO cities VALUES (city, state);
    END;
    $$;
 H   DROP FUNCTION public.add_city(city character varying, state character);
       public          postgres    false            '           1255    20667    sp_mahasiswa_aktif(numeric)    FUNCTION     �   CREATE FUNCTION public.sp_mahasiswa_aktif(nim numeric) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$
    BEGIN
		select 
		t1.nama_mhs,
		t1.nim
		from mahasiswa t1
		where t1.status in('Aktif','Cuti');
	END;
    $$;
 6   DROP FUNCTION public.sp_mahasiswa_aktif(nim numeric);
       public          postgres    false            �            1259    20668    admins_id_seq    SEQUENCE     s   CREATE SEQUENCE pmb.admins_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 !   DROP SEQUENCE pmb.admins_id_seq;
       pmb          postgres    false    7            �            1259    20670    admins    TABLE     j  CREATE TABLE pmb.admins (
    id bigint DEFAULT nextval('pmb.admins_id_seq'::regclass) NOT NULL,
    username character varying(100) NOT NULL,
    password character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    insert_time timestamp without time zone,
    update_time timestamp without time zone
);
    DROP TABLE pmb.admins;
       pmb         heap    postgres    false    203    7            �            1259    20675    data_keluarga_id_seq    SEQUENCE     z   CREATE SEQUENCE pmb.data_keluarga_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE pmb.data_keluarga_id_seq;
       pmb          postgres    false    7            �            1259    20677    data_keluarga    TABLE     N  CREATE TABLE pmb.data_keluarga (
    no_pendaftaran character varying(100) NOT NULL,
    nama_ayah character varying(100),
    nama_ibu character varying(100),
    umur_ayah integer DEFAULT 0,
    umur_ibu integer,
    alamat_ortu character varying(255),
    tlp_ortu character varying(20),
    pend_ayah character varying(30),
    pend_ibu character varying(30),
    pekerjaan_ayah character varying(30),
    pekerjaan_ibu character varying(30),
    agama_ayah character varying(20),
    agama_ibu character varying(20),
    nama_wali character varying(100),
    alamat_wali character varying(255),
    tlp_wali character varying(20),
    pekerjaan_wali character varying(30),
    hubungan_wali character varying(20),
    agama_wali character varying(20),
    nama_sk text,
    jk_sk character varying(100),
    umur_sk character varying(255),
    pend_sk character varying(255),
    agama_sk character varying(255),
    nama_pcr character varying(100),
    agama_pcr character varying(20),
    hub_pcr character varying(255),
    tanggapan_pcr character varying(255),
    thn_div character varying(20),
    alasan_div character varying(255),
    stat_tp integer,
    nama_tp character varying(100),
    thn_pcr character varying(20),
    tgl_insert date,
    tgl_update date,
    id bigint DEFAULT nextval('pmb.data_keluarga_id_seq'::regclass) NOT NULL
);
    DROP TABLE pmb.data_keluarga;
       pmb         heap    postgres    false    205    7            �            1259    20685    data_lain_id_seq    SEQUENCE     v   CREATE SEQUENCE pmb.data_lain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE pmb.data_lain_id_seq;
       pmb          postgres    false    7            �            1259    20687 	   data_lain    TABLE     k  CREATE TABLE pmb.data_lain (
    no_pendaftaran character varying(100) NOT NULL,
    harapan character varying(300),
    darimana character varying(300),
    thn_daftar_bef character varying(20),
    utusan_stat integer,
    nama_utus character varying(100),
    alamat_utus character varying(255),
    tlp_utus character varying(20),
    utus_stat integer,
    mandiri_stat integer,
    nama_biaya character varying(100),
    alamat_biaya character varying(255),
    tlp_biaya character varying(20),
    tgl_insert date,
    tgl_update date,
    id bigint DEFAULT nextval('pmb.data_lain_id_seq'::regclass) NOT NULL
);
    DROP TABLE pmb.data_lain;
       pmb         heap    postgres    false    207    7            �            1259    20694    data_pp_id_seq    SEQUENCE     t   CREATE SEQUENCE pmb.data_pp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE pmb.data_pp_id_seq;
       pmb          postgres    false    7            �            1259    20696    data_pp    TABLE     >  CREATE TABLE pmb.data_pp (
    no_pendaftaran character varying(100) NOT NULL,
    tk character varying(100),
    tk_thn character varying(20),
    sd character varying(100),
    sd_thn character varying(20),
    smp character varying(100),
    smp_thn character varying(20),
    sma character varying(100),
    sma_thn character varying(20),
    lain1 character varying(100),
    lain1_thn character varying(20),
    lain2 character varying(100),
    lain2_thn character varying(20),
    nonf_nm text,
    nonf_lembaga text,
    nonf_thn character varying(255),
    nm_kerja text,
    jab_kerja text,
    thn_kerja character varying(255),
    lain1_nm character varying(200),
    lain2_nm character varying(200),
    tgl_insert date,
    tgl_update date,
    id bigint DEFAULT nextval('pmb.data_pp_id_seq'::regclass) NOT NULL
);
    DROP TABLE pmb.data_pp;
       pmb         heap    postgres    false    209    7            �            1259    20703    data_pribadi_id_seq    SEQUENCE     y   CREATE SEQUENCE pmb.data_pribadi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE pmb.data_pribadi_id_seq;
       pmb          postgres    false    7            �            1259    20705    data_pribadi    TABLE     �  CREATE TABLE pmb.data_pribadi (
    no_pendaftaran character varying(100) NOT NULL,
    nama character varying(255) NOT NULL,
    prodi character varying(100) NOT NULL,
    tempat_lahir character varying(100) NOT NULL,
    tgl_lahir date NOT NULL,
    kewarganegaraan character varying(10) NOT NULL,
    alamat character varying(255) NOT NULL,
    tlp character varying(20) NOT NULL,
    alamat_saudara character varying(255) NOT NULL,
    tlp_saudara character varying(20) NOT NULL,
    pend_terakhir character varying(20) NOT NULL,
    asal_sekolah character varying(255) NOT NULL,
    thn_lulus integer NOT NULL,
    alamat_sekolah character varying(255) NOT NULL,
    tinggal_dirumah character varying(100) NOT NULL,
    nama_pasangan character varying(100),
    tempat_lahir_pasangan character varying(100),
    tgl_lahir_pasangan date,
    "jml_anak_L" integer DEFAULT 0,
    "jml_anak_P" integer DEFAULT 0,
    gereja_anggota character varying(100),
    alamat_gereja_anggota character varying(255),
    ibadah_melayani_gereja integer,
    gereja_ibadah character varying(100),
    alamat_gereja_ibadah character varying(255),
    gereja_pelayanan character varying(50),
    pekerjaan character varying(20),
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(255),
    bidang_perusahaan character varying(50),
    minat character varying(20),
    inggris character varying(3),
    bahasa_asing character varying(255),
    bakat character varying(255),
    marital_stat character varying(25) NOT NULL,
    jk character varying(2) NOT NULL,
    tgl_insert timestamp without time zone,
    tgl_update timestamp without time zone,
    id bigint DEFAULT nextval('pmb.data_pribadi_id_seq'::regclass) NOT NULL
);
    DROP TABLE pmb.data_pribadi;
       pmb         heap    postgres    false    211    7            �            1259    20714    data_rohani_id_seq    SEQUENCE     x   CREATE SEQUENCE pmb.data_rohani_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE pmb.data_rohani_id_seq;
       pmb          postgres    false    7            �            1259    20716    data_rohani    TABLE     �  CREATE TABLE pmb.data_rohani (
    no_pendaftaran character varying(250) NOT NULL,
    anggota_thn character varying(20),
    "baptisA_thn" character varying(20),
    sidi_thn character varying(20),
    "baptisD_thn" character varying(20),
    sidi_by character varying(250),
    jenis_pelayanan text,
    tempat_pelayanan text,
    thn_pelayanan character varying(255),
    hambatan_pertumbuhan character varying(225),
    hambatan_pelayanan character varying(225),
    hambatan_masuk character varying(225),
    masalah character varying(225),
    masalah_detail text,
    jml_baca integer,
    persembahan_stat integer,
    persembahan_alasan character varying(225),
    pinjam_stat integer,
    hutang bigint,
    buku character varying(225),
    ht_pengaruh character varying(225),
    hobby character varying(255),
    olahraga character varying(300),
    tgl_insert date,
    tgl_update date,
    id bigint DEFAULT nextval('pmb.data_rohani_id_seq'::regclass) NOT NULL
);
    DROP TABLE pmb.data_rohani;
       pmb         heap    postgres    false    213    7            �            1259    20723    data_upload    TABLE     D  CREATE TABLE pmb.data_upload (
    no_pendaftaran character varying(100) NOT NULL,
    file_foto character varying(255),
    file_ktp character varying(255),
    file_ijazah character varying(255),
    file_kk character varying(255),
    tgl_insert timestamp without time zone,
    tgl_update timestamp without time zone
);
    DROP TABLE pmb.data_upload;
       pmb         heap    postgres    false    7            �            1259    20729    users    TABLE     �  CREATE TABLE pmb.users (
    "id_pmbU" bigint NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    insert_date timestamp(0) without time zone,
    no_pendaftaran character varying(255),
    password character varying(100),
    done integer DEFAULT 0 NOT NULL,
    delete integer DEFAULT 0,
    status_bayar integer DEFAULT 0,
    mhs integer DEFAULT 0
);
    DROP TABLE pmb.users;
       pmb         heap    postgres    false    7            �            1259    20740    users_id_pmbU_seq    SEQUENCE     y   CREATE SEQUENCE pmb."users_id_pmbU_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE pmb."users_id_pmbU_seq";
       pmb          postgres    false    7    216            �           0    0    users_id_pmbU_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE pmb."users_id_pmbU_seq" OWNED BY pmb.users."id_pmbU";
          pmb          postgres    false    217            �            1259    20742    admins_id_username_seq    SEQUENCE        CREATE SEQUENCE public.admins_id_username_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.admins_id_username_seq;
       public          postgres    false            �            1259    20744    admins    TABLE     �  CREATE TABLE public.admins (
    id_username integer DEFAULT nextval('public.admins_id_username_seq'::regclass) NOT NULL,
    username character varying(100) DEFAULT NULL::character varying,
    password character varying(50) DEFAULT NULL::character varying,
    nama_lengkap character varying(150) DEFAULT NULL::character varying,
    level character varying(50) DEFAULT NULL::character varying,
    blokir character varying(20) DEFAULT NULL::character varying,
    foto character varying(50) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone,
    kd_prodi character varying(10)
);
    DROP TABLE public.admins;
       public         heap    postgres    false    218            �            1259    20755    admins_new_id_username_seq    SEQUENCE     �   CREATE SEQUENCE public.admins_new_id_username_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.admins_new_id_username_seq;
       public          postgres    false            �            1259    20757 
   admins_new    TABLE     �  CREATE TABLE public.admins_new (
    id_username integer DEFAULT nextval('public.admins_new_id_username_seq'::regclass) NOT NULL,
    username character varying(100) DEFAULT NULL::character varying,
    password character varying(50) DEFAULT NULL::character varying,
    nama_lengkap character varying(150) DEFAULT NULL::character varying,
    level character varying(50) DEFAULT NULL::character varying,
    blokir character varying(20) DEFAULT NULL::character varying,
    foto character varying(50) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.admins_new;
       public         heap    postgres    false    220            �            1259    20768    app_setting    TABLE     ?  CREATE TABLE public.app_setting (
    id integer NOT NULL,
    nama_aplikasi character varying(100),
    nama_pendek character varying(50),
    nama_instansi character varying(100),
    alamat_instansi text,
    alamat1 character varying(200),
    alamat2 character varying(200),
    website character varying(50),
    email character varying(100),
    kota character varying(100),
    logo character varying(100),
    bg_login character varying(100),
    bg_admin character varying(100),
    warna_header_1 character varying(10),
    warna_header_2 character varying(10)
);
    DROP TABLE public.app_setting;
       public         heap    postgres    false            �            1259    20774    app_setting_id_seq    SEQUENCE     �   CREATE SEQUENCE public.app_setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.app_setting_id_seq;
       public          postgres    false    222            �           0    0    app_setting_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.app_setting_id_seq OWNED BY public.app_setting.id;
          public          postgres    false    223            �            1259    20776    banner    TABLE     Y   CREATE TABLE public.banner (
    id integer NOT NULL,
    path character varying(255)
);
    DROP TABLE public.banner;
       public         heap    postgres    false            �            1259    20779    banner_id_seq    SEQUENCE     �   CREATE SEQUENCE public.banner_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.banner_id_seq;
       public          postgres    false    224            �           0    0    banner_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.banner_id_seq OWNED BY public.banner.id;
          public          postgres    false    225            �            1259    20781    seq_id_bayar_maba    SEQUENCE     z   CREATE SEQUENCE public.seq_id_bayar_maba
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.seq_id_bayar_maba;
       public          postgres    false            �            1259    20783 
   bayar_maba    TABLE     �  CREATE TABLE public.bayar_maba (
    id bigint DEFAULT nextval('public.seq_id_bayar_maba'::regclass) NOT NULL,
    nomor character varying(50),
    jenis_tagihan_maba_id integer,
    th_akademik character varying(20),
    th_akademik_kode character varying(5),
    no_pendaftaran character varying(20),
    tanggal date,
    jumlah bigint,
    keterangan character varying(255),
    user_id character varying(50),
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.bayar_maba;
       public         heap    postgres    false    226            �            1259    20787    bayar_mhs_id_seq    SEQUENCE     z   CREATE SEQUENCE public.bayar_mhs_id_seq
    START WITH 12
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.bayar_mhs_id_seq;
       public          postgres    false            �            1259    20789 	   bayar_mhs    TABLE     �  CREATE TABLE public.bayar_mhs (
    id integer DEFAULT nextval('public.bayar_mhs_id_seq'::regclass) NOT NULL,
    nomor character varying(20) DEFAULT NULL::character varying,
    jenis_tagihan_id integer,
    th_akademik character varying(20) DEFAULT NULL::character varying,
    th_akademik_kode character varying(5) DEFAULT NULL::character varying,
    nim character varying(20) DEFAULT NULL::character varying,
    smt integer,
    tanggal date,
    jumlah integer,
    keterangan character varying(255) DEFAULT NULL::character varying,
    sks integer,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.bayar_mhs;
       public         heap    postgres    false    228            �            1259    20799    dispensasi_id_seq    SEQUENCE     z   CREATE SEQUENCE public.dispensasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.dispensasi_id_seq;
       public          postgres    false            �            1259    20801 
   dispensasi    TABLE     �  CREATE TABLE public.dispensasi (
    id integer DEFAULT nextval('public.dispensasi_id_seq'::regclass) NOT NULL,
    th_akademik_kode character varying(5) DEFAULT NULL::character varying,
    nim character varying(20) DEFAULT NULL::character varying,
    smt integer,
    kelas character varying(2) DEFAULT NULL::character varying,
    kd_prodi character varying(10) DEFAULT NULL::character varying,
    tanggal date,
    alasan character varying(50) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.dispensasi;
       public         heap    postgres    false    230            �            1259    20811    seq_id_dosen    SEQUENCE     v   CREATE SEQUENCE public.seq_id_dosen
    START WITH 37
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.seq_id_dosen;
       public          postgres    false            �            1259    20813    dosen    TABLE     �  CREATE TABLE public.dosen (
    kd_dosen character varying(10) NOT NULL,
    kd_prodi character varying(10) NOT NULL,
    nidn character varying(20) NOT NULL,
    nama_dosen character varying(150) NOT NULL,
    sex character varying(2) NOT NULL,
    tempat_lahir character varying(50) NOT NULL,
    tanggal_lahir date NOT NULL,
    alamat character varying(254) NOT NULL,
    hp character varying(20),
    pendidikan character varying(30) NOT NULL,
    email character varying(50) DEFAULT NULL::character varying,
    prodi character varying(100) NOT NULL,
    password character varying(50) NOT NULL,
    file_foto character varying(100) DEFAULT NULL::character varying,
    tgl_insert timestamp without time zone,
    status character varying(10) DEFAULT NULL::character varying,
    tgl_update timestamp without time zone,
    tgl_masuk date,
    id_dosen character varying(50) DEFAULT NULL::character varying,
    id bigint DEFAULT nextval('public.seq_id_dosen'::regclass) NOT NULL
);
    DROP TABLE public.dosen;
       public         heap    postgres    false    232            �            1259    20824    info_id_seq    SEQUENCE     t   CREATE SEQUENCE public.info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.info_id_seq;
       public          postgres    false            �            1259    20826    info    TABLE     �  CREATE TABLE public.info (
    id integer DEFAULT nextval('public.info_id_seq'::regclass) NOT NULL,
    tgl date,
    judul character varying(50) DEFAULT NULL::character varying,
    info character varying(255) DEFAULT NULL::character varying,
    pengirim character varying(50) DEFAULT NULL::character varying,
    username character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    jenis character varying(100)
);
    DROP TABLE public.info;
       public         heap    postgres    false    234            �            1259    20837    jadwal_id_jadwal_seq    SEQUENCE        CREATE SEQUENCE public.jadwal_id_jadwal_seq
    START WITH 124
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.jadwal_id_jadwal_seq;
       public          postgres    false            �            1259    20839    jadwal    TABLE       CREATE TABLE public.jadwal (
    id_jadwal integer DEFAULT nextval('public.jadwal_id_jadwal_seq'::regclass) NOT NULL,
    th_akademik character varying(5) NOT NULL,
    semester character varying(10) DEFAULT NULL::character varying,
    kd_prodi character varying(10) NOT NULL,
    kd_mk character varying(10) NOT NULL,
    kelas character varying(4) DEFAULT NULL::character varying,
    smt integer,
    kd_dosen character varying(10) NOT NULL,
    hari character varying(10) NOT NULL,
    pukul character varying(20),
    jam_mulai time without time zone,
    jam_selesai time without time zone,
    ruang character varying(255) NOT NULL,
    user_id character varying(50) DEFAULT NULL::character varying,
    tgl_insert timestamp without time zone,
    tgl_update timestamp without time zone
);
    DROP TABLE public.jadwal;
       public         heap    postgres    false    236            �            1259    20846 
   jam_kuliah    TABLE     t   CREATE TABLE public.jam_kuliah (
    kode character varying(10) NOT NULL,
    jam character varying(50) NOT NULL
);
    DROP TABLE public.jam_kuliah;
       public         heap    postgres    false            �            1259    20849    jenis_tagihan_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.jenis_tagihan_id_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.jenis_tagihan_id_seq;
       public          postgres    false            �            1259    20851    jenis_tagihan    TABLE     �  CREATE TABLE public.jenis_tagihan (
    id integer DEFAULT nextval('public.jenis_tagihan_id_seq'::regclass) NOT NULL,
    th_akademik character varying(20) DEFAULT NULL::character varying,
    smt integer,
    kd_prodi character varying(10) DEFAULT NULL::character varying,
    nama character varying(50) DEFAULT NULL::character varying,
    jumlah integer,
    x_sks character varying(10) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone,
    biaya_uang_kuliah bigint,
    biaya_konsumsi bigint,
    biaya_asrama bigint,
    dana_pengembangan bigint,
    biaya_partisipasi bigint,
    biaya_heregistrasi bigint
);
 !   DROP TABLE public.jenis_tagihan;
       public         heap    postgres    false    239            �            1259    20860    seq_id_jenis_tagihan_maba    SEQUENCE     �   CREATE SEQUENCE public.seq_id_jenis_tagihan_maba
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.seq_id_jenis_tagihan_maba;
       public          postgres    false            �            1259    20862    jenis_tagihan_maba    TABLE     }  CREATE TABLE public.jenis_tagihan_maba (
    id integer DEFAULT nextval('public.seq_id_jenis_tagihan_maba'::regclass) NOT NULL,
    th_akademik character varying(20),
    kd_prodi character varying(10),
    nama character varying(50),
    jumlah bigint,
    user_id character varying(50),
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
 &   DROP TABLE public.jenis_tagihan_maba;
       public         heap    postgres    false    241            �            1259    20866    krs_id_krs_seq    SEQUENCE     y   CREATE SEQUENCE public.krs_id_krs_seq
    START WITH 143
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.krs_id_krs_seq;
       public          postgres    false            �            1259    20868    krs    TABLE     w  CREATE TABLE public.krs (
    id_krs integer DEFAULT nextval('public.krs_id_krs_seq'::regclass) NOT NULL,
    th_akademik character varying(10) NOT NULL,
    smt integer NOT NULL,
    semester character varying(10) DEFAULT NULL::character varying,
    id_jadwal integer NOT NULL,
    kelas character varying(4) DEFAULT NULL::character varying,
    nim character varying(20) NOT NULL,
    kd_prodi character varying(10) NOT NULL,
    kd_mk character varying(10) NOT NULL,
    nama_mk character varying(100) NOT NULL,
    sks integer NOT NULL,
    kd_dosen character varying(10) NOT NULL,
    nm_dosen character varying(100) NOT NULL,
    ruang character varying(50) NOT NULL,
    hari character varying(15) NOT NULL,
    pukul character varying(15),
    nilai_uts character varying(5) DEFAULT NULL::character varying,
    nilai_uas character varying(5) DEFAULT NULL::character varying,
    nilai_akhir character varying(5) DEFAULT NULL::character varying,
    acc_dosen character varying(2) DEFAULT NULL::character varying,
    status character varying(10) DEFAULT NULL::character varying,
    tampil character varying(2) DEFAULT NULL::character varying,
    user_id_dosen character varying(50) DEFAULT NULL::character varying,
    tgl_insert timestamp without time zone NOT NULL,
    user_id_admin character varying(50) DEFAULT NULL::character varying,
    tgl_update timestamp without time zone
);
    DROP TABLE public.krs;
       public         heap    postgres    false    243            �            1259    20885 	   mahasiswa    TABLE     �  CREATE TABLE public.mahasiswa (
    th_akademik character varying(15) NOT NULL,
    nim character varying(15) NOT NULL,
    kd_prodi character varying(15) NOT NULL,
    kelas character varying(4) DEFAULT NULL::character varying,
    nama_mhs character varying(150) NOT NULL,
    sex character varying(2) NOT NULL,
    tempat_lahir character varying(100) DEFAULT NULL::character varying,
    tanggal_lahir date NOT NULL,
    alamat character varying(150) DEFAULT NULL::character varying,
    kota character varying(50) DEFAULT NULL::character varying,
    hp character varying(30) DEFAULT NULL::character varying,
    email character varying(50) DEFAULT NULL::character varying,
    nama_ayah character varying(100) DEFAULT NULL::character varying,
    nama_ibu character varying(100) DEFAULT NULL::character varying,
    alamat_ortu character varying(150) DEFAULT NULL::character varying,
    hp_ortu character varying(50) DEFAULT NULL::character varying,
    password character varying(50) NOT NULL,
    file_foto character varying(100) DEFAULT NULL::character varying,
    status character varying(10) DEFAULT NULL::character varying,
    tgl_masuk date,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone,
    ktp character varying(50) DEFAULT NULL::character varying,
    id_mahasiswa character varying(60) DEFAULT NULL::character varying,
    isi_krs integer DEFAULT 0,
    account_mob boolean DEFAULT false,
    nama_ortu character varying(255),
    tgl_lahir date
);
    DROP TABLE public.mahasiswa;
       public         heap    postgres    false            �            1259    20907    mahasiswa_aktif_history    TABLE       CREATE TABLE public.mahasiswa_aktif_history (
    th_akademik character varying(15) NOT NULL,
    nim character varying(15) NOT NULL,
    status character varying(10) DEFAULT NULL::character varying,
    date_created timestamp without time zone NOT NULL
);
 +   DROP TABLE public.mahasiswa_aktif_history;
       public         heap    postgres    false            �            1259    20911    mahasiswa_backup    TABLE     �  CREATE TABLE public.mahasiswa_backup (
    th_akademik character varying(15) NOT NULL,
    nim character varying(15) NOT NULL,
    kd_prodi character varying(15) NOT NULL,
    kelas character varying(4) DEFAULT NULL::character varying,
    nama_mhs character varying(150) NOT NULL,
    sex character varying(2) NOT NULL,
    tempat_lahir character varying(100) DEFAULT NULL::character varying,
    tanggal_lahir date NOT NULL,
    alamat character varying(150) DEFAULT NULL::character varying,
    kota character varying(50) DEFAULT NULL::character varying,
    hp character varying(30) DEFAULT NULL::character varying,
    email character varying(50) DEFAULT NULL::character varying,
    nama_ayah character varying(100) DEFAULT NULL::character varying,
    nama_ibu character varying(100) DEFAULT NULL::character varying,
    alamat_ortu character varying(150) DEFAULT NULL::character varying,
    hp_ortu character varying(50) DEFAULT NULL::character varying,
    password character varying(50) NOT NULL,
    file_foto character varying(100) DEFAULT NULL::character varying,
    status character varying(10) DEFAULT NULL::character varying,
    tgl_masuk date,
    tgl_insert timestamp without time zone,
    tgl_update timestamp without time zone
);
 $   DROP TABLE public.mahasiswa_backup;
       public         heap    postgres    false            �            1259    20929    seq_id_master_tagihan    SEQUENCE     ~   CREATE SEQUENCE public.seq_id_master_tagihan
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.seq_id_master_tagihan;
       public          postgres    false            �            1259    20931    master_tagihan    TABLE     �   CREATE TABLE public.master_tagihan (
    id integer DEFAULT nextval('public.seq_id_master_tagihan'::regclass) NOT NULL,
    nama_tagihan character varying(100)
);
 "   DROP TABLE public.master_tagihan;
       public         heap    postgres    false    248            �            1259    20935    master_tagihan_maba    TABLE     �   CREATE TABLE public.master_tagihan_maba (
    id bigint NOT NULL,
    nama_tagihan character varying NOT NULL,
    jumlah bigint NOT NULL
);
 '   DROP TABLE public.master_tagihan_maba;
       public         heap    postgres    false            �            1259    20941    mata_kuliah    TABLE     �  CREATE TABLE public.mata_kuliah (
    kd_mk character varying(10) NOT NULL,
    kd_prodi character varying(10) NOT NULL,
    nama_mk character varying(100) NOT NULL,
    sks integer NOT NULL,
    smt character varying(2) NOT NULL,
    semester character varying(10) NOT NULL,
    aktif character varying(10) NOT NULL,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone,
    id_matkul character varying(50) DEFAULT NULL::character varying
);
    DROP TABLE public.mata_kuliah;
       public         heap    postgres    false            �            1259    20945    materi_kuliah_id_seq    SEQUENCE     }   CREATE SEQUENCE public.materi_kuliah_id_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.materi_kuliah_id_seq;
       public          postgres    false            �            1259    20947    materi_kuliah    TABLE     �  CREATE TABLE public.materi_kuliah (
    id integer DEFAULT nextval('public.materi_kuliah_id_seq'::regclass) NOT NULL,
    kd_mk character varying(10) DEFAULT NULL::character varying,
    judul character varying(50) DEFAULT NULL::character varying,
    file text DEFAULT NULL::character varying,
    username character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    smt integer
);
 !   DROP TABLE public.materi_kuliah;
       public         heap    postgres    false    252            �            1259    20958    seq_id_materi_seminar    SEQUENCE     ~   CREATE SEQUENCE public.seq_id_materi_seminar
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.seq_id_materi_seminar;
       public          postgres    false            �            1259    20960    materi_seminar    TABLE     
  CREATE TABLE public.materi_seminar (
    id bigint DEFAULT nextval('public.seq_id_materi_seminar'::regclass) NOT NULL,
    judul character varying(100),
    file text,
    username character varying(50),
    insert_date timestamp without time zone,
    name text
);
 "   DROP TABLE public.materi_seminar;
       public         heap    postgres    false    254                        1259    20967    mutasi_mhs_id_mutasi_seq    SEQUENCE     �   CREATE SEQUENCE public.mutasi_mhs_id_mutasi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.mutasi_mhs_id_mutasi_seq;
       public          postgres    false                       1259    20969 
   mutasi_mhs    TABLE     �  CREATE TABLE public.mutasi_mhs (
    id_mutasi integer DEFAULT nextval('public.mutasi_mhs_id_mutasi_seq'::regclass) NOT NULL,
    th_akademik character varying(10) NOT NULL,
    semester character varying(10) DEFAULT NULL::character varying,
    tgl_mutasi date NOT NULL,
    nim character varying(20) NOT NULL,
    status character varying(10) NOT NULL,
    ket character varying(255) NOT NULL,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone
);
    DROP TABLE public.mutasi_mhs;
       public         heap    postgres    false    256                       1259    20974    nilai_huruf    TABLE     �   CREATE TABLE public.nilai_huruf (
    huruf character varying,
    range_from numeric,
    range_to numeric,
    nilai_from numeric,
    nilai_to numeric
);
    DROP TABLE public.nilai_huruf;
       public         heap    postgres    false                       1259    20980    nim_last_number    TABLE     z   CREATE TABLE public.nim_last_number (
    tahun_ajar integer NOT NULL,
    no_urut integer,
    no_id integer NOT NULL
);
 #   DROP TABLE public.nim_last_number;
       public         heap    postgres    false                       1259    20983    param_nilai_id_seq    SEQUENCE     {   CREATE SEQUENCE public.param_nilai_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.param_nilai_id_seq;
       public          postgres    false                       1259    20985    param_nilai    TABLE     |  CREATE TABLE public.param_nilai (
    id integer DEFAULT nextval('public.param_nilai_id_seq'::regclass) NOT NULL,
    range_awal real,
    range_akhir real,
    nilai character varying(2) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.param_nilai;
       public         heap    postgres    false    260                       1259    20991    pembayaran_mhs_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.pembayaran_mhs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.pembayaran_mhs_id_seq;
       public          postgres    false                       1259    20993    pembayaran_mhs    TABLE       CREATE TABLE public.pembayaran_mhs (
    id integer DEFAULT nextval('public.pembayaran_mhs_id_seq'::regclass) NOT NULL,
    tagihan_mhs_id integer,
    nama_tagihan character varying(50) DEFAULT NULL::character varying,
    nim character varying(15) DEFAULT NULL::character varying,
    th_akademik_kode character varying(5) DEFAULT NULL::character varying,
    jumlah integer,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
 "   DROP TABLE public.pembayaran_mhs;
       public         heap    postgres    false    262                       1259    21001    pesan_id_pesan_seq    SEQUENCE     {   CREATE SEQUENCE public.pesan_id_pesan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.pesan_id_pesan_seq;
       public          postgres    false            	           1259    21003    pesan    TABLE     �  CREATE TABLE public.pesan (
    id_pesan integer DEFAULT nextval('public.pesan_id_pesan_seq'::regclass) NOT NULL,
    dari character varying(50) NOT NULL,
    ke character varying(50) NOT NULL,
    judul character varying(100) NOT NULL,
    isi character varying(4) NOT NULL,
    status character varying(10) DEFAULT NULL::character varying,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone NOT NULL
);
    DROP TABLE public.pesan;
       public         heap    postgres    false    264            
           1259    21008    poll_jawaban_id_seq    SEQUENCE     |   CREATE SEQUENCE public.poll_jawaban_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.poll_jawaban_id_seq;
       public          postgres    false                       1259    21010    poll_jawaban    TABLE     |  CREATE TABLE public.poll_jawaban (
    th_akademik character varying,
    nim character varying,
    poll_jenis_id integer,
    kd_dosen character varying,
    kd_prodi character varying,
    user_id character varying,
    insert_date timestamp without time zone,
    id bigint DEFAULT nextval('public.poll_jawaban_id_seq'::regclass) NOT NULL,
    pegawai_id character varying
);
     DROP TABLE public.poll_jawaban;
       public         heap    postgres    false    266                       1259    21017    poll_jawaban_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.poll_jawaban_detail_id_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.poll_jawaban_detail_id_seq;
       public          postgres    false                       1259    21019    poll_jawaban_detail    TABLE     �   CREATE TABLE public.poll_jawaban_detail (
    poll_jawaban_id integer NOT NULL,
    poll_pertanyaan_id integer,
    jawaban character varying,
    id bigint DEFAULT nextval('public.poll_jawaban_detail_id_seq'::regclass) NOT NULL
);
 '   DROP TABLE public.poll_jawaban_detail;
       public         heap    postgres    false    268                       1259    21026    poll_jenis_id_seq    SEQUENCE     z   CREATE SEQUENCE public.poll_jenis_id_seq
    START WITH 6
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.poll_jenis_id_seq;
       public          postgres    false                       1259    21028 
   poll_jenis    TABLE       CREATE TABLE public.poll_jenis (
    id integer DEFAULT nextval('public.poll_jenis_id_seq'::regclass) NOT NULL,
    nama character varying,
    user_id character varying,
    update_date timestamp without time zone,
    insert_date timestamp without time zone
);
    DROP TABLE public.poll_jenis;
       public         heap    postgres    false    270                       1259    21035    poll_pertanyaan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.poll_pertanyaan_id_seq
    START WITH 23
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.poll_pertanyaan_id_seq;
       public          postgres    false                       1259    21037    poll_pertanyaan    TABLE     m  CREATE TABLE public.poll_pertanyaan (
    pertanyaan character varying,
    pilihan character varying,
    aktif character varying,
    user_id character varying,
    update_date timestamp without time zone,
    insert_date timestamp without time zone,
    id bigint DEFAULT nextval('public.poll_pertanyaan_id_seq'::regclass) NOT NULL,
    poll_jenis_id integer
);
 #   DROP TABLE public.poll_pertanyaan;
       public         heap    postgres    false    272                       1259    21044    poll_transaksi_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.poll_transaksi_id_seq
    START WITH 7
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.poll_transaksi_id_seq;
       public          postgres    false                       1259    21046    poll_transaksi    TABLE     [  CREATE TABLE public.poll_transaksi (
    id integer DEFAULT nextval('public.poll_transaksi_id_seq'::regclass) NOT NULL,
    th_akademik character varying,
    poll_jenis_id integer,
    user_id character varying,
    update_date timestamp without time zone,
    insert_date timestamp without time zone,
    poll_pertanyaan_id character varying
);
 "   DROP TABLE public.poll_transaksi;
       public         heap    postgres    false    274                       1259    21053    prodi    TABLE     �  CREATE TABLE public.prodi (
    kd_prodi character varying(10) NOT NULL,
    prodi character varying(500) NOT NULL,
    singkat character varying(10) NOT NULL,
    ketua_prodi character varying(100) NOT NULL,
    nik character varying(30) NOT NULL,
    akreditasi character varying(2) NOT NULL,
    max_semester integer,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone,
    id_prodi character varying
);
    DROP TABLE public.prodi;
       public         heap    postgres    false                       1259    21059    renungan    TABLE     �   CREATE TABLE public.renungan (
    id integer NOT NULL,
    bacaan character varying(255),
    judul character varying(255),
    renungan text,
    tanggal timestamp without time zone,
    banner integer
);
    DROP TABLE public.renungan;
       public         heap    postgres    false                       1259    21065    renungan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.renungan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.renungan_id_seq;
       public          postgres    false    277            �           0    0    renungan_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.renungan_id_seq OWNED BY public.renungan.id;
          public          postgres    false    278                       1259    21067    ruang_kuliah_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ruang_kuliah_id_seq
    START WITH 41
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 99999999999
    CACHE 1;
 *   DROP SEQUENCE public.ruang_kuliah_id_seq;
       public          postgres    false                       1259    21069    ruang_kuliah    TABLE     f  CREATE TABLE public.ruang_kuliah (
    id integer DEFAULT nextval('public.ruang_kuliah_id_seq'::regclass) NOT NULL,
    kode character varying(10) NOT NULL,
    nama character varying(100) NOT NULL,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
     DROP TABLE public.ruang_kuliah;
       public         heap    postgres    false    279                       1259    21074    semester    TABLE     �   CREATE TABLE public.semester (
    semester character varying(10) DEFAULT NULL::character varying,
    dari integer NOT NULL,
    sampai integer NOT NULL
);
    DROP TABLE public.semester;
       public         heap    postgres    false                       1259    21078    setting_id_seq    SEQUENCE     w   CREATE SEQUENCE public.setting_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.setting_id_seq;
       public          postgres    false                       1259    21080    setting    TABLE     �   CREATE TABLE public.setting (
    id integer DEFAULT nextval('public.setting_id_seq'::regclass) NOT NULL,
    form character varying(50) NOT NULL,
    tgl_close date NOT NULL
);
    DROP TABLE public.setting;
       public         heap    postgres    false    282                       1259    21084    status_mhs_id_seq    SEQUENCE     z   CREATE SEQUENCE public.status_mhs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.status_mhs_id_seq;
       public          postgres    false                       1259    21086 
   status_mhs    TABLE     �   CREATE TABLE public.status_mhs (
    id integer DEFAULT nextval('public.status_mhs_id_seq'::regclass) NOT NULL,
    status character varying(50) DEFAULT NULL::character varying
);
    DROP TABLE public.status_mhs;
       public         heap    postgres    false    284                       1259    21091    tagihan_mhs_id_seq    SEQUENCE     {   CREATE SEQUENCE public.tagihan_mhs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.tagihan_mhs_id_seq;
       public          postgres    false                       1259    21093    tagihan_mhs    TABLE     �  CREATE TABLE public.tagihan_mhs (
    id integer DEFAULT nextval('public.tagihan_mhs_id_seq'::regclass) NOT NULL,
    jenis_tagihan_id integer NOT NULL,
    nim character varying(15) NOT NULL,
    kd_prodi character varying(10) DEFAULT NULL::character varying,
    kelas character varying(2) DEFAULT NULL::character varying,
    th_akademik_kode character varying(5) NOT NULL,
    th_angkatan_kode character varying(5) DEFAULT NULL::character varying,
    smt integer,
    jumlah integer,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.tagihan_mhs;
       public         heap    postgres    false    286                        1259    21101    th_akademik_id_seq    SEQUENCE     |   CREATE SEQUENCE public.th_akademik_id_seq
    START WITH 26
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.th_akademik_id_seq;
       public          postgres    false            !           1259    21103    th_akademik    TABLE     O  CREATE TABLE public.th_akademik (
    id integer DEFAULT nextval('public.th_akademik_id_seq'::regclass) NOT NULL,
    kode character(5) DEFAULT NULL::bpchar,
    th_akademik character varying(10) DEFAULT NULL::character varying,
    semester character varying(10) DEFAULT NULL::character varying,
    keterangan character varying(50) DEFAULT NULL::character varying,
    aktif character varying(10) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
    DROP TABLE public.th_akademik;
       public         heap    postgres    false    288            "           1259    21113    wisuda_id_wisuda_seq    SEQUENCE     }   CREATE SEQUENCE public.wisuda_id_wisuda_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.wisuda_id_wisuda_seq;
       public          postgres    false            #           1259    21115    wisuda    TABLE     �  CREATE TABLE public.wisuda (
    id_wisuda integer DEFAULT nextval('public.wisuda_id_wisuda_seq'::regclass) NOT NULL,
    th_akademik character varying(10) NOT NULL,
    tgl_daftar date NOT NULL,
    nim character varying(15) NOT NULL,
    kd_prodi character varying(10) DEFAULT NULL::character varying,
    judul_skripsi character varying(255) NOT NULL,
    tgl_sidang date NOT NULL,
    tgl_insert timestamp without time zone NOT NULL,
    tgl_update timestamp without time zone,
    ipk real NOT NULL,
    acc_akademik character varying(2) DEFAULT 'T'::character varying NOT NULL,
    smt integer,
    user_id character varying(50) DEFAULT NULL::character varying
);
    DROP TABLE public.wisuda;
       public         heap    postgres    false    290            $           1259    21122    wisuda_validasi_id_seq    SEQUENCE        CREATE SEQUENCE public.wisuda_validasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.wisuda_validasi_id_seq;
       public          postgres    false            %           1259    21124    wisuda_validasi    TABLE     �  CREATE TABLE public.wisuda_validasi (
    id integer DEFAULT nextval('public.wisuda_validasi_id_seq'::regclass) NOT NULL,
    th_akademik_kode character varying(5) DEFAULT NULL::character varying,
    nim character varying(20) DEFAULT NULL::character varying,
    smt integer,
    kelas character varying(2) DEFAULT NULL::character varying,
    kd_prodi character varying(10) DEFAULT NULL::character varying,
    tanggal date,
    keterangan character varying(255) DEFAULT NULL::character varying,
    user_id character varying(50) DEFAULT NULL::character varying,
    insert_date timestamp without time zone,
    update_date timestamp without time zone
);
 #   DROP TABLE public.wisuda_validasi;
       public         heap    postgres    false    292                       2604    21134    users id_pmbU    DEFAULT     l   ALTER TABLE ONLY pmb.users ALTER COLUMN "id_pmbU" SET DEFAULT nextval('pmb."users_id_pmbU_seq"'::regclass);
 ;   ALTER TABLE pmb.users ALTER COLUMN "id_pmbU" DROP DEFAULT;
       pmb          postgres    false    217    216                       2604    21135    app_setting id    DEFAULT     p   ALTER TABLE ONLY public.app_setting ALTER COLUMN id SET DEFAULT nextval('public.app_setting_id_seq'::regclass);
 =   ALTER TABLE public.app_setting ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    223    222                       2604    21136 	   banner id    DEFAULT     f   ALTER TABLE ONLY public.banner ALTER COLUMN id SET DEFAULT nextval('public.banner_id_seq'::regclass);
 8   ALTER TABLE public.banner ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224            �           2604    21137    renungan id    DEFAULT     j   ALTER TABLE ONLY public.renungan ALTER COLUMN id SET DEFAULT nextval('public.renungan_id_seq'::regclass);
 :   ALTER TABLE public.renungan ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    278    277            �          0    20670    admins 
   TABLE DATA           ^   COPY pmb.admins (id, username, password, email, status, insert_time, update_time) FROM stdin;
    pmb          postgres    false    204   `j      �          0    20677    data_keluarga 
   TABLE DATA           �  COPY pmb.data_keluarga (no_pendaftaran, nama_ayah, nama_ibu, umur_ayah, umur_ibu, alamat_ortu, tlp_ortu, pend_ayah, pend_ibu, pekerjaan_ayah, pekerjaan_ibu, agama_ayah, agama_ibu, nama_wali, alamat_wali, tlp_wali, pekerjaan_wali, hubungan_wali, agama_wali, nama_sk, jk_sk, umur_sk, pend_sk, agama_sk, nama_pcr, agama_pcr, hub_pcr, tanggapan_pcr, thn_div, alasan_div, stat_tp, nama_tp, thn_pcr, tgl_insert, tgl_update, id) FROM stdin;
    pmb          postgres    false    206   Uk      �          0    20687 	   data_lain 
   TABLE DATA           �   COPY pmb.data_lain (no_pendaftaran, harapan, darimana, thn_daftar_bef, utusan_stat, nama_utus, alamat_utus, tlp_utus, utus_stat, mandiri_stat, nama_biaya, alamat_biaya, tlp_biaya, tgl_insert, tgl_update, id) FROM stdin;
    pmb          postgres    false    208   El      �          0    20696    data_pp 
   TABLE DATA           �   COPY pmb.data_pp (no_pendaftaran, tk, tk_thn, sd, sd_thn, smp, smp_thn, sma, sma_thn, lain1, lain1_thn, lain2, lain2_thn, nonf_nm, nonf_lembaga, nonf_thn, nm_kerja, jab_kerja, thn_kerja, lain1_nm, lain2_nm, tgl_insert, tgl_update, id) FROM stdin;
    pmb          postgres    false    210   �l      �          0    20705    data_pribadi 
   TABLE DATA           >  COPY pmb.data_pribadi (no_pendaftaran, nama, prodi, tempat_lahir, tgl_lahir, kewarganegaraan, alamat, tlp, alamat_saudara, tlp_saudara, pend_terakhir, asal_sekolah, thn_lulus, alamat_sekolah, tinggal_dirumah, nama_pasangan, tempat_lahir_pasangan, tgl_lahir_pasangan, "jml_anak_L", "jml_anak_P", gereja_anggota, alamat_gereja_anggota, ibadah_melayani_gereja, gereja_ibadah, alamat_gereja_ibadah, gereja_pelayanan, pekerjaan, nama_perusahaan, alamat_perusahaan, bidang_perusahaan, minat, inggris, bahasa_asing, bakat, marital_stat, jk, tgl_insert, tgl_update, id) FROM stdin;
    pmb          postgres    false    212   �m      �          0    20716    data_rohani 
   TABLE DATA           z  COPY pmb.data_rohani (no_pendaftaran, anggota_thn, "baptisA_thn", sidi_thn, "baptisD_thn", sidi_by, jenis_pelayanan, tempat_pelayanan, thn_pelayanan, hambatan_pertumbuhan, hambatan_pelayanan, hambatan_masuk, masalah, masalah_detail, jml_baca, persembahan_stat, persembahan_alasan, pinjam_stat, hutang, buku, ht_pengaruh, hobby, olahraga, tgl_insert, tgl_update, id) FROM stdin;
    pmb          postgres    false    214   p      �          0    20723    data_upload 
   TABLE DATA           u   COPY pmb.data_upload (no_pendaftaran, file_foto, file_ktp, file_ijazah, file_kk, tgl_insert, tgl_update) FROM stdin;
    pmb          postgres    false    215   p      �          0    20729    users 
   TABLE DATA           �   COPY pmb.users ("id_pmbU", username, email, status, insert_date, no_pendaftaran, password, done, delete, status_bayar, mhs) FROM stdin;
    pmb          postgres    false    216   �q      �          0    20744    admins 
   TABLE DATA           �   COPY public.admins (id_username, username, password, nama_lengkap, level, blokir, foto, user_id, insert_date, update_date, kd_prodi) FROM stdin;
    public          postgres    false    219   s      �          0    20757 
   admins_new 
   TABLE DATA           �   COPY public.admins_new (id_username, username, password, nama_lengkap, level, blokir, foto, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    221   ps      �          0    20768    app_setting 
   TABLE DATA           �   COPY public.app_setting (id, nama_aplikasi, nama_pendek, nama_instansi, alamat_instansi, alamat1, alamat2, website, email, kota, logo, bg_login, bg_admin, warna_header_1, warna_header_2) FROM stdin;
    public          postgres    false    222   �s      �          0    20776    banner 
   TABLE DATA           *   COPY public.banner (id, path) FROM stdin;
    public          postgres    false    224   u      �          0    20783 
   bayar_maba 
   TABLE DATA           �   COPY public.bayar_maba (id, nomor, jenis_tagihan_maba_id, th_akademik, th_akademik_kode, no_pendaftaran, tanggal, jumlah, keterangan, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    227   ,u      �          0    20789 	   bayar_mhs 
   TABLE DATA           �   COPY public.bayar_mhs (id, nomor, jenis_tagihan_id, th_akademik, th_akademik_kode, nim, smt, tanggal, jumlah, keterangan, sks, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    229   Iu      �          0    20801 
   dispensasi 
   TABLE DATA           �   COPY public.dispensasi (id, th_akademik_kode, nim, smt, kelas, kd_prodi, tanggal, alasan, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    231   fu      �          0    20813    dosen 
   TABLE DATA           �   COPY public.dosen (kd_dosen, kd_prodi, nidn, nama_dosen, sex, tempat_lahir, tanggal_lahir, alamat, hp, pendidikan, email, prodi, password, file_foto, tgl_insert, status, tgl_update, tgl_masuk, id_dosen, id) FROM stdin;
    public          postgres    false    233   �u      �          0    20826    info 
   TABLE DATA           \   COPY public.info (id, tgl, judul, info, pengirim, username, insert_date, jenis) FROM stdin;
    public          postgres    false    235   �u      �          0    20839    jadwal 
   TABLE DATA           �   COPY public.jadwal (id_jadwal, th_akademik, semester, kd_prodi, kd_mk, kelas, smt, kd_dosen, hari, pukul, jam_mulai, jam_selesai, ruang, user_id, tgl_insert, tgl_update) FROM stdin;
    public          postgres    false    237   �u      �          0    20846 
   jam_kuliah 
   TABLE DATA           /   COPY public.jam_kuliah (kode, jam) FROM stdin;
    public          postgres    false    238   �u      �          0    20851    jenis_tagihan 
   TABLE DATA           �   COPY public.jenis_tagihan (id, th_akademik, smt, kd_prodi, nama, jumlah, x_sks, user_id, insert_date, update_date, biaya_uang_kuliah, biaya_konsumsi, biaya_asrama, dana_pengembangan, biaya_partisipasi, biaya_heregistrasi) FROM stdin;
    public          postgres    false    240   �u      �          0    20862    jenis_tagihan_maba 
   TABLE DATA           x   COPY public.jenis_tagihan_maba (id, th_akademik, kd_prodi, nama, jumlah, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    242   v      �          0    20868    krs 
   TABLE DATA             COPY public.krs (id_krs, th_akademik, smt, semester, id_jadwal, kelas, nim, kd_prodi, kd_mk, nama_mk, sks, kd_dosen, nm_dosen, ruang, hari, pukul, nilai_uts, nilai_uas, nilai_akhir, acc_dosen, status, tampil, user_id_dosen, tgl_insert, user_id_admin, tgl_update) FROM stdin;
    public          postgres    false    244   1v      �          0    20885 	   mahasiswa 
   TABLE DATA           5  COPY public.mahasiswa (th_akademik, nim, kd_prodi, kelas, nama_mhs, sex, tempat_lahir, tanggal_lahir, alamat, kota, hp, email, nama_ayah, nama_ibu, alamat_ortu, hp_ortu, password, file_foto, status, tgl_masuk, tgl_insert, tgl_update, ktp, id_mahasiswa, isi_krs, account_mob, nama_ortu, tgl_lahir) FROM stdin;
    public          postgres    false    245   Nv      �          0    20907    mahasiswa_aktif_history 
   TABLE DATA           Y   COPY public.mahasiswa_aktif_history (th_akademik, nim, status, date_created) FROM stdin;
    public          postgres    false    246   kv      �          0    20911    mahasiswa_backup 
   TABLE DATA           �   COPY public.mahasiswa_backup (th_akademik, nim, kd_prodi, kelas, nama_mhs, sex, tempat_lahir, tanggal_lahir, alamat, kota, hp, email, nama_ayah, nama_ibu, alamat_ortu, hp_ortu, password, file_foto, status, tgl_masuk, tgl_insert, tgl_update) FROM stdin;
    public          postgres    false    247   �v      �          0    20931    master_tagihan 
   TABLE DATA           :   COPY public.master_tagihan (id, nama_tagihan) FROM stdin;
    public          postgres    false    249   �v      �          0    20935    master_tagihan_maba 
   TABLE DATA           G   COPY public.master_tagihan_maba (id, nama_tagihan, jumlah) FROM stdin;
    public          postgres    false    250   �v      �          0    20941    mata_kuliah 
   TABLE DATA           }   COPY public.mata_kuliah (kd_mk, kd_prodi, nama_mk, sks, smt, semester, aktif, tgl_insert, tgl_update, id_matkul) FROM stdin;
    public          postgres    false    251   w      �          0    20947    materi_kuliah 
   TABLE DATA           [   COPY public.materi_kuliah (id, kd_mk, judul, file, username, insert_date, smt) FROM stdin;
    public          postgres    false    253   +w      �          0    20960    materi_seminar 
   TABLE DATA           V   COPY public.materi_seminar (id, judul, file, username, insert_date, name) FROM stdin;
    public          postgres    false    255   Hw      �          0    20969 
   mutasi_mhs 
   TABLE DATA           |   COPY public.mutasi_mhs (id_mutasi, th_akademik, semester, tgl_mutasi, nim, status, ket, tgl_insert, tgl_update) FROM stdin;
    public          postgres    false    257   ew      �          0    20974    nilai_huruf 
   TABLE DATA           X   COPY public.nilai_huruf (huruf, range_from, range_to, nilai_from, nilai_to) FROM stdin;
    public          postgres    false    258   �w      �          0    20980    nim_last_number 
   TABLE DATA           E   COPY public.nim_last_number (tahun_ajar, no_urut, no_id) FROM stdin;
    public          postgres    false    259   �w      �          0    20985    param_nilai 
   TABLE DATA           l   COPY public.param_nilai (id, range_awal, range_akhir, nilai, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    261   �w      �          0    20993    pembayaran_mhs 
   TABLE DATA           �   COPY public.pembayaran_mhs (id, tagihan_mhs_id, nama_tagihan, nim, th_akademik_kode, jumlah, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    263   �w      �          0    21003    pesan 
   TABLE DATA           _   COPY public.pesan (id_pesan, dari, ke, judul, isi, status, tgl_insert, tgl_update) FROM stdin;
    public          postgres    false    265   �w      �          0    21010    poll_jawaban 
   TABLE DATA           �   COPY public.poll_jawaban (th_akademik, nim, poll_jenis_id, kd_dosen, kd_prodi, user_id, insert_date, id, pegawai_id) FROM stdin;
    public          postgres    false    267   x      �          0    21019    poll_jawaban_detail 
   TABLE DATA           _   COPY public.poll_jawaban_detail (poll_jawaban_id, poll_pertanyaan_id, jawaban, id) FROM stdin;
    public          postgres    false    269   0x      �          0    21028 
   poll_jenis 
   TABLE DATA           Q   COPY public.poll_jenis (id, nama, user_id, update_date, insert_date) FROM stdin;
    public          postgres    false    271   Mx      �          0    21037    poll_pertanyaan 
   TABLE DATA           {   COPY public.poll_pertanyaan (pertanyaan, pilihan, aktif, user_id, update_date, insert_date, id, poll_jenis_id) FROM stdin;
    public          postgres    false    273   jx      �          0    21046    poll_transaksi 
   TABLE DATA              COPY public.poll_transaksi (id, th_akademik, poll_jenis_id, user_id, update_date, insert_date, poll_pertanyaan_id) FROM stdin;
    public          postgres    false    275   �x      �          0    21053    prodi 
   TABLE DATA           �   COPY public.prodi (kd_prodi, prodi, singkat, ketua_prodi, nik, akreditasi, max_semester, tgl_insert, tgl_update, id_prodi) FROM stdin;
    public          postgres    false    276   �x      �          0    21059    renungan 
   TABLE DATA           P   COPY public.renungan (id, bacaan, judul, renungan, tanggal, banner) FROM stdin;
    public          postgres    false    277   �y      �          0    21069    ruang_kuliah 
   TABLE DATA           Y   COPY public.ruang_kuliah (id, kode, nama, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    280   �y      �          0    21074    semester 
   TABLE DATA           :   COPY public.semester (semester, dari, sampai) FROM stdin;
    public          postgres    false    281   �y      �          0    21080    setting 
   TABLE DATA           6   COPY public.setting (id, form, tgl_close) FROM stdin;
    public          postgres    false    283   �y      �          0    21086 
   status_mhs 
   TABLE DATA           0   COPY public.status_mhs (id, status) FROM stdin;
    public          postgres    false    285   "z      �          0    21093    tagihan_mhs 
   TABLE DATA           �   COPY public.tagihan_mhs (id, jenis_tagihan_id, nim, kd_prodi, kelas, th_akademik_kode, th_angkatan_kode, smt, jumlah, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    287   ?z      �          0    21103    th_akademik 
   TABLE DATA           |   COPY public.th_akademik (id, kode, th_akademik, semester, keterangan, aktif, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    289   \z      �          0    21115    wisuda 
   TABLE DATA           �   COPY public.wisuda (id_wisuda, th_akademik, tgl_daftar, nim, kd_prodi, judul_skripsi, tgl_sidang, tgl_insert, tgl_update, ipk, acc_akademik, smt, user_id) FROM stdin;
    public          postgres    false    291   yz      �          0    21124    wisuda_validasi 
   TABLE DATA           �   COPY public.wisuda_validasi (id, th_akademik_kode, nim, smt, kelas, kd_prodi, tanggal, keterangan, user_id, insert_date, update_date) FROM stdin;
    public          postgres    false    293   �z      �           0    0    admins_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('pmb.admins_id_seq', 24, true);
          pmb          postgres    false    203            �           0    0    data_keluarga_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('pmb.data_keluarga_id_seq', 229, true);
          pmb          postgres    false    205            �           0    0    data_lain_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('pmb.data_lain_id_seq', 217, true);
          pmb          postgres    false    207            �           0    0    data_pp_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('pmb.data_pp_id_seq', 228, true);
          pmb          postgres    false    209            �           0    0    data_pribadi_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('pmb.data_pribadi_id_seq', 267, true);
          pmb          postgres    false    211            �           0    0    data_rohani_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('pmb.data_rohani_id_seq', 221, true);
          pmb          postgres    false    213            �           0    0    users_id_pmbU_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('pmb."users_id_pmbU_seq"', 354, true);
          pmb          postgres    false    217            �           0    0    admins_id_username_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.admins_id_username_seq', 31, true);
          public          postgres    false    218            �           0    0    admins_new_id_username_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.admins_new_id_username_seq', 3, false);
          public          postgres    false    220            �           0    0    app_setting_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.app_setting_id_seq', 1, false);
          public          postgres    false    223            �           0    0    banner_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.banner_id_seq', 2, true);
          public          postgres    false    225            �           0    0    bayar_mhs_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.bayar_mhs_id_seq', 656, true);
          public          postgres    false    228            �           0    0    dispensasi_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.dispensasi_id_seq', 2, true);
          public          postgres    false    230            �           0    0    info_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.info_id_seq', 14, true);
          public          postgres    false    234            �           0    0    jadwal_id_jadwal_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.jadwal_id_jadwal_seq', 1047, true);
          public          postgres    false    236            �           0    0    jenis_tagihan_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.jenis_tagihan_id_seq', 43, true);
          public          postgres    false    239            �           0    0    krs_id_krs_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.krs_id_krs_seq', 23096, true);
          public          postgres    false    243            �           0    0    materi_kuliah_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.materi_kuliah_id_seq', 28, true);
          public          postgres    false    252            �           0    0    mutasi_mhs_id_mutasi_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.mutasi_mhs_id_mutasi_seq', 43, true);
          public          postgres    false    256            �           0    0    param_nilai_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.param_nilai_id_seq', 1, false);
          public          postgres    false    260            �           0    0    pembayaran_mhs_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.pembayaran_mhs_id_seq', 1, false);
          public          postgres    false    262            �           0    0    pesan_id_pesan_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.pesan_id_pesan_seq', 1, false);
          public          postgres    false    264            �           0    0    poll_jawaban_detail_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.poll_jawaban_detail_id_seq', 17486, true);
          public          postgres    false    268            �           0    0    poll_jawaban_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.poll_jawaban_id_seq', 899, true);
          public          postgres    false    266            �           0    0    poll_jenis_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.poll_jenis_id_seq', 34, true);
          public          postgres    false    270            �           0    0    poll_pertanyaan_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.poll_pertanyaan_id_seq', 45, true);
          public          postgres    false    272            �           0    0    poll_transaksi_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.poll_transaksi_id_seq', 8, true);
          public          postgres    false    274            �           0    0    renungan_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.renungan_id_seq', 2, true);
          public          postgres    false    278                        0    0    ruang_kuliah_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.ruang_kuliah_id_seq', 47, true);
          public          postgres    false    279                       0    0    seq_id_bayar_maba    SEQUENCE SET     @   SELECT pg_catalog.setval('public.seq_id_bayar_maba', 42, true);
          public          postgres    false    226                       0    0    seq_id_dosen    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.seq_id_dosen', 76, true);
          public          postgres    false    232                       0    0    seq_id_jenis_tagihan_maba    SEQUENCE SET     H   SELECT pg_catalog.setval('public.seq_id_jenis_tagihan_maba', 1, false);
          public          postgres    false    241                       0    0    seq_id_master_tagihan    SEQUENCE SET     C   SELECT pg_catalog.setval('public.seq_id_master_tagihan', 8, true);
          public          postgres    false    248                       0    0    seq_id_materi_seminar    SEQUENCE SET     C   SELECT pg_catalog.setval('public.seq_id_materi_seminar', 9, true);
          public          postgres    false    254                       0    0    setting_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.setting_id_seq', 3, false);
          public          postgres    false    282                       0    0    status_mhs_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.status_mhs_id_seq', 1, false);
          public          postgres    false    284                       0    0    tagihan_mhs_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.tagihan_mhs_id_seq', 1, false);
          public          postgres    false    286            	           0    0    th_akademik_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.th_akademik_id_seq', 26, false);
          public          postgres    false    288            
           0    0    wisuda_id_wisuda_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.wisuda_id_wisuda_seq', 12, true);
          public          postgres    false    290                       0    0    wisuda_validasi_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.wisuda_validasi_id_seq', 4, true);
          public          postgres    false    292            �           2606    21139    admins admin_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY pmb.admins
    ADD CONSTRAINT admin_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY pmb.admins DROP CONSTRAINT admin_pkey;
       pmb            postgres    false    204            �           2606    21141     data_keluarga data_keluarga_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY pmb.data_keluarga
    ADD CONSTRAINT data_keluarga_pkey PRIMARY KEY (id);
 G   ALTER TABLE ONLY pmb.data_keluarga DROP CONSTRAINT data_keluarga_pkey;
       pmb            postgres    false    206            �           2606    21143    data_lain data_lain_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY pmb.data_lain
    ADD CONSTRAINT data_lain_pkey PRIMARY KEY (id);
 ?   ALTER TABLE ONLY pmb.data_lain DROP CONSTRAINT data_lain_pkey;
       pmb            postgres    false    208            �           2606    21145    data_pp data_pp_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY pmb.data_pp
    ADD CONSTRAINT data_pp_pkey PRIMARY KEY (id);
 ;   ALTER TABLE ONLY pmb.data_pp DROP CONSTRAINT data_pp_pkey;
       pmb            postgres    false    210            �           2606    21147    data_pribadi data_pribadi_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY pmb.data_pribadi
    ADD CONSTRAINT data_pribadi_pkey PRIMARY KEY (id);
 E   ALTER TABLE ONLY pmb.data_pribadi DROP CONSTRAINT data_pribadi_pkey;
       pmb            postgres    false    212            �           2606    21149    data_rohani data_rohani_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY pmb.data_rohani
    ADD CONSTRAINT data_rohani_pkey PRIMARY KEY (id);
 C   ALTER TABLE ONLY pmb.data_rohani DROP CONSTRAINT data_rohani_pkey;
       pmb            postgres    false    214            �           2606    21151    data_upload data_upload_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY pmb.data_upload
    ADD CONSTRAINT data_upload_pkey PRIMARY KEY (no_pendaftaran);
 C   ALTER TABLE ONLY pmb.data_upload DROP CONSTRAINT data_upload_pkey;
       pmb            postgres    false    215            �           2606    21153    users pmb_user_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY pmb.users
    ADD CONSTRAINT pmb_user_pkey PRIMARY KEY ("id_pmbU");
 :   ALTER TABLE ONLY pmb.users DROP CONSTRAINT pmb_user_pkey;
       pmb            postgres    false    216            �           2606    21155    admins_new admins_new_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY public.admins_new
    ADD CONSTRAINT admins_new_pkey PRIMARY KEY (id_username);
 D   ALTER TABLE ONLY public.admins_new DROP CONSTRAINT admins_new_pkey;
       public            postgres    false    221            �           2606    21157    admins admins_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id_username);
 <   ALTER TABLE ONLY public.admins DROP CONSTRAINT admins_pkey;
       public            postgres    false    219            �           2606    21159    app_setting app_setting_pk 
   CONSTRAINT     X   ALTER TABLE ONLY public.app_setting
    ADD CONSTRAINT app_setting_pk PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.app_setting DROP CONSTRAINT app_setting_pk;
       public            postgres    false    222            �           2606    21161    banner banner_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.banner
    ADD CONSTRAINT banner_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.banner DROP CONSTRAINT banner_pkey;
       public            postgres    false    224            �           2606    21163    bayar_maba bayar_maba_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.bayar_maba
    ADD CONSTRAINT bayar_maba_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.bayar_maba DROP CONSTRAINT bayar_maba_pkey;
       public            postgres    false    227            �           2606    21165    bayar_mhs bayar_mhs_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.bayar_mhs
    ADD CONSTRAINT bayar_mhs_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.bayar_mhs DROP CONSTRAINT bayar_mhs_pkey;
       public            postgres    false    229            �           2606    21167    dispensasi dispensasi_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.dispensasi
    ADD CONSTRAINT dispensasi_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.dispensasi DROP CONSTRAINT dispensasi_pkey;
       public            postgres    false    231            �           2606    21169    dosen dosen_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.dosen
    ADD CONSTRAINT dosen_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.dosen DROP CONSTRAINT dosen_pkey;
       public            postgres    false    233            �           2606    21171    info info_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.info
    ADD CONSTRAINT info_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.info DROP CONSTRAINT info_pkey;
       public            postgres    false    235            �           2606    21173    jadwal jadwal_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.jadwal
    ADD CONSTRAINT jadwal_pkey PRIMARY KEY (id_jadwal);
 <   ALTER TABLE ONLY public.jadwal DROP CONSTRAINT jadwal_pkey;
       public            postgres    false    237            �           2606    21175    jam_kuliah jam_kuliah_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.jam_kuliah
    ADD CONSTRAINT jam_kuliah_pkey PRIMARY KEY (kode);
 D   ALTER TABLE ONLY public.jam_kuliah DROP CONSTRAINT jam_kuliah_pkey;
       public            postgres    false    238            �           2606    21177 *   jenis_tagihan_maba jenis_tagihan_maba_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.jenis_tagihan_maba
    ADD CONSTRAINT jenis_tagihan_maba_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.jenis_tagihan_maba DROP CONSTRAINT jenis_tagihan_maba_pkey;
       public            postgres    false    242            �           2606    21179     jenis_tagihan jenis_tagihan_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.jenis_tagihan
    ADD CONSTRAINT jenis_tagihan_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.jenis_tagihan DROP CONSTRAINT jenis_tagihan_pkey;
       public            postgres    false    240            �           2606    21181    krs krs_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.krs
    ADD CONSTRAINT krs_pkey PRIMARY KEY (id_krs);
 6   ALTER TABLE ONLY public.krs DROP CONSTRAINT krs_pkey;
       public            postgres    false    244            �           2606    21183 4   mahasiswa_aktif_history mahasiswa_aktif_history_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.mahasiswa_aktif_history
    ADD CONSTRAINT mahasiswa_aktif_history_pkey PRIMARY KEY (th_akademik, nim, date_created);
 ^   ALTER TABLE ONLY public.mahasiswa_aktif_history DROP CONSTRAINT mahasiswa_aktif_history_pkey;
       public            postgres    false    246    246    246            �           2606    21185 &   mahasiswa_backup mahasiswa_backup_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.mahasiswa_backup
    ADD CONSTRAINT mahasiswa_backup_pkey PRIMARY KEY (nim);
 P   ALTER TABLE ONLY public.mahasiswa_backup DROP CONSTRAINT mahasiswa_backup_pkey;
       public            postgres    false    247            �           2606    21187    mahasiswa mahasiswa_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.mahasiswa
    ADD CONSTRAINT mahasiswa_pkey PRIMARY KEY (nim);
 B   ALTER TABLE ONLY public.mahasiswa DROP CONSTRAINT mahasiswa_pkey;
       public            postgres    false    245            �           2606    21189 ,   master_tagihan_maba master_tagihan_maba_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.master_tagihan_maba
    ADD CONSTRAINT master_tagihan_maba_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.master_tagihan_maba DROP CONSTRAINT master_tagihan_maba_pkey;
       public            postgres    false    250            �           2606    21191 "   master_tagihan master_tagihan_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.master_tagihan
    ADD CONSTRAINT master_tagihan_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.master_tagihan DROP CONSTRAINT master_tagihan_pkey;
       public            postgres    false    249            �           2606    21193    mata_kuliah mata_kuliah_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.mata_kuliah
    ADD CONSTRAINT mata_kuliah_pkey PRIMARY KEY (kd_mk);
 F   ALTER TABLE ONLY public.mata_kuliah DROP CONSTRAINT mata_kuliah_pkey;
       public            postgres    false    251            �           2606    21195     materi_kuliah materi_kuliah_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.materi_kuliah
    ADD CONSTRAINT materi_kuliah_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.materi_kuliah DROP CONSTRAINT materi_kuliah_pkey;
       public            postgres    false    253            �           2606    21197 "   materi_seminar materi_seminar_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.materi_seminar
    ADD CONSTRAINT materi_seminar_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.materi_seminar DROP CONSTRAINT materi_seminar_pkey;
       public            postgres    false    255            �           2606    21199    mutasi_mhs mutasi_mhs_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.mutasi_mhs
    ADD CONSTRAINT mutasi_mhs_pkey PRIMARY KEY (id_mutasi);
 D   ALTER TABLE ONLY public.mutasi_mhs DROP CONSTRAINT mutasi_mhs_pkey;
       public            postgres    false    257            �           2606    21201 $   nim_last_number nim_last_number_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY public.nim_last_number
    ADD CONSTRAINT nim_last_number_pkey PRIMARY KEY (no_id);
 N   ALTER TABLE ONLY public.nim_last_number DROP CONSTRAINT nim_last_number_pkey;
       public            postgres    false    259            �           2606    21203    param_nilai param_nilai_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.param_nilai
    ADD CONSTRAINT param_nilai_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.param_nilai DROP CONSTRAINT param_nilai_pkey;
       public            postgres    false    261            �           2606    21205 "   pembayaran_mhs pembayaran_mhs_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.pembayaran_mhs
    ADD CONSTRAINT pembayaran_mhs_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.pembayaran_mhs DROP CONSTRAINT pembayaran_mhs_pkey;
       public            postgres    false    263            �           2606    21207    pesan pesan_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.pesan
    ADD CONSTRAINT pesan_pkey PRIMARY KEY (id_pesan);
 :   ALTER TABLE ONLY public.pesan DROP CONSTRAINT pesan_pkey;
       public            postgres    false    265            �           2606    21209 ,   poll_jawaban_detail poll_jawaban_detail_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.poll_jawaban_detail
    ADD CONSTRAINT poll_jawaban_detail_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.poll_jawaban_detail DROP CONSTRAINT poll_jawaban_detail_pkey;
       public            postgres    false    269            �           2606    21211    poll_jawaban poll_jawaban_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.poll_jawaban
    ADD CONSTRAINT poll_jawaban_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.poll_jawaban DROP CONSTRAINT poll_jawaban_pkey;
       public            postgres    false    267            �           2606    21213    poll_jenis poll_jenis_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.poll_jenis
    ADD CONSTRAINT poll_jenis_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.poll_jenis DROP CONSTRAINT poll_jenis_pkey;
       public            postgres    false    271            �           2606    21215 $   poll_pertanyaan poll_pertanyaan_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.poll_pertanyaan
    ADD CONSTRAINT poll_pertanyaan_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.poll_pertanyaan DROP CONSTRAINT poll_pertanyaan_pkey;
       public            postgres    false    273            �           2606    21217 "   poll_transaksi poll_transaksi_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.poll_transaksi
    ADD CONSTRAINT poll_transaksi_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.poll_transaksi DROP CONSTRAINT poll_transaksi_pkey;
       public            postgres    false    275            �           2606    21219    prodi prodi_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.prodi
    ADD CONSTRAINT prodi_pkey PRIMARY KEY (kd_prodi);
 :   ALTER TABLE ONLY public.prodi DROP CONSTRAINT prodi_pkey;
       public            postgres    false    276            �           2606    21221    renungan renungan_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.renungan
    ADD CONSTRAINT renungan_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.renungan DROP CONSTRAINT renungan_pkey;
       public            postgres    false    277            �           2606    21223    ruang_kuliah ruang_kuliah_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ruang_kuliah
    ADD CONSTRAINT ruang_kuliah_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.ruang_kuliah DROP CONSTRAINT ruang_kuliah_pkey;
       public            postgres    false    280            �           2606    21225    setting setting_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.setting
    ADD CONSTRAINT setting_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.setting DROP CONSTRAINT setting_pkey;
       public            postgres    false    283            �           2606    21227    status_mhs status_mhs_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.status_mhs
    ADD CONSTRAINT status_mhs_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.status_mhs DROP CONSTRAINT status_mhs_pkey;
       public            postgres    false    285            �           2606    21229    tagihan_mhs tagihan_mhs_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.tagihan_mhs
    ADD CONSTRAINT tagihan_mhs_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.tagihan_mhs DROP CONSTRAINT tagihan_mhs_pkey;
       public            postgres    false    287            �           2606    21231    th_akademik th_akademik_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.th_akademik
    ADD CONSTRAINT th_akademik_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.th_akademik DROP CONSTRAINT th_akademik_pkey;
       public            postgres    false    289            �           2606    21233    wisuda wisuda_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.wisuda
    ADD CONSTRAINT wisuda_pkey PRIMARY KEY (id_wisuda);
 <   ALTER TABLE ONLY public.wisuda DROP CONSTRAINT wisuda_pkey;
       public            postgres    false    291            �           2606    21235 $   wisuda_validasi wisuda_validasi_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.wisuda_validasi
    ADD CONSTRAINT wisuda_validasi_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.wisuda_validasi DROP CONSTRAINT wisuda_validasi_pkey;
       public            postgres    false    293                        2606    21236 %   renungan fk_482tc2vwa79h5mkxhfx0jpeie    FK CONSTRAINT     �   ALTER TABLE ONLY public.renungan
    ADD CONSTRAINT fk_482tc2vwa79h5mkxhfx0jpeie FOREIGN KEY (banner) REFERENCES public.banner(id);
 O   ALTER TABLE ONLY public.renungan DROP CONSTRAINT fk_482tc2vwa79h5mkxhfx0jpeie;
       public          postgres    false    3253    224    277            �   �   x���Aj�0E��)r��l%�W9�l��q��8���7����
������I[T4A�d�IT�1s����`��懬�_Z�K>��0(�J��@f8Wd͋��Kj�Ř�H����X|b�溫��o�H):/O2�A_���獨&,�T䌎e^MrIw�e_cJ��Ա�K�^�����N��?��{���W����2�����o��7�����X]4      �   �   x���ͪ�0�ד������?[u�U�[�n�{Kj����7j�.l�9a��L�HI�� 	����o�����HM�������zLG��;�PR�FFɘd�'��0�{��I<&�I���=f�X\���
��w`����?�̒o�L��4Kt���V�l[,�ہږ�+6Xܸ��?��G�9jŐ7���ߢ�>7'�P
�:�`�3㚌��0[��-�@qyx      �   Z   x����� г����AM���?G5Z�\>���Z�R����aA^�6��Y�E
��ZP���I�%̅EqaAQ��z�M�-"�x:Q      �   �   x���M�0���~���u���"(t�h��
�c?���":D�ƶg��0"�B�H����u�� Aą�"��"b�Z�����GK�Zȇ%^,��H���YB����KM�{mze���%�:+�Зi�P�7=Ҍ����J���Vc��"��`�CA�`�毣�:��A<�㵿i�lK���ge��[�n�ܲv�{ ����      �   w  x��UMo�0=�_1�]	X{�÷V{�h����l����`U��q)d�J��J��Ǟ���%�Rq�2{�.��9�����tr�I�PFͼ��o��W0����6��h]��[L���a�Cj�>�v��ֺ�ɬ��	��F�Rtm�)�X���u���g���5�w-���!/Q���&�V�����|�����Ce־4ng��+�dʝ��2;B�< ���s�õ}^��O�nƂK���f����؅��u�-ʅ�2��f[���E釰�TEU�5�5��@�-�d2��x*d�$	r�{Ύ�0�yYc��
����H"���������#2�kk����6�1�^�Dq����8t�Q\�Qܬl�	z:Q�!��7�i!�T][��x`�d��7�<�#���Y�^w���?)�D��p�%L5m��f\�t)��C�C��g+?��Z�][�Ք8n�G�f��=����T�]��]2QPč(ւ����҆轶�Nf9��	�(�iPR��)
�P)ڨ� LA�B6�u���/���G�tãmx�`(���Yw�1<!τ8��s�?lN.�_L�3gS�t&+S����UE�!�sFg��2�쿌��2ւwl���e$��x0�����      �   ^   x����� �3م�8|����� �6jo�HQ.OVH*�	���k� =�#�v9�$|T�P�U�l�b]\Qy�Ӕn�j*���I�yE׻�A�!"H|0      �   �   x�}�Kj�0���)r�=F�P(��ɕ%ndl���W5}x�f!F��?8�@((NO�Ás��<@c�6���_0��2J�w��:K��W�}��i�RY�8$I�e
T�T
���=�c|�!�]|m��]��8�9܆v��_n����|��!�{���3��3�1���=,����{XރEno�aI��N�"C ���j �v���[������G;�����y��q\�4�%l�?���H�U?%OUY�y�u�      �   g  x�u�9n1Ek�0E��ԥN��!�8���K1��<7*��E
ɽ�����e���}�t�_��I������r>9p�	�ƫf1q�d�M��Ɛ
`/��ȯgG!��e:���x�
>�1�M��L����9��ܧ.�b�7"��Cиv�X��T�y�b$ܢ�>?T����y��k�м%�	JW��$�������e�U���η�U<:,��p����g�H�"<���[���2Zr!_�uO1Ơ��B��G�}Ŗ�\֚E�Hf���4b �"]X�|�}���r��	4Kd��Z+�[m�9@F֜��W�;,�e��n�����H��Z�M9���K���D�O����~ 	��%      �   [   x�36�LL��̋O/��M�K,O�L54HLI6�4�LJ4�LLJJ55K505O32H��0N�tiPp�k(.-H-R ������㇌�b���� 	��      �   w   x���K
�0D��a�-˕�����P��$�?mhPf�����x^u���3k�D��dR�I��.�vl���k��0��7(�����/���dI����--q�U�������9���0+      �     x��AO�@��˯���fa)P��=4���H/&&f�[Xaw	,1����܌z��wy����<F2�[���xJ֐h+:�ɶ��-��19�q<�w��$;n�w�[T�A�I%4�!����!���o�7�̠����Q�XB*

���'̇��S���0*�f�
��~b�o�߸��Iem�_���<u�U;4oLٷ���(�������A�7���8���β/,
���c,��.�]�E��׶$��������s�`���L�yp���      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   <   x�3�M�KWp,.J�M�2���R�3�K��3��!"ޥ9��\�>��yy��\1z\\\ P�l      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x�u��j�0�g�)����%��)2�nr�,�X8jm�H��Wu����~?��DsP������	6.���>��Jh���-���=�A8�6Gsi[R򭦺��[�c����	�� 5�>���L���J&����ꉓ�K�������vU����i̜t��ѳ}�~P�p u�yn�n �1qq���6_�g󊉍}"Z���"�ɺ�����f����L�7?����}�Ǣ(� ћx�      �      x������ � �      �      x������ � �      �      x������ � �      �      x�3��
�4202�50�50����� 0G�      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �     