<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_polling extends CI_Model {

	/**
	 * @author : Deddy Rusdiansyah
	 * @web : http://deddyrusdiansyah.blogspot.com
	 * @keterangan : Model untuk menangani semua query database aplikasi
	 **/

   public function getPertanyaan($th_akademik,$poll_jenis_id,$poll_pertanyaan_id)
   {
     $this->db->where('th_akademik',$th_akademik);
     $this->db->where('poll_jenis_id',$poll_jenis_id);
    //  $this->db->where('poll_pertanyaan_id',$poll_pertanyaan_id);
     $get = $this->db->get('poll_transaksi');
     if($get->num_rows()>0)
     {
       $row = $get->row();
       $pertanyaan = explode(',',$row->poll_pertanyaan_id);
       if(in_array($poll_pertanyaan_id, $pertanyaan))
       {
         $hasil = 1;
       }else{
         $hasil = 0;
       }
     }else{
       $hasil = 0;
     }
     return $hasil;
   }

	public function  getInfoPertanyaan($id)
	{
		$this->db->where('id',(int)$id);
		$get = $this->db->get('poll_pertanyaan');
		if($get->num_rows()>0)
		{
			$row = $get->row();
			$hasil = array(
				'pertanyaan' => $row->pertanyaan,
				'pilihan' => $row->pilihan,
				'poll_jenis_id' => $row->poll_jenis_id
			);

		}else{
			$hasil = array(
				'pertanyaan' => '',
				'pilihan' => '',
				'poll_jenis_id' => ''
			);
		}
		return $hasil;
	}

	public function getDataPertanyaan($th_akademik,$jenis)
	{
		$this->db->where('a.th_akademik',$th_akademik);
		$this->db->where('b.nama',$jenis);
		$this->db->from('poll_transaksi as a');
		$this->db->join('poll_jenis as b','a.poll_jenis_id=b.id');
		return $this->db->get();
	}

	public function getIDJenis($nama)
	{
		$this->db->where('nama',$nama);
		$get = $this->db->get('poll_jenis');
		if($get->num_rows()>0)
		{
			$row = $get->row();
			$hasil = array(
				'id' => $row->id,
				'nama' => $row->nama
			);
		}else{
			$hasil = array(
				'id' => '',
				'nama' => ''
			);
		}
		return $hasil;
	}

	public function getIDJawaban($keyth)
	{
		// print_r($keyth);die;
		$get = $this->db->get_where('poll_jawaban',$keyth);
		if($get->num_rows()>0)
		{
				$row = $get->row();
				$id = $row->id;
		}else{
			$id = '';
		}
		return $id;
	}

}
