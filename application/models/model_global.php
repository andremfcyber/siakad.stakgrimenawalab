<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_global extends CI_Model {

	/**
	 * @author : Deddy Rusdiansyah
	 * @web : http://deddyrusdiansyah.blogspot.com
	 * @keterangan : Model untuk menangani semua query database aplikasi
	 **/

	function __construct(){

		@session_start();

	}

	public function getAllData($table)
	{
		return $this->db->get($table);
	}

	public function getAllDataLimited($table,$limit,$offset)
	{
		return $this->db->get($table, $limit, $offset);
	}

	public function getSelectedDataLimited($table,$data,$limit,$offset)
	{
		return $this->db->get_where($table, $data, $limit, $offset);
	}

	//select table
	public function getSelectedData($table,$data)
	{
		return $this->db->get_where($table, $data);
	}

	//update table
	function updateData($table,$data,$field_key)
	{
		$this->db->update($table,$data,$field_key);
	}
	function deleteData($table,$data)
	{
		$this->db->delete($table,$data);
	}

	function insertData($table,$data)
	{
		$this->db->insert($table,$data);
	}

	//Query manual
	function manualQuery($q)
	{
		return $this->db->query($q);
	}

	function cari_max_mutasi_mhs(){
		$q = $this->db->query("SELECT MAX(id_mutasi) as no FROM mutasi_mhs");
		foreach($q->result() as $dt){
			$no = (int) $dt->no+1;
		}
		return $no;
	}

	function cari_max_wisuda_mhs(){
		$q = $this->db->query("SELECT MAX(id_wisuda) as no FROM wisuda");
		foreach($q->result() as $dt){
			$no = (int) $dt->no+1;
		}
		return $no;
	}

	public function cari_semester(){
		date_default_timezone_set('Asia/Jakarta');
		$bln = date('m');

		switch ($bln){
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				return "genap";
				break;
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 1:
				return "ganjil";
				break;
		}

	}

	public function max_sks($ip){

		// if($ip>=3.00){
		// 	$sks = 24;
		// }elseif($ip>=2.50){
		// 	$sks = 22;
		// }elseif($ip>=2.00){
		// 	$sks = 20;
		// }elseif($ip>=1.50){
		// 	$sks = 16;
		// }elseif($ip>=1.00){
		// 	$sks = 14;
		// }else{
		// 	$sks = 24;
		// }
		$sks = 24;
		return $sks;
		/*
		switch($ip){
			case 3.00:
				return 24;
				break;
			case 2.50:
				return 22;
				break;
			case 2.00:
				return 20;
				break;
			case 1.50:
				return 16;
				break;
			case 1.00:
				return 14;
				break;
			case 0.00:
				return 12;
				break;
		}
		*/

	}

	function cari_th_akademik(){
		date_default_timezone_set('Asia/Jakarta');
		$th = date('Y'); //ini tahun berjalan bisa diganti dengan tahun dimaksud

		$smt = $this->model_global->cari_semester();
		if($smt=='ganjil'){
			$ket = 1;
		}else{
			$ket = 2;
		}
		$hasil = $th.$ket;

		return $hasil;
	}

	/*
	function semester($nim){
		$id['nim'] = $nim;
		$q = $this->db->get_where("mahasiswa",$id);
		$r = $q->num_rows();
		if($r>0){
			foreach($q->result() as $dt){
				$th = substr($dt->th_akademik,0,4);
				$bln_now = date('m');
				$th_now = date('Y');
				$thn = $th_now-$th;
				if($thn >1 && $bln_now >=2){
					$smt = ($thn*2)+1;
				}elseif($thn >1 && $bln_now >=8){
					$smt = ($thn*2)+2;
				}else{
					$smt = 1;
				}
			}
		}else{
			$smt = 1;
		}
		return $smt;
	}
	*/
	function getSMT($nim,$thak){
		$id['nim'] = $nim;
		$id['th_akademik'] = $thak;
		$this->db->select('smt');
		$q = $this->db->get_where("krs",$id);
		$r = $q->num_rows();
		if($r>0){
			return $q->row()->smt;
		}else{
			$hasil = 1;
		}
		return $hasil;
	}

	function semester($nim,$thak){
		$thak = $this->model_global->getThAkademikAktif()['kode'];
		$id['nim'] = $nim;
		$this->db->select('th_akademik,nim');
		$q = $this->db->get_where("mahasiswa",$id);
		$r = $q->num_rows();
		if($r>0){
			foreach($q->result() as $dt){
				date_default_timezone_set('Asia/Jakarta');
				$th_now = substr($thak,0,4);//date('Y');
				$th_masuk = substr($dt->th_akademik,0,4);
				//$smt_masuk = substr($dt->th_akademik,4,1);
				$smt = substr($thak,4,1);
				$th = $th_now-$th_masuk;
				$hasil =  ($th*2)+$smt;
			}
		}else{
			$hasil = 1;
		}
		return $hasil;
	}
	//Konversi tanggal
	public function tgl_sql($date){
		$exp = explode('-',$date);
		if(count($exp) == 3) {
			$date = $exp[2].'-'.$exp[1].'-'.$exp[0];
		}
		return $date;
	}

	public function tgl_str($date){
		$exp = explode('-',$date);
		if(count($exp) == 3) {
			$date = $exp[2].'-'.$exp[1].'-'.$exp[0];
		}
		return $date;
	}

	public function ambilTgl($tgl){
		$exp = explode('-',$tgl);
		$tgl = $exp[2];
		return $tgl;
	}

	public function ambilBln($tgl){
		$exp = explode('-',$tgl);
		$tgl = $exp[1];
		$bln = $this->model_global->getBulan($tgl);
		$hasil = substr($bln,0,3);
		return $hasil;
	}

	public function tgl_indo($tgl){
			$jam = substr($tgl,11,10);
			$tgl = substr($tgl,0,10);
			$tanggal = substr($tgl,8,2);
			$bulan = $this->model_global->getBulan(substr($tgl,5,2));
			$tahun = substr($tgl,0,4);
			return $tanggal.' '.$bulan.' '.$tahun.' '.$jam;
	}

	public function getBulan($bln){
		switch ($bln){
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}

	public function hari_ini($hari){
		date_default_timezone_set('Asia/Jakarta'); // PHP 6 mengharuskan penyebutan timezone.
		$seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
		//$hari = date("w");
		$hari_ini = $seminggu[$hari];
		return $hari_ini;
	}

	//query login
	public function getLoginData($usr,$psw)
	{
		//mysql_real_escape_string
		$u = $usr;
		$p = md5($psw);
		$q_cek_login = $this->db->get_where('admins', array('username' => $u, 'password' => $p,'blokir'=>'Tidak'));
		if(count($q_cek_login->result())>0)
		{
			foreach($q_cek_login->result() as $qck)
			{
					foreach($q_cek_login->result() as $qad)
					{

						$_SESSION['logged_in'] = 'getLoginSIAKAD_online';
						$_SESSION['username'] = $qad->username;
						$_SESSION['nama_lengkap'] = $qad->nama_lengkap;
						$_SESSION['level'] = 'admin';
						$_SESSION['role'] = $qad->level;
						$_SESSION['adm_kd_prodi'] = @$qad->kd_prodi;
						// $sess_data['logged_in'] = 'getLoginSIAKAD_online';
						// $sess_data['username'] = $qad->username;
						// $sess_data['nama_lengkap'] = $qad->nama_lengkap;
						// $sess_data['level'] = 'admin';
						// $this->session->set_userdata($sess_data);
					}
					header('location:'.base_url().'index.php/home');
			}
		}else{
			/**login Mahasiswa **/
			$u = $usr;
			$p = md5($psw);
			//$p = $psw;
			$s = array('Aktif','Lulus','Cuti');
			//$l = 'Lulus';
			$this->db->where('nim',$u);
			$this->db->where('password',$p);
			$this->db->where_in('status',$s);
			//$this->db->or_where('status','Lulus');
			$q_mhs = $this->db->get('mahasiswa');
			//$q_mhs = $this->db->get_where('mahasiswa', array('nim' => $u, 'password' => $p,'status'=>$s, 'status'=>'Lulus'));
			if($q_mhs->num_rows()>0){
				foreach($q_mhs->result() as $dt){
					$_SESSION['logged_in'] = 'getLoginSIAKAD_online';
					$_SESSION['username'] = $dt->nim;
					$_SESSION['nama_lengkap'] = $dt->nama_mhs;
					$_SESSION['kd_prodi'] = $dt->kd_prodi;
					$_SESSION['level'] = 'mahasiswa';
					$_SESSION['status'] = $dt->status;
					$_SESSION['adm_kd_prodi'] = null;

					// $sess_data['logged_in'] = 'getLoginSIAKAD_online';
					// $sess_data['username'] = $dt->nim;
					// $sess_data['nama_lengkap'] = $dt->nama_mhs;
					// $sess_data['kd_prodi'] = $dt->kd_prodi;
					// $sess_data['level'] = 'mahasiswa';
					// $sess_data['status'] = $dt->status;
					// $this->session->set_userdata($sess_data);
				}
				header('location:'.base_url().'index.php/site_mahasiswa/home');
			}else{
				/*** Login Dosen ***/
				$u = $usr;
				$p = md5($psw);
				$s = 'Aktif';
				$i = 0;
				$_SESSION['kd_prodi'] = [];
				//$l = 'Lulus';
				$q_mhs = $this->db->get_where('dosen', array('kd_dosen' => $u, 'password' => $p,'status'=>$s));
				if($q_mhs->num_rows()>0){
					foreach($q_mhs->result() as $dt){
						$_SESSION['logged_in'] = 'getLoginSIAKAD_online';
						$_SESSION['username'] = $dt->kd_dosen;
						$_SESSION['nama_lengkap'] = $dt->nama_dosen;
						$_SESSION['kd_prodi'][$i] = $dt->kd_prodi;
						$_SESSION['level'] = 'dosen';
						$_SESSION['adm_kd_prodi'] = null;
						$i=$i+1;
						// $sess_data['logged_in'] = 'getLoginSIAKAD_online';
						// $sess_data['username'] = $dt->kd_dosen;
						// $sess_data['nama_lengkap'] = $dt->nama_dosen;
						// $sess_data['kd_prodi'] = $dt->kd_prodi;
						// $sess_data['level'] = 'dosen';
						// $this->session->set_userdata($sess_data);
					}
					header('location:'.base_url().'index.php/site_dosen/home');
				}else{

					$this->session->set_flashdata('result_login', '<br>Username / Kode Dosen / NIM atau Password yang anda masukkan salah. Atau Akun Anda diblokir. Silahkan Hubungi Administrator.');
					header('location:'.base_url().'index.php/login');
				}
			}
		}
	}


	public function getThAkademikAktif()
	{
		// $this->db->select('kode');
		$this->db->where('aktif','Ya');
		$this->db->order_by('id','DESC');
		$this->db->limit(1);
		$data = $this->db->get('th_akademik');
		if($data->num_rows()>0)
		{
			$row = $data->row();
			$hasil = array('kode' => $row->kode,
									'th_akademik' => $row->th_akademik,
								'semester' => $row->semester);
		}else{
			$hasil = array('kode' => '',
									'th_akademik' => '',
									'semester' => ''
								);
		}
		return $hasil;
	}

	/*fungsi terbilang*/
public function bilang($x) {
	$x = abs($x);
	$angka = array("", "Satu", "Dua", "Tiga", "Empat", "Lima",
	"Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
	$result = "";
	if ($x <12) {
		$result = " ". $angka[$x];
	} else if ($x <20) {
		$result = $this->model_global->bilang($x - 10). " Belas";
	} else if ($x <100) {
		$result = $this->model_global->bilang($x/10)." Puluh". $this->model_global->bilang($x % 10);
	} else if ($x <200) {
		$result = " Seratus" . $this->model_global->bilang($x - 100);
	} else if ($x <1000) {
		$result = $this->model_global->bilang($x/100) . " Ratus" . $this->model_global->bilang($x % 100);
	} else if ($x <2000) {
		$result = " Seribu" . $this->model_global->bilang($x - 1000);
	} else if ($x <1000000) {
		$result = $this->model_global->bilang($x/1000) . " Ribu" . $this->model_global->bilang($x % 1000);
	} else if ($x <1000000000) {
		$result = $this->model_global->bilang($x/1000000) . " Juta" . $this->model_global->bilang($x % 1000000);
	} else if ($x <1000000000000) {
		$result = $this->model_global->bilang($x/1000000000) . " Milyar" . $this->model_global->bilang(fmod($x,1000000000));
	} else if ($x <1000000000000000) {
		$result = $this->model_global->bilang($x/1000000000000) . " Trilyun" . $this->model_global->bilang(fmod($x,1000000000000));
	}
		return $result;
}
public function terbilang($x, $style=4) {
	if($x<0) {
		$hasil = "minus ". trim($this->model_global->bilang($x));
	} else {
		$hasil = trim($this->model_global->bilang($x));
	}
	switch ($style) {
		case 1:
			$hasil = strtoupper($hasil);
			break;
		case 2:
			$hasil = strtolower($hasil);
			break;
		case 3:
			$hasil = ucwords($hasil);
			break;
		default:
			$hasil = ucfirst($hasil);
			break;
	}
	return $hasil;
}

public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

public function Qsql_data_exist($wHere, $fieLd, $tabLe){

	$q = $this->db->query("SELECT $fieLd FROM $tabLe WHERE $wHere");

   if ($q->num_rows() > 0){
   	return true;
   }else{
   	return false;
   }
}

public function MaxFromTable($fieLd, $tabLe){
//       echo "A";
	$q = $this->db->query("select max($fieLd) as no from $tabLe");
		foreach($q->result() as $dt){
			$no = (int) $dt->no+1;
		}
		return $no;
}

public function getToken(){
	$data =array('act'		=> 'GetToken',
					 'username'	=> '233004',
					 'password' => '20120887'
					);

   $ctype = 'json';
	$result_string = $this->runWS($data, $ctype);

	if (strstr($result_string, '<?xml')) {
		$result = simplexml_load_string($result_string);
		$result = json_decode(json_encode($result), true);
	}else
		$result = json_decode($result_string, true);

	$_SESSION['token'] = $result['data']['token'];
   $token  = $result['data']['token'];

   return $token;
}

public function runWS($data, $type='json') {
	$live = 'Y';

	if ($live=='Y') {
//		$url = 'http://localhost:8082/ws/live2.php'; // gunakan live
		$url = 'http://103.19.109.253:8082/ws/live2.php'; // gunakan live
	} else {
//		$url = 'http://localhost:8082/ws/sandbox2.php'; // gunakan sandbox
		$url = 'http://103.19.109.253:8082/ws/sandbox2.php'; // gunakan sandbox
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 1);
	$headers = array();

	if ($type == 'xml')
		$headers[] = 'Content-Type: application/xml';
	else
		$headers[] = 'Content-Type: application/json';

	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	if ($data) {
		if ($type == 'xml') {
			/* contoh xml:    <?xml version="1.0"?><data><act>GetToken</act><username>agus</username><password>abcdef</password> </data>    */

			$data = stringXML($data);
		}  else {
			/* contoh json:    {"act":"GetToken","username":"agus","password":"abcdef"}    */

			$data = json_encode($data);
		}


		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	}

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

if(curl_errno($ch))
    echo 'Curl error: '.curl_error($ch);

	$result = curl_exec($ch);
	print_r($result);

	curl_close($ch);

	return $result;
}

}

/* End of file app_model.php */
/* Location: ./application/models/app_model.php */
