<?php 

    if (!function_exists('app_setting')) {
        function app_setting() {
            $CI = get_instance();
            $CI->load->model('model_appsetting');
            $d['data_setting'] = $CI->model_appsetting->get_setting()->result();
            return $d;
        }
    }

?>