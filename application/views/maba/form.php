<script type="text/javascript">
$(document).ready(function(){
	$(".chzn-select").chosen();

	$("#cari_no_pendaftaran").change(function(){
		var no_pendaftaran = $("#cari_no_pendaftaran").val();
		//alert(no_pendaftaran);
		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/maba/cari_mhs",
			data	: "no_pendaftaran="+no_pendaftaran,
			cache	: false,
			success	: function(data){
				$("#info_mhs").html(data);
			}
		});
	});
});
</script>


<div class="widget-box">
  <div class="widget-header">
    <h4><i class="icon-user"></i> <?php echo $judul;?></h4>
  </div>
  <div class="widget-body">
    <div class="widget-main">
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-header">
              <h4 class="smaller">Filter Jurusan</h4>
            </div>

            <div class="widget-body">
             <div class="widget-main">

                <form action="<?php echo site_url();?>/maba/view_data" method="post">
                  <?php if($_SESSION['adm_kd_prodi']==null){?>
                    <label class="control-label" for="form-field-1">Filter Jurusan</label>
                    <div class="controls">
                      <select name="cari_jurusan" id="cari_jurusan">
                      <?php
                        $data = $this->model_data->data_jurusan();
                        foreach($data->result() as $dt){
                      ?>
                         <option value="<?php echo $dt->kd_prodi;?>"><?php echo $dt->prodi;?></option>
                      <?php } ?>
                      </select>
                  <?php 
                    }else{
                        $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                    ?>
                        <label class="control-label" for="form-field-1">Jurusan</label>
                        <div class="controls">
                        <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                        <input type="hidden" name="cari_jurusan" id="cari_jurusan" value="<?php echo $data->row()->kd_prodi;?>" placeholder="<?php echo $data->row()->prodi;?>">
                    <?php }?>
                      <button type="submit" name="lanjut" id="lanjut" class="btn btn-small btn-success" > 
                     	  Lanjut
                        <i class="icon-arrow-right icon-on-right bigger-110"></i>
                      </button>
                    </div>
                </form>

              </div>
            </div>
          </div>
        </div><!--/span-->


        <div class="span6">
          <div class="widget-box">
            <div class="widget-header">
              <h4 class="smaller">Pencarian Mahasiswa Baru</h4>
            </div>
            <div class="widget-body">
              <div class="widget-main">
                <label for="form-field-select-1">Masukan No. Pendaftaran</label>
                <select class="chzn-select span4" data-placeholder="Cari No. Pendaftaran ...." name="cari_no_pendaftaran" id="cari_no_pendaftaran" >
                  <option value="">Cari No. Pendaftaran ....</option>
                  <?php
                    $data = $this->db->get_where('pmb.users', array('done' =>1, 'delete' =>0, 'status_bayar' =>1, 'mhs' =>0));
                    foreach($data->result() as $dt){
                  ?>
                  <option value="<?php echo $dt->no_pendaftaran;?>"><?php echo $dt->no_pendaftaran;?></option>
                  <?php } ?>
                </select>

              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</div><!--/span-->

<div id="info_mhs"></div>
