<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Login Page - <?php echo app_setting()['data_setting'][0]->nama_aplikasi;?></title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!--basic styles-->
		<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>assets/css/bootstrap2.min.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!--page specific plugin styles-->
		<!--fonts-->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-skins.min.css" />
		<link href="https://fonts.googleapis.com/css?family=Belleza" rel="stylesheet">
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->
		<!--inline styles related to this page-->
        <script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
		<!--<![endif]-->
		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			$(document).ready(function(){
				$('#username').focus();
			});
			if("ontouchend" in document) document.write("<script src='<?php echo base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
        <!--page specific plugin scripts-->
		<!--ace scripts-->
		<script src="<?php echo base_url();?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace.min.js"></script>

		<!--CUSTOM CSS-->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" />
		<style>
		.login-layout {
			background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_login ?>) !important;
			background-size:cover;
			background-repeat: no-repeat;
			height: 100vh;
		}
		</style>
	<!-- </head> -->
	</head>
	<body class="login-layout">
		<div class="main-container container-fluid center-vertical">
			<div class="main-content">
				<div class="row">
					<div class="login-container">
						<div class="row">
						</div>
						<div class="row justify-content-sm-center">
							<div class="col-sm-12 login-box-custom border-login">
								<div class="row">
								<div class="col-sm-6 center right-line">

								<img src="<?php echo base_url();?>/assets/app_setting_upload/img/<?php echo app_setting()['data_setting'][0]->logo ?>" class="w-60"><br><br><br>
								<!-- <h4><span class="white"><?php echo app_setting()['data_setting'][0]->nama_aplikasi;?></span></h4>
								<h2><span class="white"><?php echo app_setting()['data_setting'][0]->nama_pendek;?></span></h2>
                                <h5><span class="white"><?php echo app_setting()['data_setting'][0]->nama_instansi;?></span></h5> -->
                                <h5 class="white"><b><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; 
								<?php echo app_setting()['data_setting'][0]->nama_instansi ?></b></h5><br>
							</div>
							<div class="col-sm-6">
								<form id="validation-form" method="post" action="<?php echo base_url();?>index.php/login" >
									<fieldset>
										<h4 class="white center"><b>LOG IN</b></h4>
										<br>
										<label class="w-100">
												<span class="block input-icon input-icon-left">
												<i class="icon-user color-purple"></i>
												<input type="text" class="w-100" id="username" name="username" placeholder="Username" />
												</span>
										</label>
										<label class="w-100">
											<span class="block input-icon input-icon-left bg-white padding-off coba">
												<i class="icon-lock color-purple"></i>
												<input type="password" id="password" name="password" class="w-85 margin-off" placeholder="Password" />
												<a id="eye" toggle="#password">&nbsp<img src="<?php echo base_url();?>assets/css/img/New/Ic-View-Password.png" class="w-10 eye-login"></a>
											</span>
										</label>
										<div class="space"></div>
										<div class="clearfix">
											<button type="submit" name="submit" class="w-100 btn btn-warna-login">
												<b>SIGN IN</b>
											</button>
										</div>
										<div class="space-4"></div>
									</fieldset>
	                                <?php
									$valid = validation_errors();
	                                if(!empty($valid)){
									?>
	                                <div class="alert alert-error">
	                                <strong>Warning ..!!! </strong>
	                               	<?php
										echo validation_errors();
									?>
	                                </div>
	                                <?php } ?>
	                                <?php
									$info = $this->session->flashdata('result_login');
									if(!empty($info)){
									?>
	                                <div class="alert alert-error">
	                                <strong>Warning ..!!! </strong>
	                               	<?php
										echo validation_errors();
										echo $this->session->flashdata('result_login');
									?>
	                                </div>
	                                <?php } ?>
								</form>
								</div>
							</div>
						</div>
									<!--/widget-main-->
											<!-- <div class="toolbar clearfix">
												<center>
													<a href="http://deddyrusdiansyah.blogspot.com" class="forgot-password-link">
														<p>Copyright &copy; <?php echo app_setting()['data_setting'][0]->nama_pendek;?> - 2016</p>
													</a>
                                                    </center>
											</div> -->
										<!--</div><--/widget-body-->
									<!--</div>/login-box-->
						</div><!-- Div row -->
						<br><br>
						<div class="row white justify-content-sm-center">
							<div class="col-sm-auto text-center">
								<h4><b>SIAKAD (Sistem Akademik)</b></h4>
								<p><small>
									- Jika lupa password dapat menghubungi petugas Akademik masing-masing fakultas<br>
									- Kalender Akademik TA. <?=$this->model_global->getThAkademikAktif()['th_akademik'];?> dapat di download <a href="" style="text-decoration: none;">di sini</a><br>
									<!-- - Jadwal Pengisian Rencana Studi (LIRS) dimulai dari tanggal 15 Januari 2018 - selesai -->
								</small></p>
							</div>
						</div>
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div>
		</div><!--/.main-container-->
		<!--basic scripts-->
		<!--[if !IE]>-->
		<script type="text/javascript">
			$("#eye").click(function() {

			  $(".coba").toggleClass("bg-white bg-klik");
			  var input = $($(this).attr("toggle"));
			  if (input.attr("type") == "password") {
			    input.attr("type", "text");
			  } else {
			    input.attr("type", "password");
			  }
			});
		</script>
		<!-- </body> </html> -->

