<script type="text/javascript">
$(document).ready(function(){
	$(".chzn-select").chosen();

	// $("#smt").change(function(){
	// 	$("#nim").val('');
	// 	$("#nim").focus;
	// });

	$("#th_ak").change(function(){
		var th_ak =$("#th_ak").val();
		var th = $("#th_ak").val().substr(4,1);

		if(th==1){
			$("#semester").val("Ganjil");
		}else{
			$("#semester").val("Genap");
		}

	});

	function cari_smt(){
		var th_ak = $("#th_ak").val();
		var kd_prodi = $("#kd_prodi").val();
		var semester = $("#semester").val();
		var smt = $("#smt").val();

		var string = {};
		string.th_ak = $("#th_ak").val();
		string.kd_prodi = $("#kd_prodi").val();
		string.semester = $("#semester").val();
		string.smt = $("#smt").val();

		if(!$("#th_ak").val()){
			//alert('Tahun Akademik tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}
		if(!$("#semester").val()){
			//alert('Semester tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#semester").focus();
			return false();
		}

		if(!$("#nim").val()){
			//alert('Semester tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'NIM tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#nim").focus();
			return false();
		}

		if(!$("#smt").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester Mahasiswa tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#smt").focus();
			return false();
		}


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cari_smt'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#pilih_smt").html(data);
				// $("#mk_paket").html(data);
			}
		});
	}

	$("#nim").change(function(){
		var string = {};
		string.th_ak =$("#th_ak").val();
		string.nim = $("#nim").val();

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cari_nim'); ?>",
			data	: string,
			cache	: false,
			dataType: "json",
			success	: function(data){
				console.log(data);
				$("#nama").val(data.nama);
				$("#th_angkatan").val(data.th_angkatan);
				$("#kd_prodi").val(data.kd_prodi);
				$("#nm_prodi").val(data.nm_prodi);
				$("#smt").val(data.smt);
				$("#max_sks").val(24);
				$("#kelas").val(data.kelas);
				$("#sex").val(data.sex);

				cari_bayar();

			}
		});
	});

	function cari_bayar()
	{
		var string = {};
		string.th_akademik = $("#th_ak").val();
		string.nim = $("#nim").val();
		string.smt = $("#smt").val();
		string.kd_prodi = $("#kd_prodi").val();
		string.th_angkatan = $("#th_angkatan").val();

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cari_bayar'); ?>",
			data	: string,
			cache	: false,
			dataType: "json",
			success	: function(data){
				console.log(data);
				// $("#nama").val(data.nama);
				if(data.info=='VALID'){
					$("#invalid").hide();
					$("#valid").show();
					$("#simpan").show();
					// $("#id_jadwal").show();
					cari_smt();
					cari_mata_kuliah();
				}else{
					$("#invalid").show();
					$("#valid").hide();
					$("#simpan").hide();
					// $("#id_jadwal").hide();
				}

				cari_krs();
			}
		});
	}

	$("#pilih_smt").change(function(){
			cari_mata_kuliah();
	});

	function cari_mata_kuliah(){
		var th_ak = $("#th_ak").val();
		var kd_prodi = $("#kd_prodi").val();
		var semester = $("#semester").val();
		var smt = $("#pilih_smt").val();

		var string = {};
		string.th_ak = $("#th_ak").val();
		string.kd_prodi = $("#kd_prodi").val();
		string.semester = $("#semester").val();
		string.smt = $("#pilih_smt").val();
		string.kelas = $("#kelas").val();

		if(!$("#th_ak").val()){
			//alert('Tahun Akademik tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#th_ak").focus();
			return false();
		}
		if(!$("#semester").val()){
			//alert('Semester tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#semester").focus();
			return false();
		}

		if(!$("#nim").val()){
			//alert('Semester tidak boleh kosong');
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'NIM tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#nim").focus();
			return false();
		}

		if(!$("#smt").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester Mahasiswa tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#smt").focus();
			return false();
		}


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cari_mata_kuliah'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#id_jadwal").html(data);
				// $("#mk_paket").html(data);
			}
		});
	}



	$("#simpan").click(function(){

		var string = $("#my-form").serialize();

		//alert(string);

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#th_ak").focus();
			return false();
		}

		if(!$("#nim").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'NIM tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#nim").focus();
			return false();
		}

		if(!$("#semester").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#semester").focus();
			return false();
		}

		if(!$("#id_jadwal").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Anda belum memilih mata kuliah',
				class_name: 'gritter-error'
			});

			$("#id_jadwal").focus();
			return false();
		}



		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/krs/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				$.gritter.add({
					title: 'Info..!!',
					text: data,
					class_name: 'gritter-info'
				});
				cari_krs();
			}
		});

	});

	function cari_krs(){
		var string = {};
		string.th_ak = $("#th_ak").val();
		string.nim = $("#nim").val();
		string.semester = $("#semester").val();
		string.smt = $("#smt").val();


		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cari_krs'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				$("#view_detail").html(data);
			}
		});
	}

	$("#cetak").click(function(){

		var string = $("#my-form").serialize();

		//alert(string);

		if(!$("#th_ak").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Tahun Akademik tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#th_ak").focus();
			return false();
		}

		if(!$("#nim").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'NIM tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#nim").focus();
			return false();
		}

		if(!$("#semester").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#semester").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('krs/cetak_krs'); ?>",
			data	: string,
			cache	: false,
			success	: function(data){
				// console.log(data);
				if(data=='Sukses'){
					window.location.assign("<?php echo site_url('krs/print_krs');?>");
					// window.open("<?php echo site_url('krs/print_krs');?>");
				}
			}
		});
	});
});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form class="form-horizontal" name="my-form" id="my-form">
							<fieldset>
<div class="span5">
<div class="control-group">
<label class="control-label" for="form-field-1">Tahun Akademik</label>
<div class="controls">
<select name="th_ak" id="th_ak" class="span2">
<option value="" selected="selected">-Pilih-</option>
<?php
$data = $this->model_data->th_akademik_jadwal();
foreach($data->result() as $dt){
?>
<option value="<?php echo $dt->th_akademik;?>"><?php echo $dt->th_akademik;?></option>
<?php } ?>
</select>
</div>
</div>
<div class="control-group">
<label class="control-label" for="form-field-1">Semester</label>
<div class="controls">
<input type="text" name="semester" id="semester" class="span1" readonly="readonly" />
</div>
</div>

<div class="control-group">
<label class="control-label" for="nim">NIM</label>
<div class="controls">
<select class="chzn-select span3" name="nim" id="nim" data-placeholder="pilih NIM...">
<option value="" selected="selected"> Pilih NIM ....</option>
<?php
$data = $this->model_data->data_all_mhs();
foreach($data->result() as $dt){
?>
<option value="<?php echo $dt->nim;?>"><?php echo $dt->nim;?></option>
<?php
}
?>
</select>
</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Nama</label>
	<div class="controls">
		<input type="text" name="nama" id="nama" class="span4" readonly="readonly" />
	</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Kelamin</label>
	<div class="controls">
		<input type="text" name="sex" id="sex" class="span2" readonly="readonly" />
	</div>
</div>

</div>

<div class="span7">

<div class="control-group">
<label class="control-label" for="form-field-1">Th.Angkatan</label>
<div class="controls">
<input type="text" name="th_angkatan" id="th_angkatan" class="span2" readonly="readonly" />
</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Nama Prodi</label>
<div class="controls">
	<input type="text" name="kd_prodi" id="kd_prodi" class="span1" readonly="readonly" />
<input type="text" name="nm_prodi" id="nm_prodi" readonly="readonly" />
</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Kelas</label>
<div class="controls">
<input type="text" name="kelas" id="kelas" class="span1" readonly="readonly" />
</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Semester</label>
<div class="controls">
	<input type="text" name="smt" id="smt" class="span1" readonly="readonly" />
</div>
</div>

<div class="control-group">
<label class="control-label" for="form-field-1">Max SKS</label>
<div class="controls">
<input type="text" name="max_sks" id="max_sks" class="span1" readonly="readonly" />
</div>
</div>

</div>
				<!-- <div id="mk_paket"></div> -->



				<div id="valid" class="span14">
					<hr/>
								<div class="control-group">
										<label class="control-label" for="form-field-1">Semester</label>

										<div class="controls">
												<select name="pilih_smt" id="pilih_smt" class="span1">
													<option value="" selected="selected">-Pilih-</option>
												 </select>
										</div>
								</div>

                	<div class="control-group">
                        <label class="control-label" for="form-field-1">Mata Kuliah</label>

                        <div class="controls">
                            <select name="id_jadwal" id="id_jadwal" class="span12">
                            	<option value="" selected="selected">-Pilih Mata Kuliah-</option>
                             </select>
                        </div>
                    </div>
				</div>
									</fieldset>

<div class="row-fluid">
<div class="span12 text-center" id="invalid" style="display:none;">
<div  class="alert alert-error" >
<strong>
<i class="icon-remove"></i>
Peringatan !
</strong>
<br/>
MAHASISWA BELUM MELAKSANAKAN PEMBAYARAN SPP / REGISTRASI
<br>
</div>
</div>
</div>

										<div class="form-actions center">
                     <button type="button" name="simpan" id="simpan" class="btn btn-small btn-primary">
                     <i class="icon-save"></i> Simpan
                     </button>

                     <button type="button" name="cetak" id="cetak" class="btn btn-small btn-info">
                     <i class="icon-print"></i> Cetak
                     </button>

                     <a href="<?php echo base_url();?>index.php/krs" class="btn btn-small btn-success">
                     <i class="icon-arrow-left"></i> Kembali
                     </a>
									 </div>
           </form>



        </div> <!-- wg body -->
    </div> <!--wg-main-->
</div>
Ket : KRS dicetak oleh mahasiswa
<div id="view_detail"></div>
