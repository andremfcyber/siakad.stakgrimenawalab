<script type="text/javascript">
  $(document).ready(function(){

    $("#simpan").click(function(){
        // console.log('testing');
        var sisa = $("#sisa_tagihan").val();
        var jumlah = $("#jumlah").val();

        var string = {};
        string.tagihan_mhs_id = $("#tagihan_mhs_id").val();
        string.nim = $("#nim").val();
        string.th_akademik_kode = $("#th_akademik").val();
        string.jumlah = $("#jumlah").val();

        // console.log(string);

        if(!$("#jumlah").val()){
    			$.gritter.add({
    				title: 'Peringatan..!!',
    				text: 'Jumlah tidak boleh kosong',
    				class_name: 'gritter-error'
    			});

    			$("#jumlah").focus();
    			return false();
    		}

        if(parseInt(jumlah) > parseInt(sisa)){
          $.gritter.add({
    				title: 'Peringatan..!!',
    				text: 'Jumlah tidak boleh lebih besar dari Tagihan',
    				class_name: 'gritter-error'
    			});

    			$("#jumlah").focus();
    			return false();
        }

        if(parseInt(jumlah)==0){
          $.gritter.add({
    				title: 'Peringatan..!!',
    				text: 'Jumlah tidak boleh Nol',
    				class_name: 'gritter-error'
    			});

    			$("#jumlah").focus();
    			return false();
        }

        $.ajax({
          type	: 'POST',
          url		: "<?php echo site_url('pembayaran/simpan'); ?>",
          data	: string,
          cache	: false,
          success	: function(data){
            // $("#info_mhs").html(data);
            // console.log(data);
            // $("#detail_bayar").html(data);
            // $("#list_mhs").hide();
            $('#modal-table').modal('toggle');
            getDataPembayaran();
            data_detail();
            $.gritter.add({
      				title: 'Info ..!!',
      				text: data,
      				class_name: 'gritter-success'
      			});
            $("#jumlah").val(0);
          }
        });


    });

    function data_detail()
    {
      var string = {};
      string.nim = $("#nim").val();
      // console.log(string);
      $.ajax({
        type	: 'POST',
        url		: "<?php echo site_url('pembayaran/data_detail'); ?>",
        data	: string,
        cache	: false,
        success	: function(data){
          // $("#info_mhs").html(data);
          // console.log(data);
          $("#data_detail").html(data);
          // $("#list_mhs").hide();
        }
      });
    }

    function getDataPembayaran()
    {
      var string = {};
      string.tagihan_mhs_id = $("#tagihan_mhs_id").val();
      $.ajax({
        type	: 'POST',
        url		: "<?php echo site_url('pembayaran/data_pembayaran'); ?>",
        data	: string,
        cache	: false,
        success	: function(data){
          // $("#info_mhs").html(data);
          // console.log(data);
          $("#detail_bayar").html(data);
          // $("#list_mhs").hide();
        }
      });
    }
  });
  function getTagihanMhsId(id,sisa)
  {
    console.log(id);
    $("#tagihan_mhs_id").val(id);
    $("#sisa_tagihan").val(sisa);

    getDataPembayaran();
  }

  function getDataPembayaran()
  {
    var string = {};
    string.tagihan_mhs_id = $("#tagihan_mhs_id").val();
    $.ajax({
      type	: 'POST',
      url		: "<?php echo site_url('pembayaran/data_pembayaran'); ?>",
      data	: string,
      cache	: false,
      success	: function(data){
        // $("#info_mhs").html(data);
        // console.log(data);
        $("#detail_bayar").html(data);
        // $("#list_mhs").hide();
      }
    });
  }
</script>
<style media="screen">
.modal {
  margin-left:-400px;
  width:900px;
}
</style>
<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" rowspan="2">No</th>
            <th class="center" rowspan="2">Th Akademik</th>
            <th class="center" colspan="3">Tagihan</th>
            <th class="center" rowspan="2">Pembayaran</th>
            <th class="center" rowspan="2">Sisa</th>
            <th class="center" rowspan="2">Bayar</th>
        </tr>
        <tr>
            <th class="center">Kode</th>
            <th class="center">Nama</th>
            <th class="center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
      <?php
		$i=1;
    $tota_sisa = 0;
		foreach($data as $dt){
      $infotagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);
      $kodetagihan = $infotagihan['kode'];
      $namatagihan = $infotagihan['nama'];
      $pembayaran = $this->model_data->getJumlahPembayaran($dt->id);
      $sisa = $dt->jumlah - $pembayaran;
      // $tagihan = $this->model_data->tagihan($th_akademik_kode,$jenis_tagihan_id);
      // $smt =  $this->model_global->semester($dt->nim,$th_akademik_kode);
      // $status = $this->model_data->cekStatusTagihan($th_akademik_kode,$jenis_tagihan_id,$dt->nim);
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik_kode;?></td>
            <td class="center"><?php echo $kodetagihan;?></td>
            <td><?php echo $namatagihan;?></td>
            <td style="text-align:right"><?php echo number_format($dt->jumlah);?></td>
            <td style="text-align:right"><?php echo number_format($pembayaran);?></td>
            <td style="text-align:right"><?php echo number_format($sisa);?></td>
            <td class="center span2">
              <a href="#modal-table" class="btn btn-small btn-info"  role="button" data-toggle="modal" name="bayar" id="bayar" onclick="getTagihanMhsId('<?php echo $dt->id;?>','<?php echo $sisa;?>')">
                <i class="icon-check"></i>Bayar
              </a>
            </td>
        </tr>
		<?php
      $tota_sisa +=$sisa;
      } ?>
      <tr>
        <td colspan="6" class="center">
          <strong>Total Tagihan</strong>
        </td>
        <td style="text-align:right" class="red">
          <strong><?php echo number_format($tota_sisa);?></strong>
        </td>
      </tr>
    </tbody>
</table>

<!-- Modal -->

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Data Pembayaran
        </div>
    </div>

    <div class="modal-body ">
        <div class="row-fluid no-padding">
            <form class="form-horizontal" name="my-form" id="my-form">
                <input type="hidden" name="tagihan_mhs_id" id="tagihan_mhs_id">
                <fieldset>
                  <!-- <div class="control-group">
                      <label class="control-label" for="form-field-1">Tagihan</label>

                      <div class="controls">
                          <input type="text" name="tagihan" id="tagihan"  class="span4" readonly />
                      </div>
                  </div> -->
                <div class="control-group">
                    <label class="control-label" for="form-field-1">Tagihan</label>

                    <div class="controls">
                        <input type="text" name="sisa_tagihan" id="sisa_tagihan"  class="span4" readonly />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="form-field-1">Bayar</label>

                    <div class="controls">
                        <input type="number" name="jumlah" id="jumlah"  class="span4" />
                    </div>
                </div>
              </fieldset>
			      </form>
        </div>
    </div>

    <div id="detail_bayar"></div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal" name="close" id="close">
            <i class="icon-remove"></i>
            Close
        </button>

        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>
