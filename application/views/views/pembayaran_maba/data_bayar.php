<!-- <pre>
  <?php print_r($data);?>
</pre> -->
<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center" >No</th>
            <th class="center" >Th Akademik</th>
            <th class="center" >Nomor</th>
            <th class="center" >Tanggal</th>
            <th class="center" >No. Pendaftaran</th>
            <th class="center" >Nama</th>
            <th class="center" >PRODI</th>
            <!-- <th class="center" >Kelas</th> -->
            <th class="center" >Jenis Tagihan</th>
            <th class="center" >Jumlah</th>
            <th class="center" >Keterangan</th>
            <th class="center span2" >Aksi</th>
        </tr>
    </thead>
    <tbody>
      <?php
		$i=1;
    $tota_sisa = 0;
		foreach($data as $dt){
      $infoMaba = $this->model_data->getInfoMaba($dt->no_pendaftaran);
      $nama_mhs = $infoMaba['nama_mhs'];
      $prodi_mhs = $infoMaba['nama_prodi'];
      // $kelas_mhs = $infoMaba['kelas'];
      $infotagihan = $this->db->get_where('master_tagihan_maba',array('id' => $dt->jenis_tagihan_maba_id))->row_array();

      $namatagihan =  $infotagihan['nama_tagihan'];
      ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->th_akademik;?></td>
            <td class="center"><?php echo $dt->nomor;?></td>
            <td class="center"><?php echo $this->model_global->tgl_str($dt->tanggal);?></td>
            <td class="center"><?php echo $dt->no_pendaftaran;?></td>
            <td><?php echo $nama_mhs;?></td>
            <td><?php echo $prodi_mhs;?></td>
            <!-- <td class="center"><?php echo $kelas_mhs;?></td> -->
            <td><?php echo $namatagihan;?></td>
            <td style="text-align:right"><?php echo number_format($dt->jumlah);?></td>
            <td><?php echo $dt->keterangan;?></td>

            <td class="center">
              <a href="" onclick="editData('<?php echo $dt->id;?>')" class="btn btn-mini btn-success"  role="button" data-toggle="modal">
                <i class="icon-edit"></i>
              </a>
              <a href="" onclick="deleteData('<?php echo $dt->id;?>')"  class="btn btn-mini btn-danger"  role="button" data-toggle="modal">
                <i class="icon-trash"></i>
              </a>
              <a href="<?php echo site_url('pembayaran_maba/cetak/'.$dt->nomor);?>" target="_blank" class="btn btn-mini btn-primary"  role="button" data-toggle="modal">
                <i class="icon-print"></i>
              </a>
            </td>
        </tr>
		<?php
      $tota_sisa +=$dt->jumlah;
      } ?>
      <tr>
        <td colspan="10" class="center">
          <strong>Total Bayar</strong>
        </td>
        <td colspan="3" style="text-align:left" class="red">
          <strong><?php echo number_format($tota_sisa);?></strong>
        </td>
      </tr>
    </tbody>
</table>
