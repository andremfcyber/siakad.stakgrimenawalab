<script type="text/javascript">
	function cari_jadwal(){
		var th_ak = $("#th_akademik").val();
		var kd_prodi = $("#kd_prodi").val();
		var smt = $("#smt").val();

		if(smt.length==0){
			alert('Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/cari_jadwal",
			data	: "kd_prodi="+kd_prodi+"&smt="+smt+"&th_ak="+th_ak,
			cache	: false,
			success	: function(data){
				$("#view_data").html(data);
			}
		});
	}

	function edit(id){
		list_mk();
		// break;
		$("#id").val(id);
		var string = {};
		string.id = id;

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url('jadwal/cari_jadwal_id'); ?>",
			data	: string,
			cache	: false,
			dataType : 'json',
			success	: function(data){
				$("#view_data").html(data);
				// console.log(data);
				// list_mk();
				$("#hari").val(data.hari);
				// $("#jam_mulai").val(data.jam_mulai);
				// $("#jam_selesai").val(data.jam_selesai);
				$("#ruang").val(data.ruang);
				$("#mk").val(data.kd_mk);
				$("#kd_dosen").val(data.kd_dosen);
				$("#kelas").val(data.kelas);
			}
		});

	}

	function list_mk()
	{
		var kd_prodi = $("#kd_prodi").val();
		var smt = $("#smt").val();

		console.log(kd_prodi);

		if(smt.length==0){
			alert('Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/mata_kuliah",
			data	: "kd_prodi="+kd_prodi+"&smt="+smt,
			cache	: false,
			success	: function(data){
				$("#mk").html(data);
				// cari_jadwal();
			}
		});
	}
	function hapus(id){
		var r = confirm("Yakin akan menghapus data ini!");
		if (r == true) {
			$.ajax({
				type	: 'POST',
				url		: "<?php echo site_url(); ?>/jadwal/hapus",
				data	: "id="+id,
				cache	: false,
				success	: function(data){
					$.gritter.add({
						title: 'Info..!!',
						text: data,
						class_name: 'gritter-info'
					});
					cari_jadwal();
				}
			});
		}

	}
</script>
<div class="alert alert-error">
	<button type="button" class="close" data-dismiss="alert">
		<i class="icon-remove"></i>
	</button>
	<strong>
		<i class="icon-remove"></i>
		Perhatian ..!!
	</strong>
  <br/>
  Hati-hati terhadap edit Jadwal, akan mempengaruhi Validasi.
</div>
<div class="row-fluid">
<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center span2">Hari</th>
            <!-- <th class="center">Pukul</th> -->
			<th class="center">Kelas</th>
			<th class="center">Ruang</th>
            <th class="center">Kode - Mata Kuliah</th>
			<th class="center">SKS</th>
			<th class="center">SMT</th>
            <th class="center">Dosen</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){
			$infoMK = $this->model_data->getInfoMK($dt->kd_mk);
			$nama_mk = $infoMK['nama_mk']; //$this->model_data->cari_nama_mk($dt->kd_mk);
			$sks_mk = $infoMK['sks'];
			$smt_mk = $infoMK['smt'];
			$nama_dosen = $this->model_data->cari_nama_dosen($dt->kd_dosen);
		?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center span2"><?php echo $dt->hari;?></td>
            <!-- <td class="center"><?php echo $dt->pukul;?></td> -->
			<td class="center"><?php echo $dt->kelas;?></td>
			<td class="center"><?php echo $dt->ruang;?></td>
            <td ><?php echo $dt->kd_mk;?> - <?php echo $nama_mk;?></td>
			<td class="center"><?php echo $sks_mk;?></td>
			<td class="center"><?php echo $smt_mk;?></td>
            <td ><?php echo $dt->kd_dosen;?> - <?php echo $nama_dosen;?></td>
            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
								<a class="red" href="javascript:" onClick="edit('<?php echo $dt->id_jadwal;?>')">
										<i class="icon-edit bigger-130"></i>
								</a>
                    <a class="red" href="javascript:" onClick="hapus('<?php echo $dt->id_jadwal;?>')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>
<div id="view_data"></div>
