<script type="text/javascript">
$(document).ready(function(){
  $("#simpan").click(function(){
		var th_akademik	= $("#th_akademik").val();
		var aktif			= $("#aktif").val();
		var aktif_hidden	= $("#aktif_hidden").val();

		var string = $("#my-form").serialize();


    if(!$("#kode").val()){
      $.gritter.add({
        title: 'Peringatan..!!',
        text: 'Kode tidak boleh kosong',
        class_name: 'gritter-error'
      });

      $("#kode").focus();
      return false();
    }

		if(th_akademik.length==0){
			alert('Maaf, Th Akademik Tidak boleh kosong');
			$("#th_akademik").focus();
			return false();
		}

    if(!$("#semester").val()){
      $.gritter.add({
        title: 'Peringatan..!!',
        text: 'Semester tidak boleh kosong',
        class_name: 'gritter-error'
      });

      $("#semester").focus();
      return false();
    }

    if(!$("#keterangan").val()){
      $.gritter.add({
        title: 'Peringatan..!!',
        text: 'Keterangan tidak boleh kosong',
        class_name: 'gritter-error'
      });

      $("#keterangan").focus();
      return false();
    }

		if(aktif.length==0){
			alert('Maaf, Status Aktif Tidak boleh kosong');
			$("#aktif").focus();
			return false();
		}

		if (aktif_hidden == 'Tidak' && aktif == 'Ya'){
			if(!confirm('Peringatan!\nPerubahan Aktif akan merubah mahasiswa Aktif menjadi Cuti. Setuju?')){
				alert('not confirmed');
				exit();
			}
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/thakademik/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				alert(data);
				location.reload();
			}
		});

	});

  $("#tambah").click(function(){
    $('#id').val('0');
    $('#kode').val('');
    $('#kode').attr("readonly",false);
    $('#th_akademik').val('');
	 $('#th_akademik').attr("readonly",false);
    $('#keterangan').val('');
    $("#aktif").val('');
    $("#aktif_hidden").val('Tidak');
    $("#semester").val('');
	});

});

function editData(ID){
	var cari	= ID;
	$.ajax({
		type	: "POST",
		url		: "<?php echo site_url(); ?>/thakademik/cari",
		data	: "cari="+cari,
		dataType: "json",
		success	: function(data){
			//alert(data.ref);
      $('#id').val(ID);
		$('#kode').val(data.kode);
      $('#kode').attr("readonly","true");
      $('#th_akademik').val(data.th_akademik);
      $('#th_akademik').attr("readonly","true");
      $('#keterangan').val(data.keterangan);
		$('#aktif').val(data.aktif);
      $('#aktif_hidden').val(data.aktif);
      $('#semester').val(data.semester);
		}
	});

}
</script>
<div class="row-fluid">
<div class="table-header">
    <?php echo $judul;?>
    <div class="widget-toolbar no-border pull-right">
    <a href="#modal-table" class="btn btn-small btn-success"  role="button" data-toggle="modal" name="tambah" id="tambah" >
        <i class="icon-check"></i>
        Tambah Data
    </a>
    </div>
</div>

<table  class="table fpTable lcnp table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Kode</th>
            <th class="center">Tahun Akademik</th>
            <th class="center">Semester</th>
            <th class="center">Aktif</th>
            <th class="center">Keterangan</th>
            <th class="center">Aksi</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
		foreach($data->result() as $dt){ ?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->kode;?></td>
            <td class="center"><?php echo $dt->th_akademik;?></td>
            <td class="center"><?php echo $dt->semester;?></td>
            <td class="center" >
              <?php
              if($dt->aktif=='Ya'){
                echo "<span class='badge badge-success'>$dt->aktif</span>";
              }else{
                echo $dt->aktif;
              }
            ?></td>
            <td>
              <?php echo $dt->keterangan;?>
            </td>
            <td class="td-actions"><center>
            	<div class="hidden-phone visible-desktop action-buttons">
                    <a class="green" href="#modal-table" onclick="javascript:editData('<?php echo $dt->id;?>')" data-toggle="modal">
                        <i class="icon-pencil bigger-130"></i>
                    </a>

                    <a class="red" href="<?php echo site_url();?>/thakademik/hapus/<?php echo $dt->id;?>" onClick="return confirm('Anda yakin ingin menghapus data ini?')">
                        <i class="icon-trash bigger-130"></i>
                    </a>
                </div>

                <div class="hidden-desktop visible-phone">
                    <div class="inline position-relative">
                        <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-caret-down icon-only bigger-120"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">
                            <li>
                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                    <span class="green">
                                        <i class="icon-edit bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                    <span class="red">
                                        <i class="icon-trash bigger-120"></i>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                </center>
            </td>
        </tr>
		<?php } ?>
    </tbody>
</table>
</div>

<div id="modal-table" class="modal hide fade" tabindex="-1">
    <div class="modal-header no-padding">
        <div class="table-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            Data Tahun Akademik
        </div>
    </div>

    <div class="modal-body no-padding">
        <div class="row-fluid">
            <form class="form-horizontal" name="my-form" id="my-form">
              <input type="hidden" name="id" id="id">
              <br/>
              <div class="control-group">
                  <label class="control-label" for="form-field-1">Kode</label>
                  <div class="controls">
                      <input type="text" name="kode" id="kode"  class="span2" maxlength="5" />
                  </div>
              </div>
                <div class="control-group">
                    <label class="control-label" for="form-field-1">Th Akademik</label>
                    <div class="controls">
                        <input type="text" name="th_akademik" id="th_akademik"  class="span4" maxlength="10" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Semester</label>
                    <div class="controls">
                        <select name="semester" id="semester" class="span3">
                          <option value="">-Pilih-</option>
                          <option value="Ganjil">Ganjil</option>
                          <option value="Genap">Genap</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Keterangan</label>
                    <div class="controls">
                        <input type="text" name="keterangan" id="keterangan"  class="span6"/>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="form-field-1">Aktif</label>
                    <div class="controls">
                    		<input type="hidden" name="aktif_hidden" id="aktif_hidden">
                        <select name="aktif" id="aktif" class="span3">
                          <option value="">-Pilih-</option>
                          <option value="Ya">Ya</option>
                          <option value="Tidak">Tidak</option>
                        </select>
                    </div>
                </div>

			      </form>
        </div>
    </div>

    <div class="modal-footer">
        <div class="pagination pull-right no-margin">
        <button type="button" class="btn btn-small btn-danger pull-left" data-dismiss="modal">
            <i class="icon-remove"></i>
            Close
        </button>
        <button type="button" name="simpan" id="simpan" class="btn btn-small btn-success pull-left">
            <i class="icon-save"></i>
            Simpan
        </button>
		</div>
    </div>
</div>
