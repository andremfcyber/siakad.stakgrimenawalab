<script type="text/javascript">
$(document).ready(function(){
	// $('#jam_mulai').timepicker({
	// 	minuteStep: 1,
	// 	showSeconds: false,
	// 	showMeridian: false
	// });
	// $('#jam_selesai').timepicker({
	// 	minuteStep: 1,
	// 	showSeconds: false,
	// 	showMeridian: false
	// });

	cari_jadwal();

	function cari_jadwal(){
		var th_ak = $("#th_akademik").val();
		var kd_prodi = $("#kd_prodi").val();
		var smt = $("#smt").val();

		if(smt.length==0){
			alert('Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/cari_jadwal",
			data	: "kd_prodi="+kd_prodi+"&smt="+smt+"&th_ak="+th_ak,
			cache	: false,
			success	: function(data){
				$("#view_data").html(data);
			}
		});
	}

	list_mk();

	$("#kd_prodi").change(function(){
		list_mk();
		cari_jadwal();
		list_dosen();
	});

	function list_mk()
	{
		var kd_prodi = $("#kd_prodi").val();
		var smt = $("#smt").val();

		if(smt.length==0){
			alert('Semester tidak boleh kosong');
			$("#smt").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/mata_kuliah",
			data	: "kd_prodi="+kd_prodi+"&smt="+smt,
			cache	: false,
			success	: function(data){
				$("#mk").html(data);

			}
		});
	}

	function list_dosen()
	{
	
		var kd_prodi = $("#kd_prodi").val();
		// var smt = $("#smt").val();

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/list_dosen",
			data	: "kd_prodi="+kd_prodi,
			cache	: false,
			success	: function(data){
				$("#kd_dosen").html(data);

			}
		});
	}

	$("#simpan").click(function(){

		var string = $("#my-form").serialize();

		//alert(string);

		if(!$("#smt").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Semester tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#smt").focus();
			return false();
		}

		if(!$("#kd_prodi").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Program Studi tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#kd_prodi").focus();
			return false();
		}

		if(!$("#hari").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Hari tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#hari").focus();
			return false();
		}

		// if(!$("#jam_mulai").val()){
		// 	$.gritter.add({
		// 		title: 'Peringatan..!!',
		// 		text: 'Jam Mulai tidak boleh kosong',
		// 		class_name: 'gritter-error'
		// 	});

		// 	$("#jam_mulai").focus();
		// 	return false();
		// }

		// if(!$("#jam_selesai").val()){
		// 	$.gritter.add({
		// 		title: 'Peringatan..!!',
		// 		text: 'Jam Selesai tidak boleh kosong',
		// 		class_name: 'gritter-error'
		// 	});

		// 	$("#jam_selesai").focus();
		// 	return false();
		// }

		if(!$("#ruang").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Ruang tidak boleh kosong',
				class_name: 'gritter-error'
			});

			$("#ruang").focus();
			return false();
		}

		if(!$("#mk").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Mata Kuliah tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#mk").focus();
			return false();
		}

		if(!$("#kd_dosen").val()){
			$.gritter.add({
				title: 'Peringatan..!!',
				text: 'Dosen tidak boleh kosong',
				class_name: 'gritter-error'
			});
			$("#kd_dosen").focus();
			return false();
		}

		$.ajax({
			type	: 'POST',
			url		: "<?php echo site_url(); ?>/jadwal/simpan",
			data	: string,
			cache	: false,
			success	: function(data){
				$.gritter.add({
					title: 'Info..!!',
					text: data,
					class_name: 'gritter-info'
				});
				cari_jadwal();
			}
		});

	});

	$("#tambah").click(function(){
		$("#id").val('');
		$("#hari").val('');
		$("#pukul").val('');
		$("#ruang").val('');
		$("#mk").val('');
		$("#kd_dosen").val('');
		$("#kelas").val('');
		$("#kat_dosen").val('');
		// $("#jam_selesai").val('');

		$("#hari").focus();

		list_mk();
	});
});
</script>

<div class="widget-box ">
    <div class="widget-header">
        <h4 class="lighter smaller">
            <i class="icon-book blue"></i>
            <?php echo $judul;?>
        </h4>
    </div>

    <div class="widget-body">
    	<div class="widget-main no-padding">

            <form name="my-form" id="my-form">
			<input type="hidden" name="id" id="id">
			<fieldset>
            <table width="100%" style="margin:10px;">
            <tr>
            	<td width="50%" valign="top">
                    <label class="control-label" for="form-field-1">Tahun Akademik Aktif</label>
                    <div class="controls">
                    	<input type="text" name="th_akademik" id="th_akademik" value="<?php echo $th_akademik;?>" class="span1" readonly="readonly" />
                    </div>
                    <label class="control-label" for="form-field-1">Semester</label>
                    <div class="controls">
                    <input type="text" name="smt" id="smt" value="<?php echo $semester;?>" class="span1" readonly="readonly" />
                    </div>

                    <label class="control-label" for="form-field-1">Jurusan</label>
                    <div class="controls">
                    <?php if($_SESSION['adm_kd_prodi']==null){?>
	                    <select name="kd_prodi" id="kd_prodi">
	                    	<option value="">-Pilih Prodi-</option>
	                        <?php
	                        $data = $this->model_data->data_jurusan();
	                        foreach($data->result() as $dt){
														if($dt->kd_prodi==$kd_prodi){
															$select = "selected=true";
														}else{
															$select = '';
														}
	                        ?>
	                        <option value="<?php echo $dt->kd_prodi;?>" <?php echo $select;?>><?php echo $dt->prodi;?></option>
	                        <?php
	                        }
	                        ?>
	                    </select>
	                <?php 
                    }else{
                        $data = $this->model_data->data_jurusan($_SESSION['adm_kd_prodi']);
                    ?>
                        <input type="text" value="<?php echo $data->row()->prodi;?>" readonly="readonly">
                        <input type="hidden" name="kd_prodi" id="kd_prodi" value="<?php echo $data->row()->kd_prodi;?>">
                    <?php }?>
                    </div>

										<label class="control-label" for="form-field-1">Hari</label>
                    <div class="controls">
                    <select name="hari" id="hari" class="span2">
                    	<option value="">-Pilih Hari-</option>
                         <?php
						$data = $this->model_data->hari_kuliah();
						foreach($data as $dt){
						?>
                        <option value="<?php echo $dt;?>"><?php echo $dt;?></option>
                        <?php } ?>
                    </select>
                    </div>
                   <!--  <label class="control-label" for="form-field-1">Jam Mulai</label>
                    <div class="controls-group">
						<div class="input-append bootstrap-timepicker">
							<input name="jam_mulai" id="jam_mulai" type="text" class="input-small" />
							<span class="add-on">
								<i class="icon-time"></i>
							</span>
						</div>

                    </div>
					<label class="control-label" for="form-field-1">Jam Selesai</label>
                    <div class="controls-group">
						<div class="input-append bootstrap-timepicker">
							<input name="jam_selesai" id="jam_selesai" type="text" class="input-small" />
							<span class="add-on">
								<i class="icon-time"></i>
							</span>
						</div>
                    </div> -->
               </td>
							 <!-- Pemisah  -->
               <td width="50%" valign="top">
					<label class="control-label" for="form-field-1">Kelas</label>
					<div class="controls">
					<select name="kelas" id="kelas" class="span2">
						<option value="">-Pilih Kelas-</option>
							<?php
								$data = $this->model_data->get_kelas();
								foreach($data as $dt){
								?>
								<option value="<?php echo $dt;?>"><?php echo $dt;?></option>
							<?php } ?>
					</select>
					</div>
                    <label class="control-label" for="form-field-1">Ruang</label>
                    <div class="controls">
                    <select name="ruang" id="ruang" class="span2">
                    	<option value="">-Pilih Ruang-</option>
                         <?php
						$data = $this->db->get('ruang_kuliah')->result();
						foreach($data as $dt){
						?>
                        <option value="<?php echo $dt->nama;?>"><?php echo $dt->nama;?></option>
                        <?php } ?>
                    </select>
                    </div>
                    <label class="control-label" for="form-field-1">Mata Kuliah - Semester</label>
                    <div class="controls">
                    <select name="mk" id="mk" class="span5">
                    	<option value="">-Pilih Mata Kuliah-</option>

                    </select>
                    </div>
					<label class="control-label" for="form-field-1">Dosen Perprodi</label>
                    <div class="controls">
						<select name="ka_dosen" id="ka_dosen" class="span4">
							<option value="1">Ya</option>
							<option value="2">Tidak</option>
						</select>
                    </div>
                    <label class="control-label" for="form-field-1">Kode Dosen</label>
                    <div class="controls">
                    <select name="kd_dosen" id="kd_dosen" class="span5 js-select-2 form-control">
                    	<option value="">-Pilih Dosen-</option>
                       	<!-- // $data = $this->model_data->data('dosen'); -->
						<!-- // foreach($data as $dt){ -->
						<!-- <option value="<?php echo $dt->kd_dosen;?>"><?php echo $dt->kd_dosen;?> - <?php echo $dt->nama_dosen;?></option> -->
                     
                    </select>
                    </div>
               </td>
			</tr>
            </table>
			</fieldset>
           	<div class="form-actions center">
	            <button type="button" name="simpan" id="simpan" class="btn btn-mini btn-primary">
	            	<i class="icon-save"></i> Simpan
	            </button>
	            <button type="button" name="tambah" id="tambah" class="btn btn-mini btn-info">
	            	<i class="icon-check"></i> Tambah
	            </button>
	            <a href="<?php echo base_url();?>index.php/jadwal" class="btn btn-mini btn-success">
	            <i class="icon-double-angle-right"></i> Kembali
	            </a>
        	</div>
            </form>

        </div>

    </div>
</div>

<div id="view_data"></div>
<script>
	$(document).ready(function() {
		$('.js-select-2').select2();
		$("#ka_dosen").change(function(){
			var st_dosen = $('#ka_dosen').val();
			if(st_dosen == 1){
				var kd_prodi = $("#kd_prodi").val();
			}else{
				kd_prodi = 'all';
			}
			// var smt = $("#smt").val();

			$.ajax({
				type	: 'POST',
				url		: "<?php echo site_url(); ?>/jadwal/list_dosen",
				data	: "kd_prodi="+kd_prodi,
				cache	: false,
				success	: function(data){
					$("#kd_dosen").html(data);

				}
			});
		});
	});
	

</script>