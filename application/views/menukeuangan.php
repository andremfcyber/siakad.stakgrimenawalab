<?php
if($class=='home'){
	$home = 'class="active"';
	$master ='';
	$transaksi = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
	$lapkeuangan = '';
}elseif($class=='master'){
	$home = '';
	$master ='class="active"';
	$transaksi = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
	$lapkeuangan = '';
}elseif($class=='keuangan'){
	$home = '';
	$master ='';
	$transaksi = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = 'class="active"';
	$lapkeuangan = '';
}elseif($class=='lapkeuangan'){
	$home = '';
	$master ='';
	$transaksi = '';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
	$lapkeuangan = 'class="active"';
}elseif($class=='transaksi'){
	$home = '';
	$master ='';
	$transaksi = 'class="active"';
	$laporan = '';
	$grafik = '';
	$import = '';
	$keuangan = '';
	$lapkeuangan = '';
}elseif($class=='laporan'){
	$home = '';
	$master ='';
	$transaksi = '';
	$laporan = 'class="active"';
	$grafik = '';
	$import = '';
	$keuangan = '';
	$lapkeuangan = '';
}elseif($class=='import'){
	$home = '';
	$master ='';
	$transaksi = '';
	$laporan = '';
	$grafik = '';
	$import = 'class="active"';
	$keuangan = '';
	$lapkeuangan = '';
}else{
	$home = '';
	$master ='';
	$transaksi = '';
	$laporan = '';
	$grafik = 'class="active"';
	$import = '';
	$keuangan = '';
	$lapkeuangan = '';
}
?>
<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar keuangan-overlay" id="sidebar">
    <div class="sidebar-shortcuts keuangan-dark" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <i class="icon-calendar"></i>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			echo $this->model_global->hari_ini(date('w')).", ".$this->model_global->tgl_indo(date('Y-m-d'));
			?>
        </div>
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div><!--#sidebar-shortcuts-->

    <div align="center" class="padding-t-20">
    <img src="<?php echo base_url();?>assets/images/logo.png" width="80">
    <h6><?php echo app_setting()['data_setting'][0]->nama_pendek.'<br/>'. app_setting()['data_setting'][0]->nama_instansi;?></h6>
    </div>

    <ul class="nav nav-list keuangan">
        <li <?php echo $home;?> >
            <a href="<?php echo base_url();?>index.php/home">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Home </span>
            </a>
        </li>

				<li <?php echo $keuangan;?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-bullhorn"></i>
                <span class="menu-text">
                    Keuangan
                </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
							<li>
									<a href="<?php echo site_url('jenis_tagihan');?>">
											<i class="icon-double-angle-right"></i>
											Jenis Tagihan
									</a>
							</li>
							<li>
									<a href="<?php echo site_url('pembayaran');?>">
											<i class="icon-double-angle-right"></i>
											Pembayaran
									</a>
							</li>

							<li>
									<a href="<?php echo site_url('pembayaran_maba');?>">
											<i class="icon-double-angle-right"></i>
											Pemb. Mahasiswa Baru
									</a>
							</li>

							<li>
									<a href="<?php echo site_url('validasiwisuda');?>">
											<i class="icon-double-angle-right"></i>
											Validasi Wisuda
									</a>
							</li>
								 <!-- <li>
									 <a href="<?php echo site_url('pembayaran');?>">
                        <i class="icon-double-angle-right"></i>
                        Pembayaran
                    </a>
                </li> -->
								<li>
									 <a href="<?php echo site_url('dispensasi');?>">
											 <i class="icon-double-angle-right"></i>
											 Dispensasi
									 </a>
							 </li>
            </ul>
        </li>

				<li <?php echo $lapkeuangan;?>>
            <a href="#" class="dropdown-toggle">
                <i class="icon-print"></i>
                <span class="menu-text">
                    Laporan Keuangan
                </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">

							<li>
									<a href="<?php echo site_url('lappembayaran');?>">
											<i class="icon-double-angle-right"></i>
											Pembayaran
									</a>
							</li>
							<li>
									<a href="<?php echo site_url('lappiutang');?>">
											<i class="icon-double-angle-right"></i>
											Piutang
									</a>
							</li>

            </ul>
        </li>


         <li>
            <a href="<?php echo site_url('login/logout');?>">
                <i class="icon-off"></i>
                <span class="menu-text"> Keluar </span>
            </a>
        </li>
    </ul><!--/.nav-list-->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left"></i>
    </div>
</div>
