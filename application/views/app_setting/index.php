
<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo $judul;?></h4>

		<span class="widget-toolbar">
			<a href="#" data-action="collapse">
				<i class="icon-chevron-up"></i>
			</a>

			<a href="#" data-action="close">
				<i class="icon-remove"></i>
			</a>
		</span>
	</div>

	<div class="widget-body">
		<div class="widget-main no-padding">
		<?=$this->session->flashdata('pesan');?>

       <form method="POST" action="#" enctype="multipart/form-data" name="form-entry" id="insert_data" class="form-horizontal">
         <fieldset>
            
            <div class="control-group">
				<label class="control-label" for="form-field-2">Nama Aplikasi</label>

				<div class="controls">
                    <input type="text" required name="nama_aplikasi" id="nama_aplikasi" class="span6" value="<?php echo $data_setting[0]->nama_aplikasi;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Nama Pendek</label>

				<div class="controls">
                    <input type="text" required name="nama_pendek" id="nama_pendek" class="span6" value="<?php echo $data_setting[0]->nama_pendek;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Nama Instansi</label>

				<div class="controls">
                    <input type="text" required name="nama_instansi" id="nama_instansi" class="span6" value="<?php echo $data_setting[0]->nama_instansi;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Alamat Instansi</label>

				<div class="controls">
                <textarea required name="alamat_instansi" id="alamat_instansi" class="span6">
                    <?php echo $data_setting[0]->alamat_instansi;?>
                </textarea>
                    
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Alamat 1</label>

				<div class="controls">
                    <input type="text" name="alamat1" id="alamat1" class="span6" value="<?php echo $data_setting[0]->alamat1;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Alamat 2</label>

				<div class="controls">
                    <input type="text" required name="alamat2" id="alamat2" class="span6" value="<?php echo $data_setting[0]->alamat2;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Website</label>

				<div class="controls">
                    <input type="text" required name="website" id="website" class="span6" value="<?php echo $data_setting[0]->website;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Email</label>

				<div class="controls">
                    <input type="email" required name="email" id="email" class="span6" value="<?php echo $data_setting[0]->email;?>">
				</div>
			</div>

            <div class="control-group">
				<label class="control-label" for="form-field-2">Kota</label>

				<div class="controls">
                    <input type="text" required name="kota" id="kota" class="span6" value="<?php echo $data_setting[0]->kota;?>">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="form-field-2">Warna Header 1</label>

				<div class="controls">
                    <input type="color" required name="warna_header_1" id="warna_header_1" class="span6" value="<?php echo $data_setting[0]->warna_header_1;?>">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" required for="form-field-2">Warna Header 2</label>

				<div class="controls">
                    <input type="color" required name="warna_header_2" id="warna_header_2" class="span6" value="<?php echo $data_setting[0]->warna_header_2;?>">
				</div>
			</div>

            <div class="control-group">
				<div class="dropifyCustom">
					<label class="control-label" for="form-field-2">Logo</label>

					<div class="controls">
						<input type="file" data-allowed-file-extensions="png jpg jpeg" data-show-remove="false" name="logo" id="logo" class="dropify span6" data-default-file="<?php echo ($data_setting[0]->logo != null) ? "/assets/app_setting_upload/img/".$data_setting[0]->logo : "" ;?>" />
					</div>
				</div>
			</div>

			<div class="control-group">
				<div class="dropifyCustom">
					<label class="control-label" for="form-field-2">Background Login</label>

					<div class="controls">
						<input type="file" data-allowed-file-extensions="png jpg jpeg" data-show-remove="false" name="bg_login" id="bg_login" class="dropify span6" data-default-file="<?php echo ($data_setting[0]->bg_login != null) ? "/assets/app_setting_upload/img/".$data_setting[0]->bg_login : "" ;?>" />
					</div>
				</div>
			</div>

			<div class="control-group">
				<div class="dropifyCustom">
					<label class="control-label" for="form-field-2">Background Admin</label>

					<div class="controls">
						<input type="file" data-allowed-file-extensions="png jpg jpeg" data-show-remove="false" name="bg_admin" id="bg_admin" class="dropify span6" data-default-file="<?php echo ($data_setting[0]->bg_admin != null) ? "/assets/app_setting_upload/img/".$data_setting[0]->bg_admin : "" ;?>" />
					</div>
				</div>
			</div>
            

        </fieldset>

        <div class="form-actions center">
            <button type="submit" name="simpan" id="simpan" class="btn btn-small btn-success">
                Simpan
                <i class="icon-save icon-on-right bigger-110"></i>
            </button>
        </div>


      </form>

		</div>
	</div>
</div>
<script src="<?php echo base_url();?>assets/dropify/js/dropify.js"></script>
<script>
$(document).ready(function() {
    $('.dropify').dropify();

	$('#insert_data').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
          url:"<?php echo site_url('/app_setting/update/')?>",
          type:"POST",
          data:new FormData(this),
          contentType:false,
          cache:false,
          dataType: 'json',
          processData:false,
		  beforeSend:function () {
				$("#simpan").prop("disabled", true)
				var element = document.getElementById("simpan").firstChild;
				element.data = "Loading ...";
			},
          success: function(data) {
			$("#simpan").prop("disabled", false)
			var element = document.getElementById("simpan").firstChild;
			element.data = "Simpan";
            location.reload();
          },
		  error: function (resp) {
			$("#simpan").prop("disabled", false)
				var element = document.getElementById("simpan").firstChild;
				element.data = "Simpan";
				location.reload();
        },
        })
		});

});
</script>