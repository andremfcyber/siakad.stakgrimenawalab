<table  class="table fpTable lcnp table-striped table-bordered table-hover dt-responsive">
    <thead>
        <tr>
            <th class="center">No</th>
            <th class="center">Nomor</th>
            <th class="center">Tanggal</th>
            <th class="center">NIM</th>
            <th class="center">Nama Mahasiswa</th>
            <th class="center">Kelamin</th>
            <th class="center">Prodi</th>
            <th class="center">Kelas</th>
            <th class="center">Jenis Tagihan</th>
            <th class="center">Semester</th>
            <th class="center">Jumlah</th>
        </tr>
    </thead>
    <tbody>
    	<?php
		$i=1;
    $total = 0;
		foreach($data->result() as $dt){
      $infoMhs = $this->model_data->getInfoMhs($dt->nim);
      $nama_mhs = $infoMhs['nama_mhs'];
      $sex = $infoMhs['sex'];
      $kelas = $infoMhs['kelas'];
      $nama_prodi = $infoMhs['nama_prodi'];

      $infoTagihan = $this->model_data->getJenisTagihan($dt->jenis_tagihan_id);
      $nama_tagihan  = $infoTagihan['nama'];
		?>
        <tr>
        	<td class="center span1"><?php echo $i++?></td>
            <td class="center"><?php echo $dt->nomor;?></td>
            <td class="center"><?php echo $this->model_global->tgl_str($dt->tanggal);?></td>
            <td class="center"><?php echo $dt->nim;?></td>
            <td ><?php echo $nama_mhs;?></td>
            <td class="center"><?php echo $sex=='L'?'Laki-laki':'Perempuan';?></td>
            <td ><?php echo $nama_prodi;?></td>
            <td class="center"><?php echo $kelas;?></td>
            <td ><?php echo $nama_tagihan;?></td>
            <td class="center"><?php echo $dt->smt;?></td>
            <td style="text-align:right;"><?php echo number_format($dt->jumlah);?></td>
        </tr>
		<?php
        $total +=$dt->jumlah;
      } ?>
      <!-- <tr>
        <td colspan="10" class="center">
          TOTAL PEMBAYARAN
        </td>
        <td style="text-align:right;">
            <?php echo number_format($total);?>
        </td>
      </tr> -->
    </tbody>
</table>

<div class="grid-container">
    <div class="item1">TOTAL PEMBAYARAN : </div>
    <div class="item2"><?php echo number_format($total);?></div>
</div>
