<?php
if($class=='home'){
	$home = 'class="active"';
	$master ='';
	$transaksi = '';
	$polling = '';
	$laporan = '';
	$grafik = '';
}elseif($class=='master'){
	$home = '';
	$master ='class="active"';
	$transaksi = '';
	$polling = '';
	$laporan = '';
	$grafik = '';
}elseif($class=='transaksi'){
	$home = '';
	$master ='';
	$transaksi = 'class="active"';
	$polling = '';
	$laporan = '';
	$grafik = '';
}elseif($class=='polling'){
	$home = '';
	$master ='';
	$transaksi = '';
	$polling = 'class="active"';
	$laporan = '';
	$grafik = '';
}elseif($class=='laporan'){
	$home = '';
	$master ='';
	$transaksi = '';
	$polling = '';
	$laporan = 'class="active"';
	$grafik = '';
}else{
	$home = '';
	$master ='';
	$transaksi = '';
	$polling = '';
	$laporan = '';
	$grafik = 'class="active"';
}
?>
<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>
<div class="sidebar <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo-overlay':'mhs-pak-overlay';?>" id="sidebar">
    <div class="sidebar-shortcuts <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo-dark':'mhs-pak-dark';?>" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <i class="icon-calendar"></i>
			<?php
			date_default_timezone_set('Asia/Jakarta');
			echo $this->model_global->hari_ini(date('w')).", ".$this->model_global->tgl_indo(date('Y-m-d'));
			?>
        </div>
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div><!--#sidebar-shortcuts-->

    <div align="center" class="padding-t-20">
    <img src="<?php echo base_url();?>assets/img/logo.png" width="80">
    <h6><?php echo $this->config->item('nama_pendek').'<br/>'.$this->config->item('nama_instansi');?></h6>
    </div>

    <ul class="nav nav-list <?php echo ($_SESSION['kd_prodi'] == '201' || $_SESSION['kd_prodi'] == '77101')?'mhs-teo':'mhs-pak';?>">
        <li <?php echo $home;?> >
            <a href="<?php echo base_url();?>index.php/site_mahasiswa/home">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
        </li>

        <li <?php echo $master;?> >
            <a href="#" class="dropdown-toggle">
                <i class="icon-desktop"></i>
                <span class="menu-text"> AKADEMIK </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/mata_kuliah">
                        <i class="icon-double-angle-right"></i>
                        MATA KULIAH
                    </a>
                </li>
                <?php
				$status = @$_SESSION['status'];
				if($status=='Aktif'){
				?>
                 <li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/isi_krs">
                        <i class="icon-double-angle-right"></i>
                        ISI KRS
                    </a>
                </li>
                <?php } ?>
                <li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/krs">
                        <i class="icon-double-angle-right"></i>
                        LIHAT KRS
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/jadwal">
                        <i class="icon-double-angle-right"></i>
                        JADWAL
                    </a>
                </li>
				<li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/khs">
                        <i class="icon-double-angle-right"></i>
                        LIHAT KHS
                    </a>
                </li>
				<li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/wisuda">
                        <i class="icon-double-angle-right"></i>
                        WISUDA
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php/site_mahasiswa/grafik/ip">
                        <i class="icon-double-angle-right"></i>
                        GRAFIK IP
                    </a>
                </li>

            </ul>
        </li>

        <li <?php echo $polling;?> >
            <a href="#" class="dropdown-toggle">
                <i class="icon-edit"></i>
                <span class="menu-text"> KUISIONER </span>
                <b class="arrow icon-angle-down"></b>
            </a>
            <ul class="submenu">
                <li>
                    <a href="<?php echo site_url('site_mahasiswa/poll_dosen');?>">
                        <i class="icon-double-angle-right"></i>
                        DOSEN
                    </a>
                </li>
<!--		<li>
                    <a href="<?php echo site_url('site_mahasiswa/poll_karyawan');?>">
                        <i class="icon-double-angle-right"></i>
                        KARYAWAN
                    </a>
                </li>-->

            </ul>
        </li>


         <li>
            <a href="<?php echo base_url();?>index.php/login/logout">
                <i class="icon-off"></i>
                <span class="menu-text"> Keluar </span>
            </a>
        </li>
    </ul><!--/.nav-list-->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left"></i>
    </div>
</div>
