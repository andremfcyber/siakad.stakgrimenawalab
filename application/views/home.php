<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo app_setting()['data_setting'][0]->nama_aplikasi;?></title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

		<!-- CUSTOM CSS -->
		<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
		<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css"> -->

		<!--basic styles-->
		<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/icon/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-fonts.css" />

		<!--ace styles-->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ace-skins.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/app.css" />
	

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui-1.10.3.custom.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/chosen.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-timepicker.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.gritter.css" />
		<link href="https://fonts.googleapis.com/css?family=Belleza" rel="stylesheet">
		<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

		<link rel="stylesheet" href="<?php echo base_url();?>assets/dropify/css/dropify.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>assets/dropify/css/custom.css" />
        <!--
		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='<?php echo base_url();?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
        -->

		  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/test.css" />	

        <script src="<?php echo base_url();?>assets/js/jquery-2.0.3.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.mobile.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

		<script src="<?php echo base_url();?>assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.ui.touch-punch.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/chosen.jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/flot/jquery.flot.resize.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/date-time/bootstrap-datepicker.min.js"></script>
				<script src="<?php echo base_url();?>assets/js/date-time/bootstrap-timepicker.min.js"></script>

        <!--Table-->
		<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.dataTables.bootstrap.js"></script>
        <script type='text/javascript' src='<?php echo base_url()?>assets/js/jquery/jquery-migrate-1.1.1.min.js'></script>


        <!--ace scripts-->
		<script type='text/javascript' src='<?php echo base_url()?>assets/js/app.js'></script>
        <script src="<?php echo base_url();?>assets/js/jquery.gritter.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace-elements.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/ace.min.js"></script>

		<!-- CUSTOM JS -->
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

		<!-- <script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script> -->

<style media="screen">
	body {
		font-family: 'Belleza', sans-serif;
	}

	.sidebar:before{
		content: "";
		display: block;
		width: 189px;
		position: fixed;
		bottom: 0;
		top: 0;
		z-index: -1;
		background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_admin ?>) !important;
		background-size: cover;
		background-repeat: no-repeat;
		border-right: none
	}

	.navbar .navbar-inner, .ace-nav>li>a, .table-header {
		background-color: <?php echo app_setting()['data_setting'][0]->warna_header_1;?> !important;
	}

	.breadcrumbs, .sidebar-shortcuts, .nav-list>li.active>a, .nav-list>li.active>a:hover, .nav-list>li.active>a:focus, .nav-list>li.active>a:active {
		background-color: <?php echo app_setting()['data_setting'][0]->warna_header_2;?> !important;
	}
</style>
<!-- /*<style>
	.text-center {
		text-align: center;
	}
	.text-right {
		text-align: right;
	}
</style>*/ -->


	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner <?php echo ($_SESSION['role'] == 'keuangan')?'keuangan':'';?>">
				<div class="container-fluid">
				<!-- <?=$_SESSION['role'];?> -->
					<a href="#" class="brand">
						<small>
							<!--<i class="icon-leaf"></i>-->
							<?php echo app_setting()['data_setting'][0]->nama_aplikasi;?> Tahun Akademik <?php echo $this->model_global->getThAkademikAktif()['th_akademik'].' - '.$this->model_global->getThAkademikAktif()['kode'];?>
						</small>
					</a><!--/.brand-->

					<?php echo $this->load->view('notifikasi');?>
                </div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>

		<?php
		$username = @$_SESSION['username'];
		$infoUser = $this->model_data->getInfoUser($username);
		$level = $infoUser['level'];
		if($level=='super admin'){
				echo $this->view('menu');
		}elseif($level=='admin'){
			echo $this->view('menuadmin');
		}elseif($level=='keuangan'){
			echo $this->view('menukeuangan');
		}else{
			redirect('login/logout');
		}


		?>

			<div class="main-content">
				<div class="breadcrumbs <?php echo ($_SESSION['role'] == 'keuangan')?'keuangan-dark':'';?>" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home home-icon"></i>
							<a href="<?php echo base_url();?>index.php/home">Home</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						<li class="active">
						<a href="<?php echo base_url();?>index.php/<?php echo $this->uri->segment('1');?>"><?php echo $judul;?></a>
						</li>
					</ul><!--.breadcrumb-->
                    <div class="pull-right T1">
                    Copyright &copy; <?php echo app_setting()['data_setting'][0]->nama_pendek;?> <?php echo app_setting()['data_setting'][0]->nama_instansi;?> Tahun 2021
                    </div>
				</div>

				<div class="page-content">
					<!-- <pre>
						<?php
								print_r($_SESSION);
						?>
					</pre>
					 -->
					<?php echo $this->load->view($content);?>
				</div><!--/.page-content-->
			</div><!--/.main-content-->
		</div><!--/.main-container-->

		<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
			<i class="icon-double-angle-up icon-only bigger-110"></i>
		</a>
		<script type="text/javascript">
    		$('table').addClass(' dt-responsive ');
		</script>
	<!-- </body> </html> -->