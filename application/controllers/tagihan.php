<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tagihan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Tagihan Mahasiswa";
			$d['class'] = "keuangan";
      $d['th_akademik_aktif'] = $this->model_global->getThAkademikAktif()['kode'];
			$d['content'] = 'tagihan/create';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function cari_mhs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $dt = $this->model_data->getInfoMhs($nim);
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}

  public function jumlah_tagihan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id = $this->input->post('id');
      $dt = $this->model_data->getJenisTagihan($id);
      echo json_encode($dt);
		}else{
			redirect('login','refresh');
		}
	}

  public function data_detail()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$nim = $this->input->post('nim');
      $this->db->where('nim',$nim);
      $dt['data'] = $this->db->get('tagihan_mhs')->result();
      $this->load->view('tagihan/data_detail',$dt);
		}else{
			redirect('login','refresh');
		}
	}

  public function simpan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
      date_default_timezone_set('Asia/Jakarta');

      $jenis_tagihan_id = $this->input->post('jenis_tagihan_id');
			// $nama_tagihan = $this->model_data->getJenisTagihan($jenis_tagihan_id);
      $nim = $this->input->post('nim');
      $kd_prodi = $this->input->post('kd_prodi');
      $kelas = $this->input->post('kelas');
      $th_akademik_kode = $this->input->post('th_akademik');
      $th_angkatan_kode = $this->input->post('th_angkatan');
      $smt =  $this->model_global->semester($nim,$th_akademik_kode);
      $jumlah = str_replace(',','',$this->input->post('jumlah'));

      $dt = array('jenis_tagihan_id' => $jenis_tagihan_id,
								// 'nama_tagihan' => $nama_tagihan,
								'nim' => $nim,
                'kd_prodi' => $kd_prodi,
                'kelas' => $kelas,
                'th_akademik_kode' => $th_akademik_kode,
                'th_angkatan_kode' => $th_angkatan_kode,
                'smt' => $smt,
                'jumlah' => $jumlah,
                'user_id' => @$_SESSION['username']
                );


      $id = array('th_akademik_kode' => $th_akademik_kode,
                  'jenis_tagihan_id' => $jenis_tagihan_id,
                  'nim' => $nim
                  );
      $data = $this->db->get_where('tagihan_mhs',$id);
      if($data->num_rows()>0){
				$dt['update_date'] = date('Y-m-d H:i:s');
        $this->db->update('tagihan_mhs',$dt,$id);
        echo "Tagihan Mahasiswa berhasil di UBAH";
      }else{
				$dt['insert_date'] = date('Y-m-d H:i:s');
        $this->db->insert('tagihan_mhs',$dt);
        echo "Tagihan Mahasiswa berhasil di SIMPAN";
      }

		}else{
			redirect('login','refresh');
		}
	}

}
