<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll_pertanyaan extends CI_Controller {

    /**
     * Index Page for this controller.
     * Programmer : Deddy Rusdiansyah.S.Kom
     * http://deddyrusdiansyah.blogspot.com
     * http://softwarebanten.com
     * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
     * Developer : Fitria Wahyuni.S.Pd
     */
    public function index() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $d['judul'] = "Pertanyaan Kuesioner";
            $d['class'] = "polling";
            // $d['data'] = $this->db->order_by('id','DESC')->get('poll_jenis');

            $d['content'] = 'poll_pertanyaan/view';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function get_json() {
        $this->db->select('a.*,b.nama as nama_jenis');
        $this->db->from('poll_pertanyaan as a');
        $this->db->join('poll_jenis as b', 'b.id=a.poll_jenis_id', 'left');
        $this->db->order_by('a.id', 'DESC');
        $results = $this->db->get()->result_array();
        $data = array();
        $no = 1;
        foreach ($results as $r) {
            array_push($data, array(
                $no++,
                $r['nama_jenis'],
                $r['pertanyaan'],
                $r['pilihan'],
                $r['aktif'],
                anchor('poll_pertanyaan/edit/' . $r['id'], 'Edit', array('class' => 'btn btn-mini btn-primary')) . '  ' .
                anchor('poll_pertanyaan/hapus/' . $r['id'], 'Delete', array("onclick" => "return confirm('yakin akan menghapus ?')", 'class' => 'btn btn-mini btn-danger'))
            ));
        }

        echo json_encode(array('data' => $data));
    }

    public function tambah() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $d['judul'] = "Tambah Pertanyaan Polling";
            $d['class'] = "polling";
            $d['id'] = '';
            $d['poll_jenis_id'] = '';
            $d['pertanyaan'] = '';
            $d['pilihan'] = '';
            $d['aktif'] = '';

            $d['content'] = 'poll_pertanyaan/add';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function edit() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id = $this->uri->segment(3);

            $this->db->where('id', $id);
            $get = $this->db->get('poll_pertanyaan');
            if ($get->num_rows() > 0) {
                $row = $get->row();
                $poll_jenis_id = $row->poll_jenis_id;
                $pertanyaan = $row->pertanyaan;
                $pilihan = $row->pilihan;
                $aktif = $row->aktif;
            } else {
                $poll_jenis_id = '';
                $pertanyaan = '';
                $pilihan = '';
                $aktif = '';
            }
            $d['judul'] = "Tambah Pertanyaan Polling";
            $d['class'] = "polling";
            $d['id'] = $id;
            $d['poll_jenis_id'] = $poll_jenis_id;
            $d['pertanyaan'] = $pertanyaan;
            $d['pilihan'] = $pilihan;
            $d['aktif'] = $aktif;

            $d['content'] = 'poll_pertanyaan/add';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function simpan() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {

            // print_r($_POST);die;
            $id['id'] = (int)$this->input->post('id');

            $exp_pilihan = $this->input->post('pilihan');
            $pilihan = implode(',', $exp_pilihan);
            if ($this->input->post('aktif')) {
                $aktif = 'Y';
            } else {
                $aktif = 'T';
            }

            $dt['poll_jenis_id'] = $this->input->post('poll_jenis_id');
            $dt['pertanyaan'] = $this->input->post('pertanyaan');
            $dt['pilihan'] = $pilihan;
            $dt['aktif'] = $aktif;

            $dt['user_id'] = @$_SESSION['username'];


            $q = $this->db->get_where("poll_pertanyaan", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $dt['update_date'] = date('Y-m-d H:i:s');
                $this->db->update("poll_pertanyaan", $dt, $id);
                // echo "Data Sukses diUpdate";
                $this->session->set_flashdata('info', 'Update data berhasil');
            } else {
                $dt['insert_date'] = date('Y-m-d H:i:s');
                $this->db->insert("poll_pertanyaan", $dt);
                // echo "Data Sukses diSimpan";
                $this->session->set_flashdata('info', 'Insert data berhasil');
            }
            redirect('poll_pertanyaan/tambah');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function hapus() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->uri->segment(3);

            $q = $this->db->get_where("poll_pertanyaan", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $this->db->delete("poll_pertanyaan", $id);
                $this->session->set_flashdata('info', 'Delete data berhasil');
            }
            redirect('poll_pertanyaan', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
