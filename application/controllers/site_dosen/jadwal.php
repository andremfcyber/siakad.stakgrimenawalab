<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){

			$d['judul']="Jadwal Mengajar ";
			$d['class'] = "master";

			$d['content']= 'site_dosen/jadwal/form';
			$this->load->view('site_dosen/home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_smt()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){
			$kd_dosen = @$_SESSION['username'];
			$th_ak	= $this->input->post('th_ak');
			if(!empty($th_ak)){
				if(substr($th_ak,4,1)==1){
					$smt = 'Ganjil';
				}else{
					$smt = 'Genap';
				}

				$d['semester'] = $smt;
				//$d['smt'] = $this->model_global->semester($nim,$th_ak);
			}else{
				$d['semester'] = '';
				//$d['smt'] = '';
			}
			echo json_encode($d);

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='dosen'){

			$kd_dosen = @$_SESSION['username'];
			$th_ak = $this->input->post('thak');

			// $where = "WHERE th_akademik='$th_ak' AND kd_dosen='$kd_dosen'";

			$this->db->where('th_akademik',$th_ak);
			$this->db->where('kd_dosen',$kd_dosen);
			$this->db->group_by('kd_mk,kelas, krs.id_krs');
			$this->db->order_by('id_jadwal');

			$q = $this->db->get('krs'); // $this->db->query("SELECT * FROM krs $where GROUP BY th_akademik,kd_mk ");

			// print_r($q->num_rows());
			// die;

			if($q->num_rows()>0){
				$dt['data'] = $q;
				echo $this->load->view('site_dosen/jadwal/view',$dt);
			}else{
				echo $this->load->view('site_dosen/view_kosong');
			}

		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
