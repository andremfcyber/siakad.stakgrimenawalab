<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poll_karyawan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	 public function __construct()
   {
       parent::__construct();
       $this->load->model('model_polling');
   }

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='mahasiswa'){

			$th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];
			$nim = @$_SESSION['username'];
			$jenis = 'KARYAWAN';

			$this->db->where('a.th_akademik',$th_akademik);
			$this->db->where('a.nim',$nim);
			$this->db->where('b.nama',$jenis);
			$this->db->from('poll_jawaban as a');
			$this->db->join('poll_jenis as b','a.poll_jenis_id=b.id');
			$get = $this->db->get()->num_rows();

			$kd_prodi = @$_SESSION['kd_prodi'];
			$nama_prodi = $this->model_data->getInfoProdi($kd_prodi)['prodi'];

			$this->db->select('pegawai_id');
      $this->db->where('pegawai_id !=','NULL');
			$this->db->where('th_akademik',$th_akademik);
			$this->db->where('nim',$nim);
			$this->db->from('poll_jawaban');
			$get_dosen_pilih = $this->db->get();
			// print_r($this->db->last_query());die;
			$arr =  array();
			foreach($get_dosen_pilih->result() as $value)
			{
				$arr[] = $value->pegawai_id;
			}
			$json_arr = $arr;
      // print_r($json_arr);die;

			$this->db->select('id_username,nama_lengkap');
			// $this->db->where('a.kd_prodi',$kd_prodi);
			$this->db->where('blokir','Tidak');
      if(!empty($json_arr)){
			$this->db->where_not_in('id_username',$json_arr);
      }
			$this->db->from('admins as a');
			$this->db->order_by('nama_lengkap');
			$get_dosen = $this->db->get();
			// print_r($this->db->last_query());die;

			$d['judul'] = "Kuisioner Karyawan";
                        $d['sub_judul'] = "Kuisioner Karyawan Program Studi ".$nama_prodi." Tahun Akademik ".$th_akademik;
                        $d['class'] = "polling";
			$d['list_dosen'] = $get_dosen;
			// $d['list_dosen_pilih'] = $list_dosen_pilih;
			// if($get > 0 )
			// {
			// 	$d['content']= 'site_mahasiswa/polling/warning';
			// }else{
				$d['content']= 'site_mahasiswa/polling/poll_karyawan';
			// }
			$this->load->view('site_mahasiswa/home',$d);
		}else{
			redirect('login','refresh');
		}
	}


	public function simpan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='mahasiswa'){
			date_default_timezone_set('Asia/Jakarta');

			$th_akademik = $this->model_global->getThAkademikAktif()['th_akademik'];
			$nim = @$_SESSION['username'];
			$kd_prodi = @$_SESSION['kd_prodi'];
			// print_r($_POST);die;

			$keyth['th_akademik'] 	= $th_akademik;
			$keyth['nim'] 		= $nim;
			$keyth['poll_jenis_id'] = $this->model_polling->getIDJenis('KARYAWAN')['id'];
			$keyth['pegawai_id'] 	= $this->input->post('pegawai_id');


			$dth['th_akademik'] 	= $th_akademik;
			$dth['nim'] 		= $nim;
			$dth['poll_jenis_id']   = $this->model_polling->getIDJenis('KARYAWAN')['id'];
			$dth['pegawai_id'] 	= $this->input->post('pegawai_id');
			$dth['kd_prodi'] 	= $kd_prodi;
			$dth['user_id']         = @$_SESSION['username'];
			$dth['insert_date']     = date('Y-m-d H:i:s');

			$get = $this->db->get_where('poll_jawaban',$keyth);
			if($get->num_rows()>0)
			{
				$this->session->set_flashdata('info', 'Maaf, Anda sudah pernah mengisi Polling');
			}else{
				$this->db->insert('poll_jawaban',$dth);
                                $dtd['poll_jawaban_id'] = $this->db->insert_id();
				//$dtd['poll_jawaban_id'] = $this->model_polling->getIDJawaban($keyth);

				$get_pertanyaan = $this->model_polling->getDataPertanyaan($th_akademik,'KARYAWAN');
				$row = $get_pertanyaan->row();
				$exp_pertanyaan = $row->poll_pertanyaan_id;
				$pertanyaan = explode(',',$exp_pertanyaan);
				foreach($pertanyaan as $value)
				{
					$dtd['poll_pertanyaan_id'] = $this->input->post('tanya_'.$value);
					$dtd['jawaban'] = $this->input->post('jawab_'.$value);
					// echo $tanya.' - '.$jawab.'<br/>';
					$this->db->insert('poll_jawaban_detail',$dtd);
				}
				$this->session->set_flashdata('info', 'Silahkan Isi Kuesioner untuk KARYAWAN/PEGAWAI lainnya..!!');
				redirect('site_mahasiswa/poll_karyawan','refresh');
			}

		}else{
			redirect('login','refresh');
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
