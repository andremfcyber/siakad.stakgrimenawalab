<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_setting extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];

        if( empty($cek) && $level != 'admin'){
            redirect('login','refresh');
        }
        $nama_file = '';
        $this->load->model('model_appsetting');
    }

	public function index() {
        $d['judul']="App Setting";
        $d['class'] = "app_setting";
        $d['data_setting'] = $this->model_appsetting->get_setting()->result();
        $d['content'] = 'app_setting/index';
        $this->load->view('home',$d);

	}

    // File upload configuration
	public function upload_img() {
		$nmfile = "file_".time();
		$config['upload_path'] = './assets/app_setting_upload/img/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['file_name'] = $nmfile;
        $nama_file = $nmfile;
        $config['max_size'] = 20000;

		// Load and initialize upload library
		$this->load->library('upload', $config);
	}

    public function upload_img2() {
        $config2['upload_path'] = './pmb/assets/app_setting_upload/img/';
		$config2['allowed_types'] = 'gif|jpg|png|jpeg';
		$config2['file_name'] = $nama_file;
        $config2['max_size'] = 20000;

        $this->load->library('upload', $config2, 'pmb');
        $this->pmb->initialize($config2);
        
    }

    public function update() {      
        
        $data['nama_aplikasi'] = $this->input->post('nama_aplikasi');
        $data['nama_pendek'] = $this->input->post('nama_pendek');
        $data['nama_instansi'] = $this->input->post('nama_instansi');
        $data['alamat_instansi'] = $this->input->post('alamat_instansi');
        $data['alamat1'] = $this->input->post('alamat1');
        $data['alamat2'] = $this->input->post('alamat2');
        $data['website'] = $this->input->post('website');
        $data['email'] = $this->input->post('email');
        $data['kota'] = $this->input->post('kota');
        $data['warna_header_1'] = $this->input->post('warna_header_1');
        $data['warna_header_2'] = $this->input->post('warna_header_2');

        $condition = [
			'column' => 'id',
			'value' => 1
		];

        $status = $this->model_appsetting->update($data, $condition);
        
        // Call upload image configuration
		$this->upload_img();
        
        // Upload logo
        if ( $_FILES['logo']['name'] ) {
            if ( $this->upload->do_upload('logo') ) {
                $logo = $this->upload->data();
                $data_logo['logo'] = $logo['file_name'];
                
                $this->upload_img2();
                $this->pmb->do_upload('logo');
                $this->pmb->data();
                $status = $this->model_appsetting->update($data_logo, $condition);
                
            }
        }

        // Upload bg login
        if ( $_FILES['bg_login']['name'] ) {
            if ( $this->upload->do_upload('bg_login') ) {
                $bg_login = $this->upload->data();
                $data_bg_login['bg_login'] = $bg_login['file_name'];

                $status = $this->model_appsetting->update($data_bg_login, $condition);
            }
        }


        // Upload bg admin
        if ( $_FILES['bg_admin']['name'] ) {
            if ( $this->upload->do_upload('bg_admin') ) {
                $bg_admin = $this->upload->data();
                $data_bg_admin['bg_admin'] = $bg_admin['file_name'];

                $status = $this->model_appsetting->update($data_bg_admin, $condition);
            }
        }
        


        if($status) {
            $this->session->set_flashdata("pesan", 
						"<div class=\"col-md-12\">
						<div class=\"alert alert-info\"id=\"alert\">
						<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
						</button>
						Update data berhasil!</div></div>");
         } else {
            $this->session->set_flashdata("pesan", 
						"<div class=\"col-md-12\">
						<div class=\"alert alert-info\"id=\"alert\">
						<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
						</button>
						Update data gagal!</div></div>");
        }
    }
}

