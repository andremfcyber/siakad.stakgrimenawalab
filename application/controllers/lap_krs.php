<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lap_krs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Laporan KRS Mahasiswa";
			$d['class'] = "laporan";

			$d['content']= 'laporan/lap_krs';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_smt()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$th_ak	= $this->input->post('th_ak');
			if(!empty($th_ak)){
				if(substr($th_ak,4,1)==1){
					$smt = 'Ganjil';
				}else{
					$smt = 'Genap';
				}

				$d['semester'] = $smt;
			}else{
				$d['semester'] = '';
			}
			echo json_encode($d);

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			$th_ak = $this->input->post('th_ak');
			$smt = $this->input->post('smt');

			$th = $this->input->post('th');
			$kd_prodi = $this->input->post('kd_prodi');
			$kelas = $this->input->post('kelas');

			/*
			$query = "SELECT *
						FROM krs as a
						RIGHT JOIN mahasiswa as b
						ON a.nim = b.nim
					WHERE a.th_akademik='$th_ak' AND a.semester='$smt' AND b.kd_prodi='$kd_prodi'
					GROUP BY b.nim";
			*/
			$this->db->where('status','Aktif');
			$this->db->where('th_akademik',$th);
			$this->db->where('kd_prodi',$kd_prodi);
			$this->db->where('kelas',$kelas);

			$q = $this->db->get('mahasiswa'); // "SELECT * FROM mahasiswa WHERE status='Aktif' AND th_akademik='$th' AND kd_prodi='$kd_prodi'";
			// $q = $this->db->query($query);
			// die ($this->db->last_query());
			$dt['th_ak'] = $th_ak;
			$dt['smt']= $smt;
			$dt['data'] = $q;

			echo $this->load->view('laporan/view_lap_krs',$dt);

		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
