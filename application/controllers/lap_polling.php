<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lap_polling extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	 public function __construct()
   {
       parent::__construct();
       $this->load->model('model_polling');
   }

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Laporan Kuesioner";
			$d['class'] = "laporan";

			$d['content']= 'laporan/lap_polling';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_pertanyaan()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = $this->input->post('th_ak');
			$poll_jenis_id = $this->input->post('poll_jenis_id');

			$this->db->where('th_akademik',$th_ak);
			$this->db->where('poll_jenis_id',$poll_jenis_id);
			$this->db->from('poll_transaksi');
			$get =$this->db->get();
			if($get->num_rows()>0)
			{
				$row = $get->row();
				$exp = $row->poll_pertanyaan_id;
				$pertanyaan = explode(',',$exp);
				echo "<option value=''>-Pilih-</option>";
				foreach($pertanyaan as $value)
				{
					$getTanya = $this->model_polling->getInfoPertanyaan($value);
					echo '<option value='.$value.'>'.$getTanya['pertanyaan'].'</option>';
				}

			}

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_object()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = $this->input->post('th_ak');
			$poll_jenis_id = $this->input->post('poll_jenis_id');

			$this->db->select('a.kd_dosen,b.nama_dosen');
			$this->db->where('a.th_akademik',$th_ak);
			$this->db->where('a.poll_jenis_id',$poll_jenis_id);
			$this->db->where('a.kd_dosen !=','NULL');
			// $this->db->where('pegawai_id !=','NULL');
			$this->db->from('poll_jawaban as a');
			$this->db->join('dosen as b','a.kd_dosen=b.kd_dosen');
			$this->db->group_by('a.kd_dosen,b.nama_dosen');
			$this->db->order_by('b.nama_dosen');
			$get =$this->db->get();
			if($get->num_rows()>0)
			{
				$row = $get->row();
				// $exp = $row->poll_pertanyaan_id;
				// $pertanyaan = explode(',',$exp);
				echo "<option value=''>-Pilih-</option>";
				foreach($get->result() as $value)
				{
					$getObject = $value->nama_dosen;// $this->model_polling->getInfoPertanyaan($value);
					echo '<option value='.$value->kd_dosen.'>'.$getObject.'</option>';
				}

			}else{
				$this->db->select('a.pegawai_id,b.nama_lengkap');
				$this->db->where('a.th_akademik',$th_ak);
				$this->db->where('a.poll_jenis_id',$poll_jenis_id);
				// $this->db->where('kd_dosen !=','NULL');
				$this->db->where('a.pegawai_id !=','NULL');
				$this->db->from('poll_jawaban as a');
				$this->db->join('admins as b','a.pegawai_id=b.id_username');
				$this->db->group_by('a.pegawai_id');
				$this->db->group_by('b.nama_lengkap');
				$get =$this->db->get();
				if($get->num_rows()>0)
				{
					$row = $get->row();
					// $exp = $row->poll_pertanyaan_id;
					// $pertanyaan = explode(',',$exp);
					echo "<option value=''>-Pilih-</option>";
					foreach($get->result() as $value)
					{
						$getObject = $value->nama_lengkap;// $this->model_polling->getInfoPertanyaan($value);
						echo '<option value='.$value->pegawai_id.'>'.$getObject.'</option>';
					}
				}

			}

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = $this->input->post('th_ak');
			$kd_prodi = $this->input->post('kd_prodi');
			$poll_jenis_id = $this->input->post('poll_jenis_id');
			$poll_pertanyaan_id = $this->input->post('poll_pertanyaan_id');
			$object = $this->input->post('object');

			if(!empty($kd_prodi)){
        $this->db->where('a.kd_prodi',$kd_prodi);
			}

			if(!empty($poll_jenis_id)){
        $this->db->where('a.poll_jenis_id',$poll_jenis_id);
			}

			if(!empty($poll_pertanyaan_id)){
        $this->db->where('b.poll_pertanyaan_id',$poll_pertanyaan_id);
			}

			if(!empty($object)){
        $this->db->where('a.kd_dosen',$object);
        $this->db->or_where('a.pegawai_id',$object);
			}

			$this->db->where('a.th_akademik',$th_ak);
			$this->db->from('poll_jawaban as a');
			$this->db->join('poll_jawaban_detail as b','a.id=b.poll_jawaban_id');
			//$this->db->group_by('th_akademik,nim');
			$this->db->order_by('nim');
			$q = $this->db->get();
			$r = $q->num_rows();
			if($r>0){
				$dt['data'] = $q;
				echo $this->load->view('laporan/view_lap_polling',$dt);
			}else{
				echo $this->load->view('laporan/view_kosong');
			}

		}else{
			redirect('login','refresh');
		}
	}

	public function cetak_pdf()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = $this->input->post('th_ak');
			$kd_prodi = $this->input->post('kd_prodi');
			$poll_jenis_id = $this->input->post('poll_jenis_id');
			$poll_pertanyaan_id = $this->input->post('poll_pertanyaan_id');
			$object = $this->input->post('object');

			if(!empty($kd_prodi))
			{
					$this->db->where('a.kd_prodi',$kd_prodi);
			}

			if(!empty($poll_jenis_id))
			{
					$this->db->where('a.poll_jenis_id',$poll_jenis_id);
			}

			if(!empty($poll_pertanyaan_id))
			{
					$this->db->where('b.poll_pertanyaan_id',$poll_pertanyaan_id);
			}

			if(!empty($object))
			{
					$this->db->where('a.kd_dosen',$object);
					$this->db->or_where('a.pegawai_id',$object);
			}

			$this->db->where('a.th_akademik',$th_ak);
			$this->db->from('poll_jawaban as a');
			$this->db->join('poll_jawaban_detail as b','a.id=b.poll_jawaban_id');
			//$this->db->group_by('th_akademik,nim');
			$this->db->order_by('nim');
			$q = $this->db->get();

			$r = $q->num_rows();
			if($r>0){
				$_SESSION['th_ak'] = $th_ak;
				$_SESSION['kd_prodi'] = $kd_prodi;
				$_SESSION['poll_jenis_id'] = $poll_jenis_id;
				$_SESSION['poll_pertanyaan_id'] = $poll_pertanyaan_id;
				$_SESSION['object'] = $object;
				// $this->session->set_userdata($sess_data);
				echo "Sukses";
			}else{
				echo "Maaf, Tidak Ada data";
			}

		}else{
			redirect('login','refresh');
		}
	}



	public function print_excel()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = @$_SESSION['th_ak'];
			$kd_prodi = @$_SESSION['kd_prodi'];
			$prodi = $this->model_data->nama_jurusan($kd_prodi);
			$poll_jenis_id = @$_SESSION['poll_jenis_id'];
			$poll_pertanyaan_id = @$_SESSION['poll_pertanyaan_id'];
			$object = @$_SESSION['object'];

			if(!empty($kd_prodi))
			{
					$this->db->where('a.kd_prodi',$kd_prodi);
			}

			if(!empty($poll_jenis_id))
			{
					$this->db->where('a.poll_jenis_id',$poll_jenis_id);
			}

			if(!empty($poll_pertanyaan_id))
			{
					$this->db->where('b.poll_pertanyaan_id',$poll_pertanyaan_id);
			}

			if(!empty($object))
			{
					$this->db->where('a.kd_dosen',$object);
					$this->db->or_where('a.pegawai_id',$object);
			}

			$this->db->where('a.th_akademik',$th_ak);
			$this->db->from('poll_jawaban as a');
			$this->db->join('poll_jawaban_detail as b','a.id=b.poll_jawaban_id');
			//$this->db->group_by('th_akademik,nim');
			$this->db->order_by('nim');
			$q = $this->db->get();
			$r = $q->num_rows();

			if($r>0){

				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=LAPORAN_Kuesioner_".$th_ak.".xls");
				header("Pragma: no-cache");
				header("Expires: 0");
			?>
            <p>LAPORAN KUESIONER MAHASISWA</p>
            <p><?php echo $th_ak;?></p>
            <table border="1">
            	<thead>
                	<tr>
										<th class="center">No</th>
										<th class="center span2">NIM</th>
										<th class="center">Nama Mahasiswa</th>
										<th class="center">L/P</th>
										<th class="center">PRODI</th>
										<th class="center">Tanggal</th>
					</tr>
				</thead>
                <tbody>
                <?php
				$no=1;
				foreach($q->result() as $dt){
					$infoMhs = $this->model_data->getInfoMhs($dt->nim);
		      $nama_mhs = $infoMhs['nama_mhs'];
		      $sex = $infoMhs['sex'];
		      $prodi = $infoMhs['kd_prodi'];
		  		$tgl = $dt->insert_date; //'';// $this->model_global->tgl_indo($dt->tgl_daftar);
				?>
				<tr>
					<td class="center span1"><?php echo $no++?></td>
						<td class="center span2"><?php echo $dt->nim;?></td>
						<td ><?php echo $nama_mhs;?></td>
						<td class="center"><?php echo $sex;?></td>
						<td><?php echo $prodi;?></td>
						<td class="center"><?php echo $tgl;?></td>
				</tr>
            <?php
				$no++;
				}
			?>
            	</tbody>
               </table>
             <?php
			}
		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
