<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
   * Developer : Fitria Wahyuni.S.Pd
	 */

   function __construct(){
       parent::__construct();
       $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
   }

	public function matakuliah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Import Data Mata Kuliah";
			$d['class'] = "import";
      // $d['data'] = $this->db->get('admins');
			$d['content'] = 'import/matakuliah';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}


  public function upload_matakuliah(){
        $fileName = time().$_FILES['file']['name'];

        $config['upload_path'] = './assets/tmp_import/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 10000;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();

        $media = $this->upload->data('file');
        $inputFileName = './assets/tmp_import/'.$media['file_name'];

        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);

                //Sesuaikan sama nama kolom tabel di database
                $smt = $rowData[0][3];
                if(($smt % 2)==0){
                  $semester = 'Genap';
                }else{
                  $semester = 'Ganjil';
                }

                // $id['kd_mk'] = $rowData[0][1];

                 $data = array(
                    "kd_prodi"=> $rowData[0][0],
                    "kd_mk"=> $rowData[0][1],
                    "nama_mk"=> $rowData[0][2],
                    "smt"=> $rowData[0][3],
                    "sks"=> $rowData[0][4],
                    "semester"=> $semester,
                    "tgl_insert"=> date('Y-m-d H:i:s'),
                    "aktif" => "Ya"
                );

                //sesuaikan nama dengan nama tabel
                // $q = $this->db->get_where("mata_kuliah",$id);
          			// $row = $q->num_rows();
          			// if($row==0){
                  $insert = $this->db->insert("mata_kuliah",$data);
                // }
                delete_files($media['file_path']);

            }
        $this->session->set_flashdata('info', 'Upload Data Berhasil');
        redirect('import/matakuliah/');
    }

    /** Dosen **/

    public function dosen()
  	{
  		$cek = @$_SESSION['logged_in'];
  		$level = @$_SESSION['level'];
  		if(!empty($cek) && $level=='admin'){
  			$d['judul']="Import Data Dosen";
  			$d['class'] = "import";
        // $d['data'] = $this->db->get('admins');
  			$d['content'] = 'import/dosen';
  			$this->load->view('home',$d);
  		}else{
  			redirect('login','refresh');
  		}
  	}


    public function upload_dosen(){
          $fileName = time().$_FILES['file']['name'];

          $config['upload_path'] = './assets/tmp_import/'; //buat folder dengan nama assets di root folder
          $config['file_name'] = $fileName;
          $config['allowed_types'] = 'xls|xlsx|csv';
          $config['max_size'] = 10000;

          $this->load->library('upload');
          $this->upload->initialize($config);

          if(! $this->upload->do_upload('file') )
          $this->upload->display_errors();

          $media = $this->upload->data('file');
          $inputFileName = './assets/tmp_import/'.$media['file_name'];

          try {
                  $inputFileType = IOFactory::identify($inputFileName);
                  $objReader = IOFactory::createReader($inputFileType);
                  $objPHPExcel = $objReader->load($inputFileName);
              } catch(Exception $e) {
                  die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
              }

              $sheet = $objPHPExcel->getSheet(0);
              $highestRow = $sheet->getHighestRow();
              $highestColumn = $sheet->getHighestColumn();

              for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
                  $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                  NULL,
                                                  TRUE,
                                                  FALSE);

                  //Sesuaikan sama nama kolom tabel di database

                   $data = array(
                      "kd_prodi"=> $rowData[0][0],
                      "kd_dosen"=> $rowData[0][1],
                      "nidn"=> $rowData[0][2],
                      "nama_dosen"=> $rowData[0][3],
                      "sex"=> $rowData[0][4],
                      "tempat_lahir"=> $rowData[0][5],
                      "tanggal_lahir"=> $rowData[0][6],
                      "alamat"=> $rowData[0][7],
                      "hp"=> $rowData[0][8],
                      "tgl_masuk"=> $rowData[0][9],
                      "password" => md5('dosen'),
                      "tgl_insert"=> date('Y-m-d H:i:s'),
                      "pendidikan"=> $rowData[0][10],
                      "prodi"=> $rowData[0][11],
                      "status"=> "Aktif"
                  );

                  //sesuaikan nama dengan nama tabel
                  // $q = $this->db->get_where("mata_kuliah",$id);
            			// $row = $q->num_rows();
            			// if($row==0){
                    $insert = $this->db->insert("dosen",$data);
                  // }
                  delete_files($media['file_path']);

              }
          $this->session->set_flashdata('info', 'Upload Data Berhasil ');
          redirect('import/dosen/');
      }


      /** Mahasiswa **/

      public function mahasiswa()
    	{
    		$cek = @$_SESSION['logged_in'];
    		$level = @$_SESSION['level'];
    		if(!empty($cek) && $level=='admin'){
    			$d['judul']="Import Data Mahasiswa";
    			$d['class'] = "import";
          // $d['data'] = $this->db->get('admins');
    			$d['content'] = 'import/mahasiswa';
    			$this->load->view('home',$d);
    		}else{
    			redirect('login','refresh');
    		}
    	}


      public function upload_mahasiswa(){
            $fileName = time().$_FILES['file']['name'];

            $config['upload_path'] = './assets/tmp_import/'; //buat folder dengan nama assets di root folder
            $config['file_name'] = $fileName;
            $config['allowed_types'] = 'xls|xlsx|csv';
            $config['max_size'] = 10000;

            $this->load->library('upload');
            $this->upload->initialize($config);

            if(! $this->upload->do_upload('file') )
            $this->upload->display_errors();

            $media = $this->upload->data('file');
            $inputFileName = './assets/tmp_import/'.$media['file_name'];

            try {
                    $inputFileType = IOFactory::identify($inputFileName);
                    $objReader = IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);

                    //Sesuaikan sama nama kolom tabel di database
                    // $nim = $rowData[0][2];
                    // $id['nim'] = $nim;
                     $data = array(
                        "kd_prodi"=> $rowData[0][0],
                        "th_akademik"=> $rowData[0][1],
                        "nim"=> $rowData[0][2],
                        "nama_mhs"=> $rowData[0][3],
                        "sex"=> $rowData[0][4],
                        "tempat_lahir"=> $rowData[0][5],
                        "tanggal_lahir"=> $rowData[0][6],
                        "alamat"=> $rowData[0][7],
                        "kota"=> $rowData[0][8],
                        "hp"=> $rowData[0][9],
                        "email"=> $rowData[0][10],
                        "nama_ayah"=> $rowData[0][11],
                        "nama_ibu" => $rowData[0][12],
                        "alamat_ortu"=> $rowData[0][13],
                        "hp_ortu"=> $rowData[0][14],
                        "tgl_masuk"=> $rowData[0][15],
                        "password"=> md5($rowData[0][2]),
						"kelas"=> $rowData[0][16],
                        "tgl_insert"=> date('Y-m-d H:i:s'),
                        "status"=> "Aktif"
                    );

                    //sesuaikan nama dengan nama tabel
                    // $q = $this->db->get_where("mahasiswa",$id);
              			// $row = $q->num_rows();
              			// if($row>0){
                    //   echo "NIM ".$nim." Sudah ada pada table Mahasiswa";
                    //   die;
                    // }else{
                      $insert = $this->db->insert("mahasiswa",$data);
                    // }
                    delete_files($media['file_path']);

                }
            $this->session->set_flashdata('info', 'Upload Data Berhasil ');
            redirect('import/mahasiswa/');
        }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
