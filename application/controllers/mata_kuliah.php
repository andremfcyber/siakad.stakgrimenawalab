<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mata_kuliah extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Mata Kuliah";
			$d['class'] = "master";

			$_SESSION['sesi_kd_prodi'] = '';
			$_SESSION['sesi_singkat_prodi'] = '';
			$_SESSION['sesi_id_prodi'] = '';

			// $this->session->set_userdata($sess_data);

			$d['content'] = 'mata_kuliah/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function view_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Mata Kuliah";
			$d['class'] = "master";

			//$kd_prodi = $this->input->post('cari_jurusan');

			$var = @$_SESSION['sesi_kd_prodi'];
			if(empty($var)){
				$kd_prodi = $this->input->post('cari_jurusan');
			}else{
				$kd_prodi = $var;
			}
			// if($kd_prodi==null){
			// 	echo "kosong";
			// }
			// echo $kd_prodi;
			$jurusan = $this->model_data->singkat_jurusan($kd_prodi);
         $id_prodi 	= $this->model_data->id_prodi($kd_prodi);

			if(!empty($jurusan)){
				$_SESSION['sesi_kd_prodi'] 		= $kd_prodi;
				$_SESSION['sesi_singkat_prodi'] = $jurusan;
				$_SESSION['sesi_id_prodi'] 		= $id_prodi;
				// $this->session->set_userdata($sess_data);

			}
			$jur = @$_SESSION['sesi_kd_prodi'];


			$d['data'] = $data = $this->model_data->data_mk($jur);
			$d['content'] = 'mata_kuliah/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function create_kdmk()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$kd_prodi = @$_SESSION['sesi_kd_prodi'];//$this->input->post('prodi'];
			$singkat_prodi = @$_SESSION['sesi_singkat_prodi'];
         	$id_prodi = @$_SESSION['sesi_id_prodi'];

			$q = $this->db->query("SELECT MAX(right(kd_mk,4)) as kode FROM mata_kuliah WHERE kd_prodi='$kd_prodi'");
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$no_akhir = (int) $dt->kode+1;
					$d['kode'] = $singkat_prodi.'-'.sprintf("%04s", $no_akhir);
				}
				echo json_encode($d);
			}else{
				$d['kode'] = $singkat_prodi.'-0001';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['kd_mk']	= $this->input->post('cari');

			$q = $this->db->get_where("mata_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['jurusan'] = $dt->kd_prodi;
					$d['semester'] = $dt->smt;
					$d['kode'] = $dt->kd_mk;
					$d['nama_mk'] = $dt->nama_mk;
					$d['sks'] = $dt->sks;
					$d['tayang'] = $dt->aktif;
				}
				echo json_encode($d);
			}else{
				$d['jurusan'] = '';
				$d['semester'] = '';
				$d['nama_mk'] = '';
				$d['sks'] = '';
				$d['tayang'] = '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$smt = $this->input->post('semester');
			if($smt % 2 ==0){
				$ket_smt = "Genap";
			}else{
				$ket_smt = "Ganjil";
			}

			$id['kd_mk'] = $this->input->post('kode');

			$dt['kd_mk'] = $this->input->post('kode');
			$dt['kd_prodi'] = $this->input->post('jurusan');
			$dt['smt'] = $this->input->post('semester');
			$dt['nama_mk'] = $this->input->post('nama_mk');
			$dt['sks'] = $this->input->post('sks');
			$dt['semester'] = $ket_smt;
			$dt['aktif'] = $this->input->post('tayang');

			$id_prodi = $this->input->post('id_prodi');

			$q = $this->db->get_where("mata_kuliah",$id);
			$row = $q->num_rows();

			if($row>0){

				$record['kode_mata_kuliah'] 			= $dt['kd_mk'];
				$record['nama_mata_kuliah'] 			= $dt['nama_mk'];
				$record['sks_mata_kuliah'] 			= $dt['sks'];
				$record['sks_tatap_muka']				= $dt['sks'];

				$dt['tgl_update'] = date('Y-m-d h:i:s');
				$this->db->update("mata_kuliah",$dt,$id);
				echo "Data Sukses diUpdate";
			}else{

				$record['id_prodi'] 						= $id_prodi;
				$record['kode_mata_kuliah'] 			= $dt['kd_mk'];
				$record['nama_mata_kuliah'] 			= $dt['nama_mk'];
				$record['id_jenis_mata_kuliah'] 		= 'A';
				$record['sks_mata_kuliah'] 			= $dt['sks'];
				$record['sks_tatap_muka']				= $dt['sks'];

				$dt['tgl_insert'] = date('Y-m-d h:i:s');
				// echo "<pre>";
				// print_r($dt);
				// echo "</pre>";
				// exit;
				$this->db->insert("mata_kuliah",$dt);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['kd_mk']	= $this->uri->segment(3);

			$q = $this->db->get_where("mata_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("mata_kuliah",$id);
			}
			redirect('mata_kuliah/view_data','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
