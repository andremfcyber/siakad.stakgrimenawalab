<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lappiutang extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
   * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Laporan Piutang Mahasiswa";
			$d['class'] = "lapkeuangan";

			$d['content']= 'lappiutang/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

  public function list_smt()
  {
    $th_akademik = $this->input->post('th_akademik');

    $this->db->select('smt');
    $this->db->where('th_akademik',$th_akademik);
    $this->db->group_by('smt');
    $this->db->order_by('smt','ASC');
    $data = $this->db->get('jenis_tagihan');
    echo "<option value=''>-</option>";
    foreach($data->result() as $row)
    {
      echo "<option value='$row->smt'>$row->smt</option>";
    }
  }

  public function list_tagihan()
  {
    $th_akademik = $this->input->post('th_akademik');
    $kd_prodi = $this->input->post('kd_prodi');

    // $this->db->select('id,nama');
    $this->db->where('th_akademik',$th_akademik);
    $this->db->where('kd_prodi',$kd_prodi);
    // $this->db->group_by('smt');
    // $this->db->order_by('smt','ASC');
    $data = $this->db->get('jenis_tagihan');
    echo "<option value=''>-</option>";
    foreach($data->result() as $row)
    {
      echo "<option value='$row->id'>".$row->id.' | '.$row->nama." semester ".$row->smt." | Rp. ".number_format($row->jumlah)."</option>";
    }
  }

  public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

      $th_akademik = $this->input->post('th_akademik');
      $id_tagihan = $this->input->post('id_tagihan');


      // $kd_prodi = $this->input->post('kd_prodi');
      // $tanggal = $this->input->post('tanggal');
      $kelas = $this->input->post('kelas');
      // $semester = $this->input->post('semester');

      $sql = "select a.nim,a.nama_mhs,a.kd_prodi,
              b.smt,b.nama,
              (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as bayar
              from mahasiswa as a
              join jenis_tagihan as b
              on a.th_akademik=b.th_akademik and a.kd_prodi=b.kd_prodi
              where b.id=11";


      $query = "a.nim,a.nama_mhs,a.kd_prodi,
                b.id,b.smt,b.nama,b.jumlah,
                (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as jumlah_bayar,
                b.jumlah - (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as sisa_bayar";
      // echo $query;die;
      $this->db->select($query);
      // $this->db->select('a.*,b*,(select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as bayar');
			$this->db->where('a.status','Aktif');
			$this->db->where('a.th_akademik',$th_akademik);
      $this->db->where('b.th_akademik',$th_akademik);
      $this->db->where('b.id',$id_tagihan);
      // $this->db->where('b.jumlah - (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id)',1);

      // if(!empty($kd_prodi)){
      //   $this->db->where('b.kd_prodi',$kd_prodi);
      // }
      //
      // if(!empty($tanggal)){
      //   $tgl = $this->model_global->tgl_sql($tanggal);
      //   $this->db->where('a.tanggal',$tgl);
      // }
      //
      if(!empty($kelas)){
        $this->db->where('a.kelas',$kelas);
      }
      //
      // if(!empty($semester)){
      //   $this->db->where('a.smt',$semester);
      // }

      $this->db->order_by('a.nim');
      $this->db->from('mahasiswa as a');
      $this->db->join('jenis_tagihan as b','a.th_akademik=b.th_akademik AND a.kd_prodi=b.kd_prodi');
      // $data = $this->db->get();
      // $this->debug($data->result());die;
      $d['data'] = $this->db->get();
//      echo $this->db->last_query();//die;

      // echo $this->db->last_query();die;

			$this->load->view('lappiutang/view',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cetak()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

      $th_akademik = $this->input->post('th_akademik');
      $id_tagihan = $this->input->post('id_tagihan');

      $kelas = $this->input->post('kelas');

      $query = "a.nim,a.nama_mhs,a.kd_prodi,
                b.id,b.smt,b.nama,b.jumlah,
                (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as jumlah_bayar,
                b.jumlah - (select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as sisa_bayar";

      $this->db->select($query);
      // $this->db->select('a.*,b*,(select sum(jumlah) from bayar_mhs where nim=a.nim and jenis_tagihan_id=b.id) as bayar');
      $this->db->where('a.th_akademik',$th_akademik);
      $this->db->where('b.th_akademik',$th_akademik);
      $this->db->where('b.id',$id_tagihan);

      if(!empty($kelas)){
        $this->db->where('a.kelas',$kelas);
      }

      $this->db->order_by('a.nim');
      $this->db->from('mahasiswa as a');
      $this->db->join('jenis_tagihan as b','a.th_akademik=b.th_akademik AND a.kd_prodi=b.kd_prodi');

      $d['data'] = $this->db->get();


			header("Content-type: application/vnd-ms-excel");
			// header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=Laporan_Piutang_".$th_akademik.".xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			$this->load->view('lappiutang/view',$d);

		}else{
			redirect('login','refresh');
		}
	}

}
