<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$data = $this->db->query('SELECT th_akademik,semester,kd_prodi FROM jadwal GROUP BY th_akademik,semester,kd_prodi');


			// $this->db->group_by('th_akademik','semester','kd_prodi');
			// $data = $this->db->get('jadwal');
			// print_r($data);die;
			// if($data->result){
			// 	$data = $data;
			// }else{
			// 	$data = 0;
			// }

			$d = array('judul' => 'Jadwal Kuliah Semester',
					'class'=> 'transaksi',
					'data' => $data,
					'content'=>'jadwal/view_data'
					);

			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function tambah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			/*
			$d['judul']="Jadwal Kuliah Semester";
			$d['class'] = "transaksi";
			$d['content'] = 'jadwal/form';
			*/
			//$th_now = date('Y');
			//$th_next = date('Y')+1;
			$th_akademik = $this->model_global->getThAkademikAktif()['kode'];  //$th_now.'/'.$th_next;
			$semester = $this->model_global->getThAkademikAktif()['semester'];

			$d = array('judul' => 'Jadwal Kuliah Semester',
					'class'=> 'transaksi',
					'content'=>'jadwal/form',
					'th_akademik'=>$th_akademik,
					'semester'=>$semester,
					'kd_prodi'=>''
					);

			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function edit()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$e = $this->uri->segment(3);
			$edt=explode('_',$e,3);
			//echo $edt[2];exit;
			$this->db->where('th_akademik',$edt[0]);
			$this->db->where('semester',$edt[1]);
			$this->db->where('kd_prodi',$edt[2]);
			$data = $this->db->get('jadwal');
			//print_r($this->db->last_query());exit;
			if($data->num_rows()>0){
				$row = $data->row();
				$th_akademik = $row->th_akademik;
				$semester = $row->semester;

				$d = array('judul' => 'Jadwal Kuliah Semester',
						'class'=> 'transaksi',
						'content'=>'jadwal/form',
						'th_akademik'=>$th_akademik,
						'semester'=>$semester,
						'kd_prodi'=>$row->kd_prodi
						);

				$this->load->view('home',$d);
			}else{
				redirect('jadwal');
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_jadwal_id()
	{
		$id = $this->input->post('id');
		// echo $id;die;
		$this->db->where('id_jadwal',$id);
		$data = $this->db->get('jadwal');
		if($data->num_rows()>0)
		{
			$row = $data->row();
			$dt = array(
				'kd_mk' => $row->kd_mk,
				'kd_dosen' => $row->kd_dosen,
				'hari' => $row->hari,
				// 'jam_mulai' => substr($row->jam_mulai,0,5),
				// 'jam_selesai' => substr($row->jam_selesai,0,5),
				'ruang' => $row->ruang,
				'kelas' => $row->kelas
			);
		}else{
			$dt = array(
				'kd_mk' => '',
				'kd_dosen' => '',
				'hari' => '',
				// 'jam_mulai' => '',
				// 'jam_selesai' => '',
				'ruang' => '',
				'kelas' => ''
			);
		}
		echo json_encode($dt);
	}
	public function data_MK_prodi(){
		$id['Fak'] = $this->input->post('id');
		$id['Smt'] = $this->input->post('smt');

		$q = $this->db->get_where("msmatakuliah",$id);
		foreach($q->result() as $dt){
			echo "<option value='".$dt->KdMK."'>".$dt->NamaMK."</option>";
		}
	}


	public function view_data()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Dosen";
			$d['class'] = "master";

			$jurusan = $this->input->post('cari_jurusan');
			if(!empty($jurusan)){
				$_SESSION['sesi_jurusan'] = $jurusan;
				
			}
			$jur = @$_SESSION['sesi_jurusan'];


			$d['data'] = $data = $this->model_data->data_dosen($jur);
			$d['content'] = 'dosen/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function mata_kuliah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('smt');

			$q = $this->db->get_where("mata_kuliah",$id);
			$row = $q->num_rows();
			if($row>0){
				echo "<option value=''>-Pilih-</option>";
				foreach($q->result() as $dt){
				?>
                	<option value="<?php echo $dt->kd_mk;?>"><?php echo $dt->nama_mk.' - '.$dt->smt;?></option>
                <?php
				}
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function list_dosen()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$kd_prodi	= $this->input->post('kd_prodi');

			$q = $this->model_data->data_dosen($kd_prodi);
			$row = $q->num_rows();
			if($row>0){
				echo "<option value=''>-Pilih-</option>";
				foreach($q->result() as $dt){
				?>
                	<option value="<?php echo $dt->kd_dosen;?>"><?php echo $dt->kd_dosen.' - '.$dt->nama_dosen.' | '.$dt->prodi;?></option>
                <?php
				}
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_jadwal()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('smt');

			$d['data'] = $this->db->get_where("jadwal",$id);
			echo $this->load->view('jadwal/view',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			date_default_timezone_set('Asia/Jakarta');

			// $jam_mulai = $this->input->post('jam_mulai');
			// $jam_selesai = $this->input->post('jam_selesai');
			// $pukul = $jam_mulai.' s/d '.$jam_selesai;

			$kd_mk = $this->input->post('mk');
			$infoMK = $this->model_data->getInfoMK($kd_mk);
			$smt_mk = $infoMK['smt'];
			$id_1 = (int) $this->input->post('id');
			// echo $id;die;

			$this->db->where('id_jadwal',$id_1);
			$q = $this->db->get('jadwal');
			// print_r($q);die;
			if($q->num_rows()>0){
				$data['th_akademik'] = $this->input->post('th_akademik');
				$data['semester'] = $this->input->post('smt');
				$data['kd_prodi'] = $this->input->post('kd_prodi');
				$data['hari'] = $this->input->post('hari');
				$data['kelas'] = $this->input->post('kelas');
				// $data['pukul'] = $pukul;//$this->input->post('pukul');
				$data['ruang'] = $this->input->post('ruang');
				// $data['jam_mulai'] = $this->input->post('jam_mulai');
				// $data['jam_selesai'] = $this->input->post('jam_selesai');
				$data['kd_mk'] = $this->input->post('mk');
				$data['smt'] = $smt_mk;
				$data['kd_dosen'] = $this->input->post('kd_dosen');
				$data['user_id']= @$_SESSION['username'];
				$data['tgl_update'] = date('Y-m-d h:i:s');

				$this->db->where('id_jadwal',$id_1);
				$this->db->update("jadwal",$data);
				echo "Data berhasil di Edit";
				die;
			}


			// $id['th_akademik'] = $this->input->post('th_akademik');
			// $id['semester'] = $this->input->post('smt');
			// $id['hari'] = $this->input->post('hari');
			// $id['pukul'] = $pukul;//$this->input->post('pukul');
			// $id['ruang'] = $this->input->post('ruang');

			// $q = $this->db->get_where('jadwal',$id);
			// $r = $q->num_rows();
			// if($r>0){
			// 	echo "Maaf, Hari - Pukul - Ruang sudah di Gunakan";
			// }else{
			// 	$id['kd_mk'] = $this->input->post('mk');
			// 	$q = $this->db->get_where('jadwal',$id);
			// 	$r = $q->num_rows();
			// 	if($r>0){
			// 		echo "Maaf, Hari - Pukul - Ruang - Mata Kuliah sudah di Gunakan";
			// 	}else{
			// 		$id_2['th_akademik'] = $this->input->post('th_akademik');
			// 		$id_2['semester'] = $this->input->post('smt');
			// 		$id_2['hari'] = $this->input->post('hari');
			// 		$id_2['pukul'] = $pukul;//$this->input->post('pukul');
			// 		$id_2['kd_dosen'] = $this->input->post('kd_dosen');
			// 		$q = $this->db->get_where('jadwal',$id_2);
			// 		$r = $q->num_rows();
			// 		if($r>0){
			// 			echo "Maaf, Hari - Pukul - Ruang - Dosen sudah di Gunakan";
			// 		}else{
			// 			//echo "Simpan";
			// 			$data['th_akademik'] = $this->input->post('th_akademik');
			// 			$data['semester'] = $this->input->post('smt');
			// 			$data['kd_prodi'] = $this->input->post('kd_prodi');
			// 			$data['hari'] = $this->input->post('hari');
			// 			$data['kelas'] = $this->input->post('kelas');
			// 			$data['pukul'] = $pukul;//$this->input->post('pukul');
			// 			$data['ruang'] = $this->input->post('ruang');
			// 			$data['jam_mulai'] = $this->input->post('jam_mulai');
			// 			$data['jam_selesai'] = $this->input->post('jam_selesai');
			// 			$data['kd_mk'] = $kd_mk;
			// 			$data['smt'] = $smt_mk;
			// 			$data['kd_dosen'] = $this->input->post('kd_dosen');
			// 			$data['user_id']= @$_SESSION['username'];
			// 			$data['tgl_insert'] = date('Y-m-d h:i:s');

			// 			// echo "<pre>";
			// 			// print_r($data);
			// 			// echo "</pre>";
			// 			// exit;

			// 			$this->db->insert("jadwal",$data);

			// 			echo "Data Sukses disimpan";
			// 		}
			// 	}
			// }

			// $id['th_akademik'] = $this->input->post('th_akademik');
			// $id['semester'] = $this->input->post('smt');
			// $id['hari'] = $this->input->post('hari');
			// $id['ruang'] = $this->input->post('ruang');
			// $id['kd_mk'] = $this->input->post('mk');
			// $id['kd_dosen'] = $this->input->post('kd_dosen');
			//echo "Simpan";
			$data['th_akademik'] = $this->input->post('th_akademik');
			$data['semester'] = $this->input->post('smt');
			$data['kd_prodi'] = $this->input->post('kd_prodi');
			$data['hari'] = $this->input->post('hari');
			$data['kelas'] = $this->input->post('kelas');
			// $data['pukul'] = $pukul;//$this->input->post('pukul');
			$data['ruang'] = $this->input->post('ruang');
			// $data['jam_mulai'] = $this->input->post('jam_mulai');
			// $data['jam_selesai'] = $this->input->post('jam_selesai');
			$data['kd_mk'] = $kd_mk;
			$data['smt'] = $smt_mk;
			$data['kd_dosen'] = $this->input->post('kd_dosen');
			$data['user_id']= @$_SESSION['username'];
			$data['tgl_insert'] = date('Y-m-d h:i:s');

			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit;

			$this->db->insert("jadwal",$data);

			echo "Data Sukses disimpan";

		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id_jadwal']	= $this->input->post('id');

			$q = $this->db->get_where("jadwal",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("jadwal",$id);
				echo 'Data Sukses dihapus';
			}
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
