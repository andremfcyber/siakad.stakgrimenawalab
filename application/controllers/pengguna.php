<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengguna extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Pengguna";
			$d['class'] = "master";
			$this->db->select('*');
			$this->db->from('admins');
			$this->db->join('prodi', 'prodi.kd_prodi = admins.kd_prodi','left');
			$d['data'] = $this->db->get();
      // $d['data'] = $this->db->get('admins');
			$d['content'] = 'pengguna/view';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}


	public function cari()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id_username']	= $this->input->post('cari');

			$q = $this->db->get_where("admins",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['username'] = $dt->username;
					$d['nama_lengkap'] = $dt->nama_lengkap;
					$d['level'] = $dt->level;
					$d['kd_prodi'] = $dt->kd_prodi;
					$d['blokir'] = $dt->blokir;
				}
				echo json_encode($d);
			}else{
				$d['username'] 		= '';
				$d['nama_lengkap'] 	= '';
				$d['level'] 	= '';
				$d['blokir'] 	= '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['username'] = $this->input->post('username');
			$password = $this->input->post('password');

			$dt['username'] = $this->input->post('username');
			$dt['level'] = $this->input->post('level');
			$dt['kd_prodi'] = $this->input->post('kd_prodi');
			$dt['nama_lengkap'] = $this->input->post('nama_lengkap');
			if(!empty($password)){
					$dt['password'] = md5($password);
			}

			$dt['blokir'] = $this->input->post('blokir');

			$q = $this->db->get_where("admins",$id);
			$row = $q->num_rows();
			if($row>0){
        $dt['update_date'] = date('Y-m-d H:i:s');
				$this->db->update("admins",$dt,$id);
				$_SESSION['adm_kd_prodi'] = $dt['kd_prodi'];
				echo "Data Sukses diUpdate";
			}else{
        $dt['insert_date'] = date('Y-m-d H:i:s');
				$this->db->insert("admins",$dt);
				echo "Data Sukses diSimpan";
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id_username']	= $this->uri->segment(3);

			$q = $this->db->get_where("admins",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("admins",$id);
			}
			redirect('pengguna','refresh');
		}else{
			redirect('login','refresh');
		}

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
