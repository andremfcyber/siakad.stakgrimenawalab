<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Poll_transaksi extends CI_Controller {

    /**
     * Index Page for this controller.
     * Programmer : Deddy Rusdiansyah.S.Kom
     * http://deddyrusdiansyah.blogspot.com
     * http://softwarebanten.com
     * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
     * Developer : Fitria Wahyuni.S.Pd
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('model_polling');
    }

    public function index() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $d['judul'] = "Kuesioner";
            $d['class'] = "polling";
            $d['content'] = 'poll_transaksi/view';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function get_json() {
        $this->db->select('a.*,b.nama as nama_jenis');
        $this->db->from('poll_transaksi as a');
        $this->db->join('poll_jenis as b', 'b.id=a.poll_jenis_id', 'left');
        $this->db->order_by('a.id', 'DESC');
        $results = $this->db->get()->result_array();
        $data = array();
        $no = 1;
        foreach ($results as $r) {
            array_push($data, array(
                $no++,
                $r['nama_jenis'],
                $r['th_akademik'],
                $r['poll_pertanyaan_id'],
                anchor('poll_transaksi/edit/' . $r['id'], 'Edit', array('class' => 'btn btn-mini btn-primary')) . '  ' .
                anchor('poll_transaksi/hapus/' . $r['id'], 'Delete', array("onclick" => "return confirm('yakin akan menghapus ?')", 'class' => 'btn btn-mini btn-danger'))
            ));
        }

        echo json_encode(array('data' => $data));
    }

    public function detail_pertanyaan() {
        $th_akademik = $this->input->post('th_akademik');
        $poll_jenis_id = (int) $this->input->post('poll_jenis_id');

        $this->db->where('poll_jenis_id', $poll_jenis_id);
        $dt['get'] = $this->db->get('poll_pertanyaan');
        $dt['th_akademik'] = $th_akademik;
        $dt['poll_jenis_id'] = $poll_jenis_id;

        $this->load->view('poll_transaksi/detail_pertanyaan', $dt);
    }

    public function tambah() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $d['judul'] = "Tambah Pertanyaan Polling";
            $d['class'] = "polling";
            $d['id'] = '';
            $d['poll_jenis_id'] = '';
            $d['poll_pertanyaan_id'] = '';
            $d['th_akademik'] = '';

            $d['content'] = 'poll_transaksi/add';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function edit() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id = $this->uri->segment(3);

            $this->db->where('id', $id);
            $get = $this->db->get('poll_transaksi');
            if ($get->num_rows() > 0) {
                $row = $get->row();
                $th_akademik = $row->th_akademik;
                $poll_jenis_id = $row->poll_jenis_id;
                $poll_pertanyaan_id = $row->poll_pertanyaan_id;
            } else {
                $poll_jenis_id = '';
                $poll_pertanyaan_id = '';
                $th_akademik = '';
            }
            $d['judul'] = "Edit Pertanyaan Polling";
            $d['class'] = "polling";
            $d['id'] = $id;
            $d['poll_jenis_id'] = $poll_jenis_id;
            $d['poll_pertanyaan_id'] = $poll_pertanyaan_id;
            $d['th_akademik'] = $th_akademik;

            $d['content'] = 'poll_transaksi/add';
            $this->load->view('home', $d);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function simpan() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {

            // print_r($_POST);die;
            $id['id'] = (int) $this->input->post('id');

            // $id['th_akademik'] = $this->input->post('th_akademik');
            // $id['poll_jenis_id'] = $this->input->post('poll_jenis_id');

            $imp_cek = $this->input->post('cek');
            $cek = implode(',', $imp_cek);

            $dt['th_akademik'] = $this->input->post('th_akademik');
            $dt['poll_jenis_id'] = $this->input->post('poll_jenis_id');

            $dt['poll_pertanyaan_id'] = $cek;

            $dt['user_id'] = @$_SESSION['username'];


            $q = $this->db->get_where("poll_transaksi", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $dt['update_date'] = date('Y-m-d H:i:s');
                $this->db->update("poll_transaksi", $dt, $id);
                $this->session->set_flashdata('info', 'Update data berhasil');
            } else {
                $dt['insert_date'] = date('Y-m-d H:i:s');
                $this->db->insert("poll_transaksi", $dt);
                $this->session->set_flashdata('info', 'Insert data berhasil');
            }
            redirect('poll_transaksi');
        } else {
            redirect('login', 'refresh');
        }
    }

    public function hapus() {
        $cek = @$_SESSION['logged_in'];
        $level = @$_SESSION['level'];
        if (!empty($cek) && $level == 'admin') {
            $id['id'] = $this->uri->segment(3);

            $q = $this->db->get_where("poll_transaksi", $id);
            $row = $q->num_rows();
            if ($row > 0) {
                $this->db->delete("poll_transaksi", $id);
                $this->session->set_flashdata('info', 'Delete data berhasil');
            }
            redirect('poll_transaksi', 'refresh');
        } else {
            redirect('login', 'refresh');
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
