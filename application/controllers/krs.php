<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Krs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */

	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Kartu Rencana Studi";
			$d['class'] = "transaksi";

			$this->db->select('a.th_akademik,a.semester,b.kd_prodi');
			$this->db->from('krs as a');
			$this->db->join('jadwal as b','a.id_jadwal=b.id_jadwal');
			$this->db->group_by('a.th_akademik,a.semester,b.kd_prodi');
			$d['data'] = $this->db->get();

			// $d['data'] = $this->db->query("SELECT a.id_krs,a.id_jadwal,a.th_akademik,a.semester,b.kd_prodi
			// 							FROM krs as a
			// 							JOIN jadwal as b
			// 							ON a.id_jadwal = b.id_jadwal
			// 							 GROUP BY a.th_akademik,a.semester,b.kd_prodi");
			$d['content'] = 'krs/view_data';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function view_detail()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$e = $this->uri->segment(3);
			$edt=explode('_',$e,3);
			//print_r($edt);exit;
			$th_akademik = $edt[0];
			$semester = $edt[1];
			$kd_prodi = $edt[2];
			$data_krs = $this->model_data->data_krs($th_akademik,$semester,$kd_prodi);
			
			// $nim = $q['nim'];
			// print_r($d['nim']);exit;
			if($data_krs->num_rows>0){
				// foreach($q->result() as $dt){
				// 	$th_ak = $dt->th_akademik;
				// 	$kd_prodi = $dt->kd_prodi;
					$prodi = $this->model_data->nama_jurusan($kd_prodi);
				// 	$key = $dt->id_jadwal;
				// }
				$d['judul']="Detail Pengambilan KRS Tahun Akademik ".$th_akademik." Program Studi ".$prodi;
				$d['class'] = "transaksi";
				/*
				$this->db->form('krs');
				$this->db->join('mahasiswa','krs.nim=mahasiswa.nim');
				$this->db->where('id_krs',$key);
				$query = $this->db->get();
				$d['data'] = $query;
				*/

				$d['data'] = $this->db->query("SELECT a.th_akademik,a.semester,b.kd_prodi,a.nim, c.nama_mhs, c.kd_prodi, c.status, d.prodi
					FROM krs as a
					JOIN jadwal as b ON a.id_jadwal = b.id_jadwal
					JOIN mahasiswa as c ON a.nim = c.nim
					JOIN prodi as d ON c.kd_prodi = d.kd_prodi
					WHERE a.th_akademik='$th_akademik' AND a.semester='$semester' AND b.kd_prodi='$kd_prodi'
					GROUP BY a.th_akademik,a.nim,a.semester,b.kd_prodi,c.nama_mhs,c.kd_prodi, c.status, d.prodi");

			// print_r($d['data']->result());exit;


				$d['content'] = 'krs/view_data_detail';
				$this->load->view('home',$d);
			}else{
				redirect('krs','refresh');
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function tambah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$d['judul']="Kartu Rencana Studi";
			$d['class'] = "transaksi";

			$d['content'] = 'krs/form';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}



	public function cari_nim()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$th_ak = $this->input->post('th_ak');
			$id['nim']	= $this->input->post('nim');

			$q = $this->db->get_where("mahasiswa",$id);
			$row = $q->num_rows();
			if($row>0){
				foreach($q->result() as $dt){
					$d['nama'] = $dt->nama_mhs;
					$d['kd_prodi'] = $dt->kd_prodi;
					$d['nm_prodi'] = $this->model_data->nama_jurusan($dt->kd_prodi);
					$d['smt'] = $this->model_global->semester($dt->nim,$th_ak);
					$d['th_angkatan'] = $dt->th_akademik;
					$d['kelas'] = $dt->kelas;
					$d['sex'] = $dt->sex=='L'?'Laki-laki':'Perempuan';
				}
				echo json_encode($d);
			}else{
				$d['nama'] = '';
				$d['kd_prodi'] = '';
				$d['nm_prodi'] = '';
				$d['smt'] = '';
				$d['th_angkatan'] = '';
				$d['kelas'] = '';
				$d['sex'] = '';
				echo json_encode($d);
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_mata_kuliah_old()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('semester');
			$id['smt']	= $this->input->post('smt');

			$q['data'] = $this->db->get_where("jadwal",$id);

			$this->load->view('krs/mk_paket',$q);

		}else{
			redirect('login','refresh');
		}
	}

	public function cari_mata_kuliah()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('semester');
			$id['smt']	= $this->input->post('smt');
			$id['kelas']	= $this->input->post('kelas');


			$q = $this->db->get_where("jadwal",$id);
			$row = $q->num_rows();
			if($row>0){
				echo "<option value=''>-Pilih Mata Kuliah-</option>";
				foreach($q->result() as $dt){
					$infoMK = $this->model_data->getInfoMK($dt->kd_mk);
					$nama_mk = $infoMK['nama_mk']; // $this->model_data->cari_nama_mk($dt->kd_mk);
					$sks_mk = $infoMK['sks'];
					$smt_mk = $infoMK['smt'];
					$nama_dosen = $this->model_data->cari_nama_dosen($dt->kd_dosen);
				?>
                	<option value="<?php echo $dt->id_jadwal;?>"><?php echo $dt->kd_mk;?> | <?php echo $nama_mk.' | smt '.$smt_mk;?> | <?php echo $dt->kd_dosen;?> | <?php echo $nama_dosen;?> | <?php echo $dt->hari.' | '.$dt->pukul.' | '.$dt->ruang.' | Kelas '.$dt->kelas;?></option>
                <?php
				}
			}else{
				echo "<option value=''>Belum Ada Jadwal ..!!!</option>";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_smt()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['kd_prodi']	= $this->input->post('kd_prodi');
			$id['semester']	= $this->input->post('semester');
			// $id['smt']	= $this->input->post('smt');

			$this->db->select('smt');
			$this->db->group_by('smt');
			$q = $this->db->get_where("jadwal",$id);
			$row = $q->num_rows();
			if($row>0){
				echo "<option value=''>-Pilih-</option>";
				foreach($q->result() as $dt){
					echo "<option value='$dt->smt'>$dt->smt</option>";
				}
			}else{
				echo "<option value=''>-</option>";
			}
		}else{
			redirect('login','refresh');
		}
	}

	// cari_bayar
	public function cari_bayar()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$th_akademik	= $this->input->post('th_akademik');
			$th_angkatan	= $this->input->post('th_angkatan');
			$smt	= $this->input->post('smt');
			$nim	= $this->input->post('nim');
			$kd_prodi	= $this->input->post('kd_prodi');

			// CARI DISPENSASI
			$this->db->where('th_akademik_kode',$th_akademik);
			$this->db->where('nim',$nim);
			$dispensasi = $this->db->get('dispensasi');
			if($dispensasi->num_rows()>0)
			{
				$hasil = array('info' => 'VALID');
			}else{
					// CARI PEMBAYARAN
					$this->db->select('SUM(jumlah) as jml_bayar');
					$this->db->where('th_akademik_kode',$th_akademik);
					$this->db->where('smt',$smt);
					$this->db->where('nim',$nim);
					$bayar = $this->db->get('bayar_mhs');
					if($bayar->num_rows()>0)
					{
						$row = $bayar->row();
						$jml_bayar = $row->jml_bayar;

						$this->db->where('th_akademik',$th_angkatan);
						$this->db->where('smt',$smt);
						$this->db->where('kd_prodi',$kd_prodi);
						$tagihan = $this->db->get('jenis_tagihan');
						if($tagihan->num_rows()>0){
							$row = $tagihan->row();
							$jml_tagihan = $row->jumlah;

							if($jml_bayar>=$jml_tagihan){
									$hasil = array('info' => 'VALID');
							}else{
									$hasil = array('info' => 'INVALID');
							}
						}else{
								$hasil = array('info' => 'INVALID');
						}

					}else{
						$hasil = array('info' => 'INVALID');
					}
			}
			echo json_encode($hasil);
		}else{
			redirect('login','refresh');
		}
	}

	public function simpan()
	{

		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$id_jadwal = $this->input->post('id_jadwal'); //'4';//

			$infoJadwal = $this->model_data->getInfoJadwal($id_jadwal);
			$kd_mk = $infoJadwal['kd_mk'];

			$th_ak = $this->input->post('th_ak'); //'2014/2015';//
			$smt = $this->input->post('semester'); //'ganjil';//
			$nim = $this->input->post('nim'); //'TI20140001';//

			$this->db->where('th_akademik',$th_ak);
			$this->db->where('nim',$nim);
			$this->db->where('kd_mk',$kd_mk);
			$data = $this->db->get('krs');
			if($data->num_rows()>0){
				echo "Anda sudah memilih mata kuliah yang sama";
				die;
			}


			/** Validasi JUMLAH SKS **/
			$jml_sks = $this->model_data->cari_jml_sks_krs($th_ak,$smt,$nim);
			$sks = $this->model_data->cari_sks_jadwal($id_jadwal);
			$t_sks = $jml_sks+$sks;
			$max_sks = $this->input->post('max_sks');
			if($t_sks>$max_sks){
				echo "Anda tidak boleh melebihi ".$max_sks." SKS";
			}else{

				$id['th_akademik'] = $this->input->post('th_ak');
				$id['semester'] = $this->input->post('semester');
				$id['nim'] = $this->input->post('nim');
				$id['smt'] = $this->input->post('smt');
				$id['id_jadwal'] = $id_jadwal;

				$dt['th_akademik'] = $this->input->post('th_ak');
				$dt['semester'] = $this->input->post('semester');
				$dt['nim'] = $this->input->post('nim');
				$dt['smt'] = $this->input->post('smt');
				$dt['id_jadwal'] = $id_jadwal;
				$dt['kd_prodi'] = $this->model_data->cari_kd_prodi_mhs($nim);
				//cari kd_mk
				$this->db->where('id_jadwal',$id_jadwal);
				$q = $this->db->get('jadwal'); // $this->db->query("SELECT * FROM jadwal WHERE id_jadwal='$id_jadwal'");
				foreach($q->result() as $dt_j){
					$kd_mk = $dt_j->kd_mk;
					$kd_dosen = $dt_j->kd_dosen;

					$dt['kd_mk'] = $kd_mk;
					$dt['kd_dosen'] = $kd_dosen;
					$dt['ruang'] = $dt_j->ruang;
					$dt['hari'] = $dt_j->hari;
					$dt['pukul'] = $dt_j->pukul;
					$dt['kelas'] = $dt_j->kelas;
				}
				//cari nama_mk
				$q_mk = $this->db->query("SELECT * FROM mata_kuliah WHERE kd_mk='$kd_mk'");
				foreach($q_mk->result() as $dt_mk){
					$dt['nama_mk'] = $dt_mk->nama_mk;
					$dt['sks'] = $dt_mk->sks;
				}
				//cari nama dosen
				$dt['nm_dosen'] = $this->model_data->cari_nama_dosen($kd_dosen);

				$q_krs = $this->db->get_where("krs",$id);
				$row = $q_krs->num_rows();



//				$record['id_registrasi_mahasiswa'] 		= '0';
//				$record['kode_mata_kuliah_asal'] 		= '0';
//				$record['nama_mata_kuliah_asal'] 		= '0';
//				$record['sks_mata_kuliah_asal'] 		= '0';
//				$record['nilai_huruf_asal'] 		= '0';
//				$record['sks_mata_kuliah_diakui'] 		= '0';
//				$record['nilai_huruf_diakui'] 		= '0';
//				$record['nilai_angka_diakui'] 		= '0';
 /*
{
"act":"InsertNilaiTransferPendidikanMahasiswa",
"token":"f518beda9f7cfe2c0e937da5e3f30ac4",
"record":{
	"id_registrasi_mahasiswa":"500c71f1-d677-43eb-8812-5fbab09fc5c5",
	"kode_mata_kuliah_asal":"MKS1MANAJ01",
	"nama_mata_kuliah_asal":"MANAJEMEN KEUANGAN",
	"sks_mata_kuliah_asal":"8",
	"nilai_huruf_asal":"A",
	"sks_mata_kuliah_diakui":"8",
	"nilai_huruf_diakui":"A",
	"nilai_angka_diakui":"4"
	}
}
 */

				if($row>0){
					$dt['tgl_update'] = date('Y-m-d h:i:s');
					$this->db->update("krs",$dt,$id);
					echo "Data Sukses diUpdate";
				}else{
					$dt['tgl_insert'] = date('Y-m-d h:i:s');
					$this->db->insert("krs",$dt);
					echo "Data Sukses diSimpan";
				}
			}
		}else{
			redirect('login','refresh');
		}

	}

	public function hapus()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['id_krs']	= $this->input->post('id');

			$q = $this->db->get_where("krs",$id);
			$row = $q->num_rows();
			if($row>0){
				$this->db->delete("krs",$id);
				echo "Data sukses dihapus";
			}
			//redirect('krs','refresh');
		}else{
			redirect('login','refresh');
		}

	}

	public function cari_krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){
			$id['th_akademik']	= $this->input->post('th_ak');
			$id['nim']	= $this->input->post('nim');
			$id['smt']	= $this->input->post('smt');
			$id['semester']	= $this->input->post('semester');

			//$this->db->order('hari','pukul');
			$d['data'] = $this->db->get_where("krs",$id);
			echo $this->load->view('krs/view',$d);
		}else{
			redirect('login','refresh');
		}
	}


	public function cetak_krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = $this->input->post('th_ak');
			$smt = $this->input->post('semester');
			$nim = $this->input->post('nim');

			// $q = $this->db->query("SELECT * FROM krs WHERE th_akademik='$th_ak' AND semester='$smt' AND nim='$nim' ");
			$this->db->where('th_akademik',$th_ak);
			$this->db->where('semester',$smt);
			$this->db->where('nim',$nim);
			$q = $this->db->get('krs');
			$r = $q->num_rows();

			if($r>0){
				$_SESSION['th_ak'] = $th_ak;
				$_SESSION['smt'] = $smt;
				$_SESSION['nim'] = $nim;
				// $this->session->set_userdata($sess_data);
				echo "Sukses";
			}else{
				echo "Maaf, Tidak ada data";
			}
		}else{
			redirect('login','refresh');
		}
	}

	public function print_krs()
	{
		$cek = @$_SESSION['logged_in'];
		$level = @$_SESSION['level'];
		if(!empty($cek) && $level=='admin'){

			$th_ak = @$_SESSION['th_ak'];
			$smt = @$_SESSION['smt'];
			$nim = @$_SESSION['nim'];

			// $q = $this->db->query("SELECT * FROM krs WHERE th_akademik='$th_ak' AND semester='$smt' AND nim='$nim' ");
			$this->db->where('th_akademik',$th_ak);
			$this->db->where('semester',$smt);
			$this->db->where('nim',$nim);
			$q = $this->db->get('krs');
			$r = $q->num_rows();

			// echo $r;die;

			if($r>0){

				$nama 	= $this->model_data->cari_nama_mhs($nim);
				$kd_prodi	= $this->model_data->cari_kd_prodi_mhs($nim);
				$prodi = $this->model_data->nama_jurusan($kd_prodi);
				$semester = $this->model_data->cari_smt_krs($th_ak,$smt,$nim);
				$ip_lalu = $this->model_data->cari_ipk_lalu($semester,$nim);


			  $pdf=new reportProduct();
			  $pdf->setKriteria("cetak_laporan");
			  $pdf->setNama("CETAK KRS");
			  $pdf->AliasNbPages();
			  $pdf->AddPage("P","A4");
				//foreach($data->result() as $t){
					$A4[0]=210;
					$A4[1]=297;
					$Q[0]=216;
					$Q[1]=279;
					$pdf->SetTitle('Cetak KRS');
					$pdf->SetCreator('Programmer IT with fpdf');

					$h = 10;
					$pdf->SetFont('Times','B',16);
					$pdf->image(base_url().'assets/img/logo.png',95,5,20,20);
					$pdf->Ln(15);
					$pdf->Cell(198,7,$this->config->item('nama_pendek'),0,1,'C');
					$pdf->SetFont('Times','B',14);
					$pdf->Cell(198,7,$this->config->item('nama_instansi'),0,1,'C');
					$pdf->SetFont('Times','',10);
					$pdf->Cell(198,4,'Alamat : '.$this->config->item('alamat_instansi'),0,1,'C');
					$pdf->Ln(8);

					//Column widths
					$pdf->SetFont('Courier','B',16);
					$pdf->Cell(198,4,'KARTU RENCANA STUDI (KRS) MAHASISWA',0,1,'C');
					$pdf->Ln(5);

					$h = 6;

					$pdf->SetFont('Courier','',12);
					$pdf->Cell(30,$h,'NIM',0,0,'L');
					$pdf->Cell(50,$h,': '.$nim,0,0,'L');
					$pdf->SetX(120);
					$pdf->Cell(35,$h,'Tahun Akademik ',0,0,'L');
					$pdf->Cell(50,$h,': '.$th_ak,0,1,'L');

					// $pdf->SetFont('Arial','',12);
					$pdf->Cell(30,$h,'Nama',0,0,'L');
					$pdf->Cell(50,$h,': '.strtoupper($nama),0,0,'L');
					$pdf->SetX(120);
					$pdf->Cell(35,$h,'Semester ',0,0,'L');
					$pdf->Cell(50,$h,': '.strtoupper($smt).'/'.$semester,0,1,'L');

					// $pdf->SetFont('Arial','',12);
					$pdf->Cell(30,$h,'PRODI',0,0,'L');
					$pdf->Cell(50,$h,': '.$prodi,0,0,'L');
					$pdf->SetX(120);
					$pdf->Cell(35,$h,'IP smt. Lalu ',0,0,'L');
					$pdf->Cell(50,$h,': '.$ip_lalu,0,1,'L');


					$w = array(10,75,10,15,20,20,40);

					//Header

					$pdf->SetFont('Courier','B',10);
					$pdf->SetFillColor(204,204,204);
    				$pdf->SetTextColor(0);
					$fill = true;
					$h=8;
					$pdf->Cell($w[0],$h,'No','TB',0,'C',$fill);
					$pdf->Cell($w[1],$h,'Mata Kuliah','TB',0,'C',$fill);
					$pdf->Cell($w[2],$h,'SKS','TB',0,'C',$fill);
					$pdf->Cell($w[3],$h,'Hari','TB',0,'C',$fill);
					$pdf->Cell($w[4],$h,'Pukul','TB',0,'C',$fill);
					$pdf->Cell($w[5],$h,'Ruang','TB',0,'C',$fill);
					$pdf->Cell($w[6],$h,'Dosen','TB',0,'C',$fill);
					$pdf->Ln();

					//data
					//$pdf->SetFillColor(224,235,255);
					$h = 7;
					$pdf->SetFont('Helvetica','',8);
					$pdf->SetFillColor(204,204,204);
    				$pdf->SetTextColor(0);
					$fill = false;
					$no=1;
					$jmlsks = 0;
					foreach($q->result() as $row)
					{
						$pdf->Cell($w[0],$h,$no,0,0,'C',$fill);
						$pdf->Cell($w[1],$h,$row->kd_mk.'-'.$row->nama_mk,0,0,'L',$fill);
						$pdf->Cell($w[2],$h,$row->sks,0,0,'C',$fill);
						$pdf->Cell($w[3],$h,$row->hari,0,0,'C',$fill);
						$pdf->Cell($w[4],$h,$row->pukul,0,0,'C',$fill);
						$pdf->Cell($w[5],$h,$row->ruang,0,0,'C',$fill);
						$pdf->Cell($w[6],$h,$row->nm_dosen,0,0,'L',$fill);
						$pdf->Ln();
						$fill = !$fill;
						$jmlsks = $jmlsks+$row->sks;
						$no++;
					}
					// Closing line
					$pdf->Cell(array_sum($w),0,'','T');
					$pdf->Ln();
					$pdf->Cell(85,$h,'Jumlah SKS :',0,0,'R');
					$pdf->Cell(10,$h, $jmlsks,0,0,'C');

					$pdf->SetFont('Helvetica','',9);
					$pdf->Ln(10);
					$h = 5;
					$pdf->Cell(50,$h,'Menyetujui',0,0,'C');
					$pdf->SetX(110);
					$pdf->Cell(100,$h,$this->config->item('kota').', '.$this->model_global->tgl_indo(date('Y-m-d')),0,1,'C');
					$pdf->Cell(50,$h,'Dosen Pembimbing,',0,0,'C');
					$pdf->SetX(110);
					$pdf->Cell(100,$h,'Mahasiswa',0,1,'C');
					$pdf->Ln(20);
					$pdf->Cell(50,$h,'_______________________',0,0,'C');
					$pdf->SetX(110);
					$pdf->Cell(100,$h,$nama,0,1,'C');
					$pdf->Cell(50,$h,'NIP : ',0,0,'L');
					$pdf->SetX(110);
					$pdf->Cell(100,$h,'NIM :'.$nim,0,1,'C');
				//}

				//}
				$pdf->Output('KRS_'.$th_ak.'_'.$smt.'_'.$nim.'.pdf','D');

			}else{
				$this->session->set_flashdata('result_info', '<center>Tidak Ada Data</center>');
				redirect('krs');
				//echo "Maaf Tidak ada data";
			}
		}else{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
