<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lap_khs_rekap extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Programmer : Deddy Rusdiansyah.S.Kom
	 * http://deddyrusdiansyah.blogspot.com
	 * http://softwarebanten.com
	 * TIM : Edy Nasri, Aldi Novialdi Rusdiansyah, Eka Juliananta
	 * Developer : Fitria Wahyuni.S.Pd
	 */
	public function index()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){
			$d['judul']="Rekap KHS Mahasiswa";
			$d['class'] = "laporan";

			$d['content']= 'laporan/lap_khs_rekap';
			$this->load->view('home',$d);
		}else{
			redirect('login','refresh');
		}
	}

	public function cari_data()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			$th = $this->input->post('th');
			$kd_prodi = $this->input->post('kd_prodi');
			//$kelas = $this->input->post('kelas');

			/*
			$query = "SELECT *
						FROM krs as a
						RIGHT JOIN mahasiswa as b
						ON a.nim = b.nim
					WHERE a.th_akademik='$th_ak' AND a.semester='$smt' AND b.kd_prodi='$kd_prodi'
					GROUP BY b.nim";
			*/
			$this->db->select('krs.smt , krs.nim, krs.nilai_akhir, krs.sks, mahasiswa.nama_mhs');
			$this->db->from('krs','mahasiswa');
			$this->db->like('krs.nim',$th,'after');
			$this->db->where('krs.kd_prodi',$kd_prodi);
			//$this->db->where('kelas',$kelas);
			$this->db->join('mahasiswa', 'mahasiswa.nim = krs.nim','right');
			$this->db->order_by('krs.nim','asc');
			$this->db->order_by('krs.smt','asc');

			$q = $this->db->get(); // "SELECT * FROM mahasiswa WHERE status='Aktif' AND th_akademik='$th' AND kd_prodi='$kd_prodi'";
			// $q = $this->db->query($query);
			// die ($this->db->last_query());
			// $dt['th_ak'] = $th_ak;
			// $dt['smt']= $smt;
			// $dt['data'] = $q;
			$dt['data'] = $q->result_array(); 

			echo $this->load->view('laporan/view_lap_khs_rekap',$dt);

		}else{
			redirect('login','refresh');
		}
	}

	public function cetak_khs()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			$th = $this->input->post('th');
			$kd_prodi = $this->input->post('kd_prodi');
			//$kelas = $this->input->post('kelas');

			/*
			$query = "SELECT *
						FROM krs as a
						RIGHT JOIN mahasiswa as b
						ON a.nim = b.nim
					WHERE a.th_akademik='$th_ak' AND a.semester='$smt' AND b.kd_prodi='$kd_prodi'
					GROUP BY b.nim";
			*/
			$this->db->select('krs.smt , krs.nim, krs.nilai_akhir, krs.sks, mahasiswa.nama_mhs');
			$this->db->from('krs','mahasiswa');
			$this->db->like('krs.nim',$th,'after');
			$this->db->where('krs.kd_prodi',$kd_prodi);
			//$this->db->where('kelas',$kelas);
			$this->db->join('mahasiswa', 'mahasiswa.nim = krs.nim','right');
			$this->db->order_by('krs.nim','asc');
			$this->db->order_by('krs.smt','asc');


			$q = $this->db->get(); // "SELECT * FROM mahasiswa WHERE status='Aktif' AND th_akademik='$th' AND kd_prodi='$kd_prodi'";
			// $q = $this->db->query($query);
			// die ($this->db->last_query());
			// $dt['th_ak'] = $th_ak;
			// $dt['smt']= $smt;
			// $dt['data'] = $q;
			//$dt['data'] = $q->result_array(); 
			if($q->num_rows()>0){
				$_SESSION['th_exc'] = $th;
				$_SESSION['kd_prodi_exc'] = $kd_prodi;

				echo "Sukses";
			}else{
				echo "Maaf, Tidak ada data";
			}
			// $this->load->view('laporan/view_lap_khs_rekap',$dt);

		}else{
			redirect('login','refresh');
		}
	}

	public function print_khs()
	{
		$cek = @$_SESSION['logged_in'];
		if(!empty($cek)){

			$th = $_SESSION['th_exc'];
			$kd_prodi = $_SESSION['kd_prodi_exc'];
			//$kelas = $this->input->post('kelas');

			/*
			$query = "SELECT *
						FROM krs as a
						RIGHT JOIN mahasiswa as b
						ON a.nim = b.nim
					WHERE a.th_akademik='$th_ak' AND a.semester='$smt' AND b.kd_prodi='$kd_prodi'
					GROUP BY b.nim";
			*/
			$this->db->select('krs.smt , krs.nim, krs.nilai_akhir, krs.sks, mahasiswa.nama_mhs');
			$this->db->from('krs','mahasiswa');
			$this->db->like('krs.nim',$th,'after');
			$this->db->where('krs.kd_prodi',$kd_prodi);
			//$this->db->where('kelas',$kelas);
			$this->db->join('mahasiswa', 'mahasiswa.nim = krs.nim','right');
			$this->db->order_by('krs.nim','asc');
			$this->db->order_by('krs.smt','asc');


			$q = $this->db->get(); // "SELECT * FROM mahasiswa WHERE status='Aktif' AND th_akademik='$th' AND kd_prodi='$kd_prodi'";
			// $q = $this->db->query($query);
			// die ($this->db->last_query());
			// $dt['th_ak'] = $th_ak;
			// $dt['smt']= $smt;
			// $dt['data'] = $q;
			$dt['data'] = $q->result_array(); 
			$dt['file_name'] = "KHS_ALL_".$th."_".$kd_prodi;

			$this->load->view('laporan/view_lap_khs_rekap_print',$dt);

		}else{
			redirect('login','refresh');
		}
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
