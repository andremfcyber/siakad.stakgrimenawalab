<?php
  class Model_data extends CI_Model {
    /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Model khusus untuk pengambilan data
   */

    public function data_jurusan(){
      $q = $this->db->order_by('kd_prodi');
      $q = $this->db->get('prodi');
      return $q;
    }

    public function data_pribadi($np){
      $this->db->select('pmb.data_pribadi.*, prodi.kd_prodi, prodi.prodi');
      $this->db->from('pmb.data_pribadi');
      $this->db->join('prodi', 'pmb.data_pribadi.prodi = prodi.kd_prodi');
      $this->db->where('no_pendaftaran', $np);
      return $this->db->get()->row_array();
    }
  
    public function data_keluarga($np){
      return $this->db->get_where('pmb.data_keluarga', array('no_pendaftaran' => $np))->row_array();
    }

    public function data_pp($np){
      return $this->db->get_where('pmb.data_pp', array('no_pendaftaran' => $np))->row_array();
    }

    public function data_rohani($np){
      return $this->db->get_where('pmb.data_rohani', array('no_pendaftaran' => $np))->row_array();
    }

    public function data_lain($np){
      return $this->db->get_where('pmb.data_lain', array('no_pendaftaran' => $np))->row_array();
    }

    public function data_upload($np){
      return $this->db->get_where('pmb.data_upload', array('no_pendaftaran' => $np))->row_array();
    }

    public function users_all(){
      $this->db->select('pmb.users.no_pendaftaran, pmb.data_pribadi.nama, pmb.data_pribadi.prodi, prodi.prodi AS nama_prodi');
      $this->db->from('pmb.users');
      $this->db->where('done',1);
      $this->db->where('mhs',0);
      $this->db->where('delete',0);
      $this->db->join('pmb.data_pribadi', 'pmb.users.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      $this->db->join('prodi', 'pmb.data_pribadi.prodi = prodi.kd_prodi');
      if(!empty($_GET['kd_prodi']) && $_GET['kd_prodi'] != 'all') {
        $kd_prodi = $_GET['kd_prodi'];
        $this->db->where("pmb.data_pribadi.prodi = '$kd_prodi'");
      }
     
      // $tanggal=date('Y');

      // $this->db->where("to_char(pmb.data_pribadi.tgl_insert::date,'Y') = '".$tanggal."'");
      $q = $this->db->get();
      if($q->num_rows()>0){
        return $q;
      }else{
        return null;
      }
    }

    public function data_all($np){
      $this->db->select('*');
      $this->db->from('pmb.data_pribadi');
      $this->db->where('pmb.data_pribadi.no_pendaftaran',$np);
      $this->db->join('pmb.data_keluarga', 'pmb.data_keluarga.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      $this->db->join('pmb.data_pp', 'pmb.data_pp.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      $this->db->join('pmb.data_rohani', 'pmb.data_rohani.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      $this->db->join('pmb.data_lain', 'pmb.data_lain.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      // $this->db->join('pmb.data_upload', 'pmb.data_upload.no_pendaftaran = pmb.data_pribadi.no_pendaftaran');
      $q = $this->db->get();
      if($q->num_rows()>0){
        // return $this->db->last_query();
        return $q->result();
      }else{
        return null;
      }
    }

    public function get_data_upload($np){
      $this->db->select('*');
      $this->db->from('pmb.data_upload');
      $this->db->where('pmb.data_upload.no_pendaftaran',$np);
      $q = $this->db->get();
      if($q->num_rows()>0){
        // return $this->db->last_query();
        return $q->result();
      }else{
        return null;
      }
    }

    public function admins_all(){
      $this->db->select('*');
      $this->db->from('pmb.admins');
      $q = $this->db->get();
      if($q->num_rows()>0){
        return $q->result();
      }else{
        return null;
      }
      // return $q->result();
    }

    public function admin_detail($username){
      $res = $this->db->get_where('pmb.admins', array('username' => $username));
      $row = $res->num_rows();
      if($row>0){
        return $res->result();
      }else{
        return null;
      }
    }
  }
?>