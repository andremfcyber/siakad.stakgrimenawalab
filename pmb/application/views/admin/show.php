<header class="header-adm">
  <h5 class="float-left">Welcome, <?=$_SESSION['username'];?></h5>
  <a class="nostyleLogout" href="<?=site_url();?>home/logout" id="logout"><i class="fas fa-user-circle"></i> LOGOUT</a>
</header>
<main class="container-custom">
  <section>
  <!-- <pre>
  <?php //echo $q;?>
  <?php //print_r($q);?>
  </pre> -->
    <table class="table w-100 bordered" id="myTable">
      <thead>
        <th width="5%">No.</th>
        <th width="25%">No Pendaftaran</th>
        <th width="25%">Nama</th>
        <th width="25%">Jurusan</th>
        <th width="10%">Jenjang</th>
        <th width="10%" style="text-align:center">Details</th>
      </thead>
      <tbody>
        
      </tbody>
    </table>
  </section>
</main>
<?php
  $kd_prodi = '';
  if (isset($_GET['kd_prodi'])){
    $kd_prodi = $_GET['kd_prodi'];
  }else{
    $kd_prodi = 'all';
  }
?>
<script>
$(document).ready(function(){
  //console.log("ready!");
  var kd_prodi = "<?= $kd_prodi ?>"
  var x = 'getUsersAll?kd_prodi='+kd_prodi
  
  // console.log(x);
  $('#myTable').DataTable({
    ajax: {
      url: x,
      // url: "<?//php echo base_url('index.php/admin/getUsersAll?kd_prodi='.$_GET['kd_prodi']); ?>",
      dataSrc: 'data'},
      columns: [
        {data: 'no'},
        {data: 'no_pendaftaran'},
        {data: 'nama'},
        {data: 'prodi'},
        {data: 'jenjang'},
        {data: 'ket', orderable: false}
      ]
  });
})
</script>