<header class="header-adm">
  <h5 class="float-left">Welcome, <?=$_SESSION['username'];?></h5>
  <a class="nostyleLogout" href="<?=site_url();?>home/logout" id="logout"><i class="fas fa-user-circle"></i> LOGOUT</a>
</header>
<main class="container-custom">
  <section>
  <!-- <pre>
  <?php //echo $q;?>
  <?php //print_r($data);?>
  </pre> -->
  <button type="buton" class="btn btn-success btn-sm float-right" id="btn-add" data-toggle="modal" data-target="#modalAdd"><i class="fas fa-plus"></i> Add Data</button>
    <table class="table w-100 bordered" id="myTable">
      <thead>
        <th width="5%">No.</th>
        <th width="40%">Username</th>
        <th width="40%">Email</th>
        <th width="15%" style="text-align:center">Edit</th>
      </thead>
      <tbody>
        
      </tbody>
    </table>
  </section>

  <section>
    <!-- Modal Register-->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAdd" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Register</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" class="" method="POST" id="formAdd">
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                <small class="text-danger">*Jangan gunakan spasi, jika ada maka spasi akan otomatis terhapus</small>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control w-100" name="email" id="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="password2" id="password2" placeholder="Confirm Password">
              </div>
              <!-- <button type="submit" class="btn btn-login btn-sm btn-block">Submit</button>
              <a href="<?=site_url();?>login" type="button" class="btn btn-login btn-sm btn-block">Back</a> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary w-50" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary w-50" id="btn-save">Save</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Edit-->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEdit" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Edit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="" class="" method="POST" id="formEdit">
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="usernameEd" id="usernameEd" placeholder="Username" readonly>
              </div>
              <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control w-100" name="emailEd" id="emailEd" placeholder="Email">
              </div>
              <label class="text-danger">Masukkan password untuk mengupdate data</label>
              <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="passwordEd" id="passwordEd" placeholder="Password">
              </div>
              <!-- <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="password2Ed" id="password2Ed" placeholder="Confirm Password">
              </div> -->
              <!-- <button type="submit" class="btn btn-login btn-sm btn-block">Submit</button>
              <a href="<?=site_url();?>login" type="button" class="btn btn-login btn-sm btn-block">Back</a>-->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary w-50" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary w-50" id="btn-saveEd">Save</button>
          </div>
        </div>
      </div>
    </div>

  </section>
</main>

<script>
$(document).ready(function(){
  //console.log("ready!");
  $('#myTable').DataTable({
    ajax: {
      // url:'/pmb/index.php/admin/getAdminsAll',
      url: "<?php echo base_url('index.php/admin/getAdminsAll'); ?>",
      dataSrc: 'data'},
      columns: [
        {data: 'no'},
        {data: 'username'},
        {data: 'email'},  
        {data: 'ket', orderable: false}
      ],
      'columnDefs': [{
        "targets": 3, // your case first column
        "className": "text-center",
      }]
  });  
})

$('#btn-save').click(function(){
  let email = $("#email").val();
  let username = $("#username").val();
  let password = $("#password").val();
  let password2 = $("#password2").val();
  let string = $("#formAdd").serialize();

  if(password != password2){
    alert("Maaf, Konfirmasi password harus sama..");
    $("#password2").focus();
    return false;
  }
  if(email.length==0){
    alert("Maaf, Email Tidak boleh kosong..");
    $("#email").focus();
    return false;
  }
  if(username.length==0){
    alert("Maaf, Username Tidak boleh kosong..");
    $("#username").focus();
    return false;
  }
  if(password.length==0){
    alert("Maaf, Password Tidak boleh kosong..");
    $("#password").focus();
    return false;
  }
  // console.log(string);
  $.ajax({
    type	: 'POST',
    url		: "<?php echo site_url('admin/cekUsername'); ?>",
    data	: string,
    cache	: false,
    success	: function(data){
      if(data == "kosong"){
        add("add");
      }else{
        alert("Maaf, Username sudah digunakan..");
        $("#username").focus();
        return false;
      }
    }
  });
});

$('#btn-saveEd').click(function(){
  let email = $("#emailEd").val();
  let username = $("#usernameEd").val();
  let password = $("#passwordEd").val();
  let string = $("#formEdit").serialize();

  if(email.length==0){
    alert("Maaf, Email Tidak boleh kosong..");
    $("#email").focus();
    return false;
  }
  if(username.length==0){
    alert("Maaf, Username Tidak boleh kosong..");
    $("#username").focus();
    return false;
  }
  if(password.length==0){
    alert("Maaf, Password Tidak boleh kosong..");
    $("#password").focus();
    return false;
  }
  // console.log(string);
  $.ajax({
    type	: 'POST',
    url		: "<?php echo site_url('admin/cekPassword'); ?>",
    data	: string,
    cache	: false,
    success	: function(data){
      if(data == "00"){
        add("edit");
        // console.log("edit");
      }else{
        alert(data);
        $("#passwordEd").focus();
        return false;
      }
    }
  });
});

function add($opt){
  // console.log("nice");
  let string = "";
  if($opt == "edit"){
    string = $("#formEdit").serialize();
  }else{
    string = $("#formAdd").serialize();
  }
  console.log(string);
  $.ajax({
    type	: 'POST',
    url		: "<?php echo site_url('admin/saveData'); ?>",
    data	: string,
    cache	: false,
    success	: function(data){
        alert(data);
        location.reload();
    }
  });
}


function coba(username){
  const e = document.querySelector(`#btn-edit${username}`);
  let u = e.dataset.username;
  //console.log(u);
  // pakai getJSON aja buat latihan, $.getJSON sama $.ajax sama aja cuma $getJSON mah versi sederhana penggunaan ajax untuk balikan berupa json

  $.getJSON("<?= site_url();?>admin/getAdminDetail/"+username, function(result){
    let res_data = result.data[0];
    let code = result.rc;

    if(code == "00"){
      // console.log("ada data");
      // console.log(res_data.username +" "+ res_data.email);
      $("#usernameEd").val(res_data.username);
      $("#emailEd").val(res_data.email);
      //$("#passwordEd").val(res_data.password);
    }else{
      alert("No data");
    }
  });
}
</script>