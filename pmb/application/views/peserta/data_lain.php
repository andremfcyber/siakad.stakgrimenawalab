 <div class="row padding-tb-30 justify-content-center">
    <div class="col-sm-9">
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 80%"></div>
        </div>
    </div>
</div>
    
  <div class="row padding-tb-30 justify-content-center">
    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataLain" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">E. LAIN-LAIN</th>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="harapan" class="col-form-label-sm">Harapkan Anda untuk Sekolah Teologi</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-12" id="harapan" name="harapan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="darimana" class="col-form-label-sm">Darimana Anda Mendapat Informasi Mengenai Sekolah Teologi?</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-12" id="darimana" name="darimana">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="thn_daftar_bef" class="col-form-label-sm">Apakah Anda Pernah Mendaftar di Sekolah Teologi Sebelumnya?</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-4" id="thn_daftar_bef" name="thn_daftar_bef" placeholder="Tahun">
                <small>Kosongkan Jika Tidak</small>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="utusan_stat" class="col-form-label-sm">Apakah Anda Merupakan Utusan Gereja?</label></td>
              <td class="col-8">
                <select name="utusan_stat" id="utusan_stat" class="custom-select custom-select-sm col-3" onchange="assignTo(this.value);">
                  <option value="">-Pilih-</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_utus" class="col-form-label-sm">Nama Gereja Pengutus</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-12" id="nama_utus" name="nama_utus" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_utus" class="col-form-label-sm">Alamat Gereja Pengutus</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_utus" name="alamat_utus" rows="3" disabled></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp_utus" class="col-form-label-sm">No. Telepon</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp_utus" name="tlp_utus" disabled>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="utus_stat" class="col-form-label-sm">Apakah Anda Terikat Janji dengan Gereja Pengutus?</label></td>
              <td class="col-8">
                <select name="utus_stat" id="utus_stat" class="custom-select custom-select-sm col-3" disabled>
                  <option value="">-Pilih-</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>
                <small>Jika YA, Lampirkan Surat Ikatan Janji</small>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="mandiri_stat" class="col-form-label-sm">Apakah Anda Menanggung Biaya Kuliah Sendiri di Sekolah Teologi?</label></td>
              <td class="col-8">
                <select name="mandiri_stat" id="mandiri_stat" class="custom-select custom-select-sm col-3">
                  <option value="">-Pilih-</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="nama_biaya" class="col-form-label-sm">Nama Orang yang Menanggung Biaya Kuliah Anda</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-8" id="nama_biaya" name="nama_biaya">
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="alamat_biaya" class="col-form-label-sm">Alamat Rumah Penanggung Biaya</label></td>
              <td class="col-8">
                <textarea class="form-control form-control-sm col-12" id="alamat_biaya" name="alamat_biaya" rows="3"></textarea>
              </td>
            </tr>
            <tr class="row">
              <td class="col-4"><label for="tlp_biaya" class="col-form-label-sm">No. Telepon</label></td>
              <td class="col-8">
                <input type="text" class="form-control form-control-sm col-6" id="tlp_biaya" name="tlp_biaya">
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_rohani" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Upload File</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
document.addEventListener('DOMContentLoaded', getData);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_lain',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        // Check utusan_stat ada/engga, trus input disabled nya on atau off berdasarkan itu
        if(r.utusan_stat == 1){
          disabledOff('#nama_utus');
          disabledOff('#alamat_utus');
          disabledOff('#tlp_utus');
          disabledOff('#utus_stat');
        }

        document.querySelector('#harapan').value = r.harapan;
        document.querySelector('#darimana').value = r.darimana;
        document.querySelector('#thn_daftar_bef').value = r.thn_daftar_bef;
        document.querySelector('#utusan_stat').value = r.utusan_stat;
        document.querySelector('#nama_utus').value = r.nama_utus;
        document.querySelector('#alamat_utus').value = r.alamat_utus;
        document.querySelector('#tlp_utus').value = r.tlp_utus;
        document.querySelector('#utus_stat').value = r.utus_stat;
        document.querySelector('#mandiri_stat').value = r.mandiri_stat;
        document.querySelector('#nama_biaya').value = r.nama_biaya;
        document.querySelector('#alamat_biaya').value = r.alamat_biaya;
        document.querySelector('#tlp_biaya').value = r.tlp_biaya;
      }
    }
  }
  xhr.send(params);
}

function assignTo(r){
  if(r == 1 ){
    disabledOff(['#nama_utus','#alamat_utus','#tlp_utus','#utus_stat']);
  }else{
    disabledOn(['#nama_utus','#alamat_utus','#tlp_utus','#utus_stat']);
  }
}

function disabledOn(kelas){
  //console.log(kelas);
  if(Array.isArray(kelas)){
    //console.log(kelas);
    kelas.forEach(function(kelas_single){
      const e = document.querySelector(kelas_single);
      e.setAttribute('disabled',true);
      e.value = "";
    });
  }else{
    const e = document.querySelector(kelas);
    e.setAttribute('disabled',true);
    e.value = "";
  }
}

function disabledOff(kelas){
  //console.log(kelas);
  if(Array.isArray(kelas)){
    //console.log(kelas);
    kelas.forEach(function(kelas_single){
      const e = document.querySelector(kelas_single);
      e.removeAttribute('disabled');
    });
  }else{
    const e = document.querySelector(kelas);
    e.removeAttribute('disabled');
  }
}
  

</script>