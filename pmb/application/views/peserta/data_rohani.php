    <div class="row padding-tb-30 justify-content-center">
        <div class="col-sm-9">
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
            </div>
        </div>
    </div> 
    
  <div class="row padding-tb-30 justify-content-center">
    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataRohani" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">D. DATA KEROHANIAN DAN KARAKTER</th>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="anggota_thn" class="col-form-label-sm">Sejak Tahun Berapa Anda Menjadi Anggota Gereja Anda?</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-4" id="anggota_thn" name="anggota_thn" placeholder="Tahun">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="baptisA_thn" class="col-form-label-sm">Apakah Anda Sudah Dibaptis Anak?</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-4" id="baptisA_thn" name="baptisA_thn" placeholder="Tahun">
                <small>Kosongkan Jika Tidak</small>
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="sidi_thn" class="col-form-label-sm">Apakah Anda Telah di Sidi?</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-4" id="sidi_thn" name="sidi_thn" placeholder="Tahun">
                <small>Kosongkan Jika Tidak</small>
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="baptisD_thn" class="col-form-label-sm">Apakah Anda Pernah Dibaptis Dewasa?</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-4" id="baptisD_thn" name="baptisD_thn" placeholder="Tahun">
                <small>Kosongkan Jika Tidak</small>
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="sidi_by" class="col-form-label-sm">Baptisan Dewasa/Sidi Dilayankan Oleh</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-8" id="sidi_by" name="sidi_by">
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm">Pengalaman Pelayanan Utama Anda</label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="7%">No.</th>
                    <th width="33%">Jenis Pelayanan</th>
                    <th width="40%">Tempat Pelayanan</th>
                    <th width="20%">Tahun</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td><input type="text" id="jenis_pelayanan1" name="jenis_pelayanan1" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="tempat_pelayanan1" name="tempat_pelayanan1" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="thn_pelayanan1" name="thn_pelayanan1" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td><input type="text" id="jenis_pelayanan2" name="jenis_pelayanan2" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="tempat_pelayanan2" name="tempat_pelayanan2" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="thn_pelayanan2" name="thn_pelayanan2" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr>
                    <td class="text-center identifier-no">3</td>
                    <td><input type="text" id="jenis_pelayanan3" name="jenis_pelayanan3" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="tempat_pelayanan3" name="tempat_pelayanan3" class="form-control form-control-sm col-12"></td>
                    <td><input type="text" id="thn_pelayanan3" name="thn_pelayanan3" class="form-control form-control-sm col-12"></td>
                  </tr>
                  <tr id="tr-tambah">
                    <td colspan="6"><a class="btn" href="" id="addRowPelayanan">&nbsp<i class="fas fa-plus-circle"></i></a></td>
                  </tr>
                </table>
                <br>
                <input type="text" name="jml_pelayanan" id="jml_pelayanan" value="3" hidden>
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="hambatan_pertumbuhan" class="col-form-label-sm">Uraikan Secara Singkat Apa Hambatan Utama dalam Pertumbuhan Rohani Anda</label></td>
              <td class="col-7">
                <input type="text" id="hambatan_pertumbuhan" name="hambatan_pertumbuhan" class="form-control form-control-sm col-12">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="hambatan_pelayanan" class="col-form-label-sm">Uraikan Secara Singkat Apa Hambatan Utama dalam Pelayanan Anda</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-12" id="hambatan_pelayanan" name="hambatan_pelayanan">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="hambatan_masuk" class="col-form-label-sm">Uraikan Secara Singkat Apa Hambatan Utama Anda untuk Masuk di Sekolah Teologi</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-12" id="hambatan_masuk" name="hambatan_masuk">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="masalah" class="col-form-label-sm">Apakah Anda Merasa Memiliki Masalah yang Akan Menghambat Kelacaran Studi Anda?</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-12" id="masalah" name="masalah">  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="masalah_detail" class="col-form-label-sm">Jelaskan Masalah Tersebut</label></td>
              <td class="col-7">
                <textarea class="form-control form-control-sm col-12" id="masalah_detail" name="masalah_detail" rows="3"></textarea>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="jml_baca" class="col-form-label-sm">Apakah Anda Rutin Membaca Alkitab Anda? Sudah Selesai Berapa Kali</label></td>
              <td class="col-7">
                <input type="text" id="jml_baca" name="jml_baca" class="form-control form-control-sm col-2 float-left">
                <label class="col-form-label-sm">&nbsp kali</label>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="persembahan_stat" class="col-form-label-sm">Apakah Anda Rutin Memberikan Persembahan Perpuluhan Anda</label></td>
              <td class="col-7">
                <select name="persembahan_stat" id="persembahan_stat" class="custom-select custom-select-sm col-3">
                  <option value="">-Pilih-</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="persembahan_alasan" class="col-form-label-sm">Sebutkan Alasan dari Jawaban Anda</label></td>
              <td class="col-7">
                <input type="text" class="form-control form-control-sm col-12" id="persembahan_alasan" name="persembahan_alasan">  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="pinjam_stat" class="col-form-label-sm">Apakah Anda Senang Meminjam Uang Atau Barang Kepada Orang Lain?</label></td>
              <td class="col-7">
                <select name="pinjam_stat" id="pinjam_stat" class="custom-select custom-select-sm col-3">
                  <option value="">-Pilih-</option>
                  <option value="1">Ya</option>
                  <option value="0">Tidak</option>
                </select>  
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="hutang" class="col-form-label-sm">Apakah Anda Saat Ini Sedang Menanggung Hutang?</label></td>
              <td class="col-7">
                <input type="text" id="hutang" name="hutang" class="form-control form-control-sm col-5 float-left">
                <label class="col-form-label-sm">&nbsp Rupiah</label><br>
                <small>Kosongkan Jika Tidak</label> 
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="buku" class="col-form-label-sm">Buku Apa yang Pernah Anda Baca dan Sangat Mempengaruhi Hidup Anda?</label></td>
              <td class="col-7">
                <input type="text" id="buku" name="buku" class="form-control form-control-sm col-12">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="ht_pengaruh" class="col-form-label-sm">Hamba Tuhan Siapakah yang Paling Berpengaruh dalam Pertumbuhan Rohani Anda?</label></td>
              <td class="col-7">
                <input type="text" id="ht_pengaruh" name="ht_pengaruh" class="form-control form-control-sm col-12">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="hobby" class="col-form-label-sm">Hobby dan Kegemaran</label></td>
              <td class="col-7">
                <input type="text" id="hobby" name="hobby" class="form-control form-control-sm col-12">
              </td>
            </tr>
            <tr class="row">
              <td class="col-5"><label for="olahraga" class="col-form-label-sm">Jenis Olahraga yang Anda Gemari dan Kuasai</label></td>
              <td class="col-7">
                <input type="text" id="olahraga" name="olahraga" class="form-control form-control-sm col-12">
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_pp" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 5</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>

document.addEventListener('DOMContentLoaded', function(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_rohani',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);
        const jenisP = r.jenis_pelayanan.split("|");
        let PLength = jenisP.length;
        let i;
        // console.log(SKLength);
        if(PLength>3){
          // let SKElement = SKLength - 3;
          for(i=4; i<=PLength; i++){
            let tr_tombol = document.querySelector('#tr-tambah');
            let no = i;
            // console.log(no);
            tr_tombol.insertAdjacentHTML("beforebegin",`
            <tr>
              <td class="text-center identifier-no">${no}</td>
              <td><input type="text" id="jenis_pelayanan${no}" name="jenis_pelayanan${no}" class="form-control form-control-sm col-12"></td>
              <td><input type="text" id="tempat_pelayanan${no}" name="tempat_pelayanan${no}" class="form-control form-control-sm col-12"></td>
              <td><input type="text" id="thn_pelayanan${no}" name="thn_pelayanan${no}" class="form-control form-control-sm col-12"></td>
            </tr>
            `);
            document.querySelector('#jml_pelayanan').value = no;
          } 
        }
      }
    }
  }
  xhr.send(params);  
  // getData();
});
window.setTimeout(getData, 500);

function getData(){
  let noPendaftaran = document.getElementById('noPendaftaran').value;
  let params = "no_pendaftaran="+noPendaftaran;

  let xhr = new XMLHttpRequest();
  xhr.open('POST','<?=site_url();?>getdata/get_data_rohani',true);
  xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
  xhr.onload = function(){
    if(this.status == 200){
      // console.log(this.responseText);
      if(this.responseText){
        let r = JSON.parse(this.responseText);

        document.querySelector('#anggota_thn').value = r.anggota_thn;
        document.querySelector('#baptisA_thn').value = r.baptisA_thn;
        document.querySelector('#sidi_thn').value = r.sidi_thn;
        document.querySelector('#baptisD_thn').value = r.baptisD_thn;
        document.querySelector('#sidi_by').value = r.sidi_by;
        
        const jenisP = r.jenis_pelayanan.split("|");
        const tempatP = r.tempat_pelayanan.split("|");
        const thnP = r.thn_pelayanan.split("|");
        let PLength = jenisP.length;
        let i;
        for(i=1; i<=PLength; i++){
          document.querySelector(`#jenis_pelayanan${i}`).value = jenisP[i-1];
          document.querySelector(`#tempat_pelayanan${i}`).value = tempatP[i-1];
          document.querySelector(`#thn_pelayanan${i}`).value = thnP[i-1];
        }
        
        document.querySelector('#hambatan_pertumbuhan').value = r.hambatan_pertumbuhan;
        document.querySelector('#hambatan_pelayanan').value = r.hambatan_pelayanan;
        document.querySelector('#hambatan_masuk').value = r.hambatan_masuk;
        document.querySelector('#masalah').value = r.masalah;
        document.querySelector('#masalah_detail').value = r.masalah_detail;
        document.querySelector('#jml_baca').value = r.jml_baca;
        document.querySelector('#persembahan_stat').value = r.persembahan_stat;
        document.querySelector('#persembahan_alasan').value = r.persembahan_alasan;
        document.querySelector('#pinjam_stat').value = r.pinjam_stat;
        document.querySelector('#hutang').value = r.hutang;
        document.querySelector('#buku').value = r.buku;
        document.querySelector('#ht_pengaruh').value = r.ht_pengaruh;
        document.querySelector('#hobby').value = r.hobby;
        document.querySelector('#olahraga').value = r.olahraga;
      }
    }
  }
  xhr.send(params);
}


document.getElementById('addRowPelayanan').addEventListener('click', function(e){
    e.preventDefault();
    let pelayanan = document.querySelectorAll('.identifier-no');
    let pelayanan_last = pelayanan[pelayanan.length-1];
    let tr_tombol = document.querySelector('#tr-tambah');
    let no = Number(pelayanan_last.innerHTML) + 1;
    // console.log(no);
    tr_tombol.insertAdjacentHTML("beforebegin",`
    <tr>
      <td class="text-center identifier-no">${no}</td>
      <td><input type="text" id="jenis_pelayanan${no}" name="jenis_pelayanan${no}" class="form-control form-control-sm col-12"></td>
      <td><input type="text" id="tempat_pelayanan${no}" name="tempat_pelayanan${no}" class="form-control form-control-sm col-12"></td>
      <td><input type="text" id="thn_pelayanan${no}" name="thn_pelayanan${no}" class="form-control form-control-sm col-12"></td>
    </tr>
    `);
    document.querySelector('#jml_pelayanan').value = no;
  });

</script>