
    
  <div class="row padding-tb-30 justify-content-center">
<section class="col-sm-9">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="menu">
      <div class="navbar-nav">
        <a class="nav-item nav-link active" href="" id="data_pribadi">Data Pribadi</a>
        <a class="nav-item nav-link" href="" id="data_keluarga">Data Keluarga</a>
        <a class="nav-item nav-link" href="" id="data_pp">Data Pendidikan dan Riwayat Pekerjaan</a>
        <a class="nav-item nav-link" href="" id="data_rohani">Data Kerohanian dan Karakter</a>
        <a class="nav-item nav-link" href="" id="data_lain">Data Lain-Lain</a>
        <a class="nav-item nav-link" href="" id="data_upload">Data Upload File</a>
        <a class="nav-item nav-link" href="<?=site_url();?>home/print_pdf" id="print_pdf" target="_blank">Print PDF</a>
        <!-- <a class="nav-item nav-link" href="<?=site_url();?>home/logout" id="logout">Logout</a> -->
      </div>
    </div>
  </nav>
  <section class="bg-light row padding-data flag">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_pribadi);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_pribadi['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row" width="5%">1</td>
          <td width="35%">Nama Lengkap</td>
          <td width="60%">: <?=$data_pribadi['nama'];?></td>
        </tr>
        <tr>
          <td scope="row">2</td>
          <td>Jenis Kelamin</td>
          <td>: <?php
          if($data_pribadi['jk']=='P'){
            echo "Perempuan";  
          }else{
            echo "Laki-Laki";
          }
          ?></td>
        </tr>
        <tr>
          <td scope="row">3</td>
          <td>Program Studi Pilihan</td>
          <td>: <?=$data_pribadi['prodi'];?></td>
        </tr>
        <tr>
          <td scope="row">4</td>
          <td>Tempat, Tanggal Lahir</td>
          <td>: <?=$data_pribadi['tempat_lahir'];?>, <?= date('d M Y', strtotime($data_pribadi['tgl_lahir']));?></td>
        </tr>
        <tr>
          <td scope="row">5</td>
          <td>Kewarganegaraan</td>
          <td>: <?=$data_pribadi['kewarganegaraan'];?></td>
        </tr>
        <tr>
          <td scope="row">6</td>
          <td>Alamat Lengkap</td>
          <td>: <?=$data_pribadi['alamat'];?>, Telp. <?=$data_pribadi['tlp']?></td>
        </tr>
        <!--<tr>-->
        <!--  <td scope="row">7</td>-->
        <!--  <td>Alamat Lengkap Saudara/Kenalan di Jabotabekban</td>-->
        <!--  <td>: <?=$data_pribadi['alamat_saudara'];?>, Telp. <?=$data_pribadi['tlp_saudara'];?></td>-->
        <!--</tr>-->
        <!--<tr>-->
        <!--  <td scope="row">8</td>-->
        <!--  <td>Pendidikan Formal Terakhir</td>-->
        <!--  <td>: <?=$data_pribadi['pend_terakhir'];?></td>-->
        <!--</tr>-->
        <tr>
          <td scope="row">9</td>
          <td>Nama Sekolah Terakhir</td>
          <td>: <?=$data_pribadi['asal_sekolah'];?>, Lulus Tahun: <?=$data_pribadi['thn_lulus'];?></td>
        </tr>
        <tr>
          <td scope="row">10</td>
          <td>Alamat Sekolah Terakhir</td>
          <td>: <?=$data_pribadi['alamat_sekolah'];?></td>
        </tr>
        <tr>
          <td scope="row">11</td>
          <td>Terakhir Tinggal di Rumah</td>
          <td>: <?=$data_pribadi['tinggal_dirumah'];?></td>
        </tr>
        <tr>
          <td scope="row">12</td>
          <td>Status Marital</td>
          <td>: <?=$data_pribadi['marital_stat'];?></td>
        </tr>
        <tr>
          <td scope="row">13</td>
          <td>Nama dan Tempat Tanggal Lahir Pasangan</td>
          <td>: <?=@$data_pribadi['nama_pasangan'];?>, TTL: <?=@$data_pribadi['tempat_lahir_pasangan'];?>, <?= date('d M Y', strtotime($data_pribadi['tgl_lahir_pasangan']));?></td>
        </tr>
        <tr>
          <td scope="row">14</td>
          <td>Jumlah Anak</td>
          <td>: Laki-Laki <?=$data_pribadi['jml_anak_L'];?> orang, Perempuan <?=$data_pribadi['jml_anak_P'];?> orang</td>
        </tr>
        <tr>
          <td scope="row">15</td>
          <td>Anggota Gereja</td>
          <td>: <?=$data_pribadi['gereja_anggota'];?></td>
        </tr>
        <tr>
          <td scope="row">16</td>
          <td>Alamat Gereja</td>
          <td>: <?=$data_pribadi['alamat_gereja_anggota'];?></td>
        </tr>
        <tr>
          <td scope="row">17</td>
          <td>Beribadah dan Melayani di Gereja yang Sama</td>
          <td>: <?=$data_pribadi['ibadah_melayani_gereja'] == 1 ? "Ya" : "Tidak";?></td>
        </tr>
        <tr>
          <td scope="row">18</td>
          <td>Nama Gereja Tempat Beribadah</td>
          <td>: <?=$data_pribadi['gereja_ibadah'];?></td>
        </tr>
        <tr>
          <td scope="row">19</td>
          <td>Alamat Gereja Tempat Beribadah</td>
          <td>: <?=$data_pribadi['alamat_gereja_ibadah'];?></td>
        </tr>
        <tr>
          <td scope="row">20</td>
          <td>Pelayanan Utama di Gereja</td>
          <td>: <?=$data_pribadi['gereja_pelayanan'];?></td>
        </tr>
        <tr>
          <td scope="row">21</td>
          <td>Pekerjaan</td>
          <td>: <?=$data_pribadi['pekerjaan'];?></td>
        </tr>
        <tr>
          <td scope="row">22</td>
          <td>Alamat Perusahaan</td>
          <td>: <?=$data_pribadi['alamat_perusahaan'];?></td>
        </tr>
        <tr>
          <td scope="row">23</td>
          <td>Perusahaan Begerak di Bidang</td>
          <td>: <?=$data_pribadi['bidang_perusahaan'];?></td>
        </tr>
        <tr>
          <td scope="row">24</td>
          <td>Minat Utama</td>
          <td>: <?=$data_pribadi['minat'];?></td>
        </tr>
        <tr>
          <td scope="row">25</td>
          <td>Kemampuan Penguasaan Bahasa Inggris</td>
          <td>: 
            <?php 
              echo $data_pribadi['inggris'];
              if($data_pribadi['inggris'] == "A"){
                echo " (Sangat Baik)";
              }elseif($data_pribadi['inggris'] == "B"){
                echo " (Baik)";
              }elseif($data_pribadi['inggris'] == "C"){
                echo " (Kurang Menguasai)";
              }else{
                echo " (Tidak Bisa Sama Sekali)";
              }
            ?>
          </td>
        </tr>
        <tr>
          <td scope="row">26</td>
          <td>Kemampuan Bahasa Asing</td>
          <td>: <?=$data_pribadi['bahasa_asing'];?></td>
        </tr>
        <tr>
          <td scope="row">27</td>
          <td>Bakat dan Talenta</td>
          <td>: <?=$data_pribadi['bakat'];?></td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_pribadi"><i class="far fa-edit"></i> Edit</a>
  </section>
  <section class="bg-light row padding-data flag none">
    <pre>
    <?php //print_r($data_keluarga);?>
    </pre>
    <table class="table table-striped">
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_keluarga['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row" width="5%">28</td>
          <td width="35%">Nama Ayah</td>
          <td width="60%">: <?=$data_keluarga['nama_ayah'];?> (<?=$data_keluarga['umur_ayah'];?> th)</td>
        </tr>
        <tr>
          <td scope="row" width="5%"></td>
          <td width="35%">Nama Ibu</td>
          <td width="60%">: <?=$data_keluarga['nama_ibu'];?> (<?=$data_keluarga['umur_ibu'];?> th)</td>
        </tr>
        <tr>
          <td scope="row">29</td>
          <td>Alamat Orang Tua</td>
          <td>: <?=$data_keluarga['alamat_ortu'];?>, Tlp. <?=$data_keluarga['tlp_ortu'];?></td>
        </tr>
        <tr>
          <td scope="row">30</td>
          <td>Pendidikan Terakhir Ayah</td>
          <td>: <?=$data_keluarga['pend_ayah'];?></td>
        </tr>
        <tr>
          <td scope="row">31</td>
          <td>Pendidikan Terakhir Ibu</td>
          <td>: <?=$data_keluarga['pend_ibu'];?></td>
        </tr>
        <tr>
          <td scope="row">32</td>
          <td>Pekerjaan Ayah</td>
          <td>: <?=$data_keluarga['pekerjaan_ayah'];?></td>
        </tr>
        <tr>
          <td scope="row"></td>
          <td>Pekerjaan Ibu</td>
          <td>: <?=$data_keluarga['pekerjaan_ibu'];?></td>
        </tr>
        <tr>
          <td scope="row">33</td>
          <td>Agama Ayah</td>
          <td>: <?=$data_keluarga['agama_ayah'];?></td>
        </tr>
        <tr>
          <td scope="row"></td>
          <td>Agama Ibu</td>
          <td>: <?=$data_keluarga['agama_ibu'];?></td>
        </tr>
        <tr>
          <td scope="row">34</td>
          <td>Nama Wali</td>
          <td>: <?=$data_keluarga['nama_wali'];?></td>
        </tr>
        <tr>
          <td scope="row">35</td>
          <td>Alamat Wali</td>
          <td>: <?=$data_keluarga['alamat_wali'];?></td>
        </tr>
        <tr>
          <td scope="row">36</td>
          <td>Pekerjaan Wali</td>
          <td>: <?=$data_keluarga['pekerjaan_wali'];?></td>
        </tr>
        <tr>
          <td scope="row"></td>
          <td>Hubungan dengan Wali</td>
          <td>: <?=$data_keluarga['hubungan_wali'];?></td>
        </tr>
        <tr>
          <td scope="row">37</td>
          <td>Agama Wali</td>
          <td>: <?=$data_keluarga['agama_wali'];?></td>
        </tr>
        <tr class="justify-content-center">
          <td scope="row">38</td>
          <td colspan="2">Nama Saudara Kandung (termasuk Anda)
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="35%">Nama Saudara Kandung</th>
                <th width="10%">Kelamin</th>
                <th width="10%">Umur</th>
                <th width="30%">Pendidikan Terakhir</th>
                <th width="10%">Agama</th>
              </tr>
              <?php
                $namaSK = explode("|", $data_keluarga['nama_sk']);
                $jkSK = explode("|", $data_keluarga['jk_sk']);
                $umurSK = explode("|", $data_keluarga['umur_sk']);
                $pendSK = explode("|", $data_keluarga['pend_sk']);
                $agamaSK = explode("|", $data_keluarga['agama_sk']);
                $length = count($namaSK);
                // echo $length;
                for($i=0; $i<$length; $i++){
              ?>
              <tr>
                <td><?=$i+1;?></td>
                <td><?=$namaSK[$i];?></td>
                <td><?=$jkSK[$i];?></td>
                <td><?=$umurSK[$i];?></td>
                <td><?=$pendSK[$i];?></td>
                <td><?=$agamaSK[$i];?></td>
              </tr>
              <?php
                }
                ?>
          </table>
          </td>
        </tr>
        <tr>
          <td scope="row">39</td>
          <td>Apakah Anda Berpacaran/Bertunangan?</td>
          <td>: <?= ($data_keluarga['thn_pcr']) ? "Ya, tahun ".$data_keluarga['thn_pcr'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">40</td>
          <td>Nama Pacar/Tunangan</td>
          <td>: <?=$data_keluarga['nama_pcr'];?></td>
        </tr>
        <tr>
          <td scope="row">41</td>
          <td>Agama Pacar/Tunangan</td>
          <td>: <?=$data_keluarga['agama_pcr'];?></td>
        </tr>
        <tr>
          <td scope="row">42</td>
          <td>Hubungan dengan Pacar/Tunangan Saat ini</td>
          <td>: <?=$data_keluarga['hub_pcr'];?></td>
        </tr>
        <tr>
          <td scope="row">43</td>
          <td>Tanggapan Pacar/Tunangan/Pasangan atas Keputusan Anda masuk Sekolah Teologi</td>
          <td>: <?=$data_keluarga['tanggapan_pcr'];?></td>
        </tr>
        <tr>
          <td scope="row">44</td>
          <td>Apakah Anda Pernah Bercerai?</td>
          <td>: <?= ($data_keluarga['thn_div']) ? "Ya, tahun ".$data_keluarga['thn_div'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">45</td>
          <td>Alasan Bercerai</td>
          <td>: <?=$data_keluarga['alasan_div'];?></td>
        </tr>
        <tr>
          <td scope="row">46</td>
          <td>Apakah Anda Menjadi Penanggung Ekonomi Keluarga?</td>
          <td>: <?= ($data_keluarga['stat_tp'] == 1) ? "Ya" : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">47</td>
          <td>Jika Masuk Sekolah Teologi, Siapakah yang Akan Menjadi Penanggung Ekonomi Keluarga?</td>
          <td>: <?=$data_keluarga['nama_tp'];?></td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_keluarga"><i class="far fa-edit"></i> Edit</a>
  </section>
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_pp);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_pribadi['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr class="justify-content-center">
          <td scope="row" width="5%">48</td>
          <td colspan="2">Riwayat Pendidikan<br>
          <em>Pendidikan Formal</em>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="35%">Pendidikan Formal</th>
                <th width="45%">Nama Sekloah</th>
                <th width="15%">Tahun</th>
              </tr>
              <tr>
                <td>1</td>
                <td>Taman Kanak-Kanak</td>
                <td><?=$data_pp['tk'];?></td>
                <td><?=$data_pp['tk_thn'];?></td>
              </tr>
              <tr>
                <td>2</td>
                <td>SD</td>
                <td><?=$data_pp['sd'];?></td>
                <td><?=$data_pp['sd_thn'];?></td>
              </tr>
              <tr>
                <td>3</td>
                <td>SMP</td>
                <td><?=$data_pp['smp'];?></td>
                <td><?=$data_pp['smp_thn'];?></td>
              </tr>
              <tr>
                <td>4</td>
                <td>SMA/SMK/STM</td>
                <td><?=$data_pp['sma'];?></td>
                <td><?=$data_pp['sma_thn'];?></td>
              </tr>
              <tr>
                <td>5</td>
                <td><?=$data_pp['lain1_nm'];?></td>
                <td><?=$data_pp['lain1'];?></td>
                <td><?=$data_pp['lain1_thn'];?></td>
              </tr>
              <tr>
                <td>6</td>
                <td><?=$data_pp['lain2_nm'];?></td>
                <td><?=$data_pp['lain2'];?></td>
                <td><?=$data_pp['lain2_thn'];?></td>
              </tr>
          </table><br>
          <em>Pendidikan Non-Formal</em>          
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="35%">Pendidikan Formal</th>
                <th width="45%">Nama Sekloah</th>
                <th width="15%">Tahun</th>
              </tr>
              <?php
                $nonfNM = explode("|", $data_pp['nonf_nm']);
                $nonfLembaga = explode("|", $data_pp['nonf_lembaga']);
                $nonfThn = explode("|", $data_pp['nonf_thn']);
                $length = count($nonfNM);
                // echo $length;
                for($i=0; $i<$length; $i++){
              ?>
              <tr>
                <td><?=$i+1;?></td>
                <td><?=$nonfNM[$i];?></td>
                <td><?=$nonfLembaga[$i];?></td>
                <td><?=$nonfThn[$i];?></td>
              </tr>
              <?php
                }
                ?>
          </table>
          </td>
        </tr>
        <tr class="justify-content-center">
          <td scope="row" width="5%">49</td>
          <td colspan="2">Riwayat Pekerjaan<br>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="30%">Jabatan</th>
                <th width="50%">Perusahaan dan Bidangnya</th>
                <th width="15%">Tahun</th>
              </tr>
              <?php
                $nmKerja = explode("|", $data_pp['nm_kerja']);
                $jabKerja = explode("|", $data_pp['jab_kerja']);
                $thnKerja = explode("|", $data_pp['thn_kerja']);
                $length = count($nmKerja);
                // echo $length;
                for($i=0; $i<$length; $i++){
              ?>
              <tr>
                <td><?=$i+1;?></td>
                <td><?=$jabKerja[$i];?></td>
                <td><?=$nmKerja[$i];?></td>
                <td><?=$thnKerja[$i];?></td>
              </tr>
              <?php
                }
                ?>
          </table>
          </td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_pp"><i class="far fa-edit"></i> Edit</a>
  </section>
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_rohani);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_pribadi['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row" width="5%">50</td>
          <td width="35%">Sejak Tahun erapa Anda Menjadi Anggota Gereja?</td>
          <td width="60%">: <?=$data_rohani['anggota_thn'];?></td>
        </tr>
        <tr>
          <td scope="row">51</td>
          <td>Sudah Dibaptis Anak?</td>
          <td>: <?= ($data_rohani['baptisA_thn']) ? "Ya, tahun ".$data_rohani['baptisA_thn'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">52</td>
          <td>Sudah Disidi?</td>
          <td>: <?= ($data_rohani['sidi_thn']) ? "Ya, tahun ".$data_rohani['sidi_thn'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">53</td>
          <td>Sudah Dibaptis Dewasa?</td>
          <td>: <?= ($data_rohani['baptisD_thn']) ? "Ya, tahun ".$data_rohani['baptisD_thn'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">54</td>
          <td>Baptisan Dewasa/Sidi Dilayankan Oleh</td>
          <td>: <?= $data_rohani['sidi_by'];?></td>
        </tr>
        <tr class="justify-content-center">
          <td scope="row" width="5%">55</td>
          <td colspan="2">Pengalaman Pelayanan<br>
          <table class="table table-bordered table-sm">
              <tr class="table-active">
                <th width="5%">No.</th>
                <th width="30%">Jenis Pelayanan</th>
                <th width="50%">Tempat Pelayanan</th>
                <th width="15%">Tahun</th>
              </tr>
              <?php
                $jenisP = explode("|", $data_rohani['jenis_pelayanan']);
                $tempatP = explode("|", $data_rohani['tempat_pelayanan']);
                $thnP = explode("|", $data_rohani['thn_pelayanan']);
                $length = count($jenisP);
                // echo $length;
                for($i=0; $i<$length; $i++){
              ?>
              <tr>
                <td><?=$i+1;?></td>
                <td><?=$jenisP[$i];?></td>
                <td><?=$tempatP[$i];?></td>
                <td><?=$thnP[$i];?></td>
              </tr>
              <?php
                }
                ?>
          </table>
          </td>
        </tr>
        <tr>
          <td scope="row">56</td>
          <td>Hambatan Utama dalam Pertumbuhan Rohani</td>
          <td>: <?= $data_rohani['hambatan_pertumbuhan'];?></td>
        </tr>
        <tr>
          <td scope="row">57</td>
          <td>Hambatan Utama dalam Pelayanan</td>
          <td>: <?= $data_rohani['hambatan_pelayanan'];?></td>
        </tr>
        <tr>
          <td scope="row">58</td>
          <td>Hambatan Utama untuk Masuk Sekolah Teologi</td>
          <td>: <?= $data_rohani['hambatan_masuk'];?></td>
        </tr>
        <tr>
          <td scope="row">59</td>
          <td>Masalah yang Akan Menghambat Kelancaran Studi</td>
          <td>: <?= $data_rohani['masalah'];?></td>
        </tr>
        <tr>
          <td scope="row">60</td>
          <td>Deskripsi Masalah No.59</td>
          <td>: <?= $data_rohani['masalah_detail'];?></td>
        </tr>
        <tr>
          <td scope="row">61</td>
          <td>Rutin Membaca Alkitab?</td>
          <td>: <?= ($data_rohani['jml_baca']) ? "Ya, Sudah Selesai ".$data_rohani['baptisA_thn']." kali" : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">62</td>
          <td>Rutin Membrikan Persembahan Perpuluhan?</td>
          <td>: <?= ($data_rohani['persembahan_stat'] == 1) ? "Ya" : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">63</td>
          <td>Alasan No.62</td>
          <td>: <?=$data_rohani['persembahan_alasan'];?></td>
        </tr>
        <tr>
          <td scope="row">64</td>
          <td>Senang Meminjam Uang atau Barang Pada Orang Lain?</td>
          <td>: <?= ($data_rohani['pinjam_stat'] == 1) ? "Ya" : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">65</td>
          <td>Sedang Menanggung Beban Hutang?</td>
          <td>: <?= ($data_rohani['hutang']) ? "Ya, Sebesar ".$data_rohani['hutang'] : "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">66</td>
          <td>Buku yang Pernah Dibaca dan Sangat Mempengaruhi Hidup</td>
          <td>: <?=$data_rohani['buku'];?></td>
        </tr>
        <tr>
          <td scope="row">67</td>
          <td>Hamba Tuhan yang Paling Berpengaruh dalam Pertumbuhan Rohani</td>
          <td>: <?=$data_rohani['ht_pengaruh'];?></td>
        </tr>
        <tr>
          <td scope="row">68</td>
          <td>Hobby dan Kegemaran</td>
          <td>: <?=$data_rohani['hobby'];?></td>
        </tr>
        <tr>
          <td scope="row">69</td>
          <td>Jenis Olahraga yang Digemari dan Dikuasai</td>
          <td>: <?=$data_rohani['olahraga'];?></td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_rohani"><i class="far fa-edit"></i> Edit</a>
  </section>
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_lain);?>
      </pre>
      <thead>
        <tr>
          <th colspan="3" scope="col" class="text-center">No. Pendaftaran : <?=$data_pribadi['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td scope="row">70</td>
          <td>Harapan untuk Sekolah Teologi</td>
          <td>: <?=$data_lain['harapan'];?></td>
        </tr>
        <tr>
          <td scope="row">71</td>
          <td>Mendapatkan Informasi tentang Sekolah Teologi dari</td>
          <td>: <?=$data_lain['darimana'];?></td>
        </tr>
        <tr>
          <td scope="row">72</td>
          <td>Pernah Mendaftar di Sekolah Teologi Sebelumnya?</td>
          <td>: <?= ($data_lain['thn_daftar_bef']) ? "Ya, Tahun ".$data_lain['thn_daftar_bef'] : "Tidak" ;?></td>          
        </tr>
        <tr>
          <td scope="row">73</td>
          <td>Merupakan Utusan Gereja</td>
          <td>: <?= ($data_lain['utusan_stat'] == 1) ? "Ya": "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">74</td>
          <td>Nama Gereja Pengutus</td>
          <td>: <?= $data_lain['nama_utus'];?></td>
        </tr>
        <tr>
          <td scope="row">75</td>
          <td>Alamat Gereja Pengutus</td>
          <td>: <?= $data_lain['alamat_utus']." Telp. ".$data_lain['tlp_utus'];?></td>
        </tr>
        <tr>
          <td scope="row">76</td>
          <td>Terikat Janji dengan Gerja Pengutus</td>
          <td>: <?= ($data_lain['utus_stat'] == 1) ? "Ya": "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">77</td>
          <td>Di Sekolah Teologi Menanggung Biaya Kuliah Sendiri?</td>
          <td>: <?= ($data_lain['mandiri_stat'] == 1) ? "Ya": "Tidak" ;?></td>
        </tr>
        <tr>
          <td scope="row">78</td>
          <td>Orang yang Mendukung Biaya Kuliah</td>
          <td>: <?= $data_lain['nama_biaya'];?></td>
        </tr>
        <tr>
          <td scope="row">79</td>
          <td>Alamat Rumah Penanggung Biaya</td>
          <td>: <?= $data_lain['alamat_biaya']." Tlp. ".$data_lain['tlp_biaya'];?></td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_lain"><i class="far fa-edit"></i> Edit</a>
  </section>
  
  <section class="bg-light row padding-data flag none">
    <table class="table table-striped">
      <pre>
      <?php //print_r($data_upload);?>
      </pre>
      <thead>
        <tr>
          <th colspan="2" scope="col" class="text-center">No. Pendaftaran : <?=$data_pribadi['no_pendaftaran'];?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td width="25%">File Foto</td>
          <td width="75%">: 
            <?php if($data_upload['file_foto']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_foto']?>" target="_blank"> <?=$data_upload['file_foto']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File KTP</td>
          <td>: 
            <?php if($data_upload['file_ktp']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ktp']?>" target="_blank"> <?=$data_upload['file_ktp']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File Ijazah</td>
          <td>: 
            <?php if($data_upload['file_ijazah']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_ijazah']?>" target="_blank"> <?=$data_upload['file_ijazah']?></a>
            <?php endif;?>
          </td>
        </tr>
        <tr>
          <td>File KK</td>
          <td>: 
            <?php if($data_upload['file_kk']) : ?>
              <a href="<?=base_url()?>assets/userfiles/<?=$_SESSION['no_pendaftaran']?>/<?=$data_upload['file_kk']?>" target="_blank"> <?=$data_upload['file_kk']?></a>
            <?php endif;?>
          </td>
        </tr>
      </tbody>
    </table>
    <a type="button" class="btn btn-info btn-edit-done" href="<?= site_url();?>data_upload"><i class="far fa-edit"></i> Edit</a>
  </section>

</section>
</main>


<script>
  document.getElementById("data_pribadi").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[0]);
    if(e[0].classList.contains('none')){
      e[0].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[0]){
          element.classList.add('none');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          id[5].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  }); 

  document.getElementById("data_keluarga").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[1].classList.contains('none')){
      e[1].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[1]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          id[5].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  }); 

  document.getElementById("data_pp").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(id[6]);
    if(e[2].classList.contains('none')){
      e[2].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[2]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          id[5].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  }); 

  document.getElementById("data_rohani").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[3].classList.contains('none')){
      e[3].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[3]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[4].classList.remove('active');
          id[5].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  }); 

  document.getElementById("data_lain").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[4].classList.contains('none')){
      e[4].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[4]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[5].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  });

  document.getElementById("data_upload").addEventListener("click", function(event){
    event.preventDefault();
    const e = document.querySelectorAll('section.flag');
    const id = document.querySelectorAll('nav div a');
    // console.log(e[1]);
    if(e[5].classList.contains('none')){
      e[5].classList.toggle('none');
      this.classList.toggle('active');
      e.forEach((element) => {
        if (element != e[5]){
          element.classList.add('none');
          // this.classList.remove('active');
          id[0].classList.remove('active');
          id[1].classList.remove('active');
          id[2].classList.remove('active');
          id[3].classList.remove('active');
          id[4].classList.remove('active');
          id[6].classList.remove('active');
        }
      });
    }
  }); 
</script>