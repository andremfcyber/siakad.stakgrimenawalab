    <div class="row padding-tb-30 justify-content-center">
        <div class="col-sm-9">
            <div class="progress">
              <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
            </div>
        </div>
    </div> 
    
  <div class="row padding-tb-30 justify-content-center">
    <section class="col-sm-9">
      <form action="<?php echo site_url();?>savedata/saveDataPp" id="my-form" name="my-form" method="POST">
        <table class="table table-striped table-bordered table-sm table-rounded">
          <tbody>
            <tr>
              <th colspan = "2">C. DATA PENDIDIKAN DAN RIWAYAT PEKERJAAN</th>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm">Riwayat Pendidikan <em>Formal</em></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Pendidikan Formal</th>
                    <th width="40%">Nama Sekolah</th>
                    <th width="20%">Tahun (Masuk - Lulus)</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>Taman Kanak-Kanak</td>
                    <td>
                      <input type="text" id="tk" name="tk" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="tk_thn" name="tk_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>SD</td>
                    <td>
                      <input type="text" id="sd" name="sd" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="sd_thn" name="sd_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>SMP</td>
                    <td>
                      <input type="text" id="smp" name="smp" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="smp_thn" name="smp_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">4</td>
                    <td>SMA/SMK/STM</td>
                    <td>
                      <input type="text" id="sma" name="sma" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="sma_thn" name="sma_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td colspan="4">Lain-Lain</td>
                  </tr>
                  <tr>
                    <td class="text-center">5</td>
                    <td>
                      <input type="text" id="lain1_nm" name="lain1_nm" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="lain1" name="lain1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="lain1_thn" name="lain1_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">6</td>
                    <td>
                      <input type="text" id="lain2_nm" name="lain2_nm" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="lain2" name="lain2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="lain2_thn" name="lain2_thn" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm">Riwayat Pendidikan <em>Non Formal</em></label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Pendidikan Non Formal</th>
                    <th width="40%">Nama Lembaga Penyelenggara</th>
                    <th width="20%">Tahun (Masuk - Lulus)</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>
                      <input type="text" id="nonf_nm1" name="nonf_nm1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_lembaga1" name="nonf_lembaga1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_thn1" name="nonf_thn1" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>
                      <input type="text" id="nonf_nm2" name="nonf_nm2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_lembaga2" name="nonf_lembaga2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_thn2" name="nonf_thn2" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>
                      <input type="text" id="nonf_nm3" name="nonf_nm3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_lembaga3" name="nonf_lembaga3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_thn3" name="nonf_thn3" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">4</td>
                    <td>
                      <input type="text" id="nonf_nm4" name="nonf_nm4" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_lembaga4" name="nonf_lembaga4" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nonf_thn4" name="nonf_thn4" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>
            <tr>
              <td colspan="2">
              <label for="sk" class="col-form-label-sm">Riwayat Pekerjaan</label><br>
                <table class="col-sm-12">
                  <tr class="col-form-label-sm text-center">
                    <th width="10%">No.</th>
                    <th width="30%">Jabatan</th>
                    <th width="40%">Nama Perusahaan dan Bidangnya</th>
                    <th width="20%">Tahun</th>
                  </tr>
                  <tr>
                    <td class="text-center">1</td>
                    <td>
                      <input type="text" id="jab_kerja1" name="jab_kerja1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nm_kerja1" name="nm_kerja1" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="thn_kerja1" name="thn_kerja1" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">2</td>
                    <td>
                      <input type="text" id="jab_kerja2" name="jab_kerja2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nm_kerja2" name="nm_kerja2" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="thn_kerja2" name="thn_kerja2" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">3</td>
                    <td>
                      <input type="text" id="jab_kerja3" name="jab_kerja3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nm_kerja3" name="nm_kerja3" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="thn_kerja3" name="thn_kerja3" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                  <tr>
                    <td class="text-center">4</td>
                    <td>
                      <input type="text" id="jab_kerja4" name="jab_kerja4" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="nm_kerja4" name="nm_kerja4" class="form-control form-control-sm col-12">
                    </td>
                    <td>
                      <input type="text" id="thn_kerja4" name="thn_kerja4" class="form-control form-control-sm col-12">
                    </td>
                  </tr>
                </table>
                <br>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <center>
                  <?php
                    // if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)&&!empty($data_upload)){
                    if(!empty($data_pribadi)&&!empty($data_keluarga)&&!empty($data_pp)&&!empty($data_rohani)&&!empty($data_lain)){
                  ?>
                    <input type="text" name="done" value="1" hidden>
                    <button type="submit" class="btn btn-sm btn-success"><i class="far fa-file-alt"></i> Save dan Lihat Semua Data</a>
                  <?php
                    }else{
                  ?>
                    <a href="<?php echo site_url();?>data_keluarga" class="btn btn-sm btn-info"><i class="fas fa-arrow-alt-circle-left"></i> Kembali</a>
                    <button type="submit" name="simpan" id="simpan" class="btn btn-sm btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Lanjut Ke Step 4</button>
                  <?php
                    }
                  ?>
                </center>
              </td>
            </tr>
          </tbody>
        </table>
        <input type="text" id="noPendaftaran" value="<?=@$this->session->userdata('no_pendaftaran');?>" hidden>
      </form>
    </section>
  </div>
</main>

<script>
  document.addEventListener('DOMContentLoaded', getData());

  function getData(){
    let noPendaftaran = document.getElementById('noPendaftaran').value;
    let params = "no_pendaftaran="+noPendaftaran;

    let xhr = new XMLHttpRequest();
    xhr.open('POST','<?=site_url();?>getdata/get_data_pp',true);
    xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    xhr.onload = function(){
      if(this.status == 200){
        if(this.responseText){
          let r = JSON.parse(this.responseText);
          
          document.querySelector('#tk').value = r.tk;
          document.querySelector('#tk_thn').value = r.tk_thn;
          document.querySelector('#sd').value = r.sd;
          document.querySelector('#sd_thn').value = r.sd_thn;
          document.querySelector('#smp').value = r.smp;
          document.querySelector('#smp_thn').value = r.smp_thn;
          document.querySelector('#sma').value = r.sma;
          document.querySelector('#sma_thn').value = r.sma_thn;
          document.querySelector('#lain1_nm').value = r.lain1_nm;
          document.querySelector('#lain1').value = r.lain1;
          document.querySelector('#lain1_thn').value = r.lain1_thn;
          document.querySelector('#lain2_nm').value = r.lain2_nm;
          document.querySelector('#lain2').value = r.lain2;
          document.querySelector('#lain2_thn').value = r.lain2_thn;
          
          const nonfNm = r.nonf_nm.split("|");
          const nonfLembaga = r.nonf_lembaga.split("|");
          const nonfThn = r.nonf_thn.split("|");
          const nmKerja = r.nm_kerja.split("|");
          const jabKerja = r.jab_kerja.split("|");
          const thnKerja = r.thn_kerja.split("|");
          let i;
          for(i=1; i<=4; i++){
            document.querySelector(`#nonf_nm${i}`).value = nonfNm[i-1];
            document.querySelector(`#nonf_lembaga${i}`).value = nonfLembaga[i-1];
            document.querySelector(`#nonf_thn${i}`).value = nonfThn[i-1];
            document.querySelector(`#nm_kerja${i}`).value = nmKerja[i-1];
            document.querySelector(`#jab_kerja${i}`).value = jabKerja[i-1];
            document.querySelector(`#thn_kerja${i}`).value = thnKerja[i-1];
          }
        }
      }
    }
    xhr.send(params);
  }
</script>