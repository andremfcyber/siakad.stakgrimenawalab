<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PMB STAK Arastamar Grimenawa Jayapura</title>
  <!-- CSS BOOTSTRAP -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
  <!-- CSS CUSTOM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
  <!-- FONT AWESOME -->
 <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  
  <style>
      body {
              background-color: #ddd;
      }
      
      table {
          background: #FFF;
      }
      
      .headers {
          background: #3f51b5;
          padding: 9px;
      }
      .headers h3 {
          margin: 0px;
          font-size: 23px;
          color: #FFF;
      }
      
      .headers p {
          margin: 0px;
           font-size: 20px;
          color: #FFF;
      }
      
      .headers h5 {
        margin-top: 8px;
        font-size: 14px;
      }
      
      .self-center {
          align-self: center;
      }
      
      .headers {box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
          margin-top:7px;
      }
    .table-striped tbody tr:nth-of-type(odd) {
        background-color: rgb(117 159 207 / 5%)!important;
    }
    .table-bordered td, .table-bordered th {
        /*border: 1px solid #f9f8f8!important;*/
        border:none!important;
        padding: 12px;
    }
    .table th {
       background: #3f51b5;
        color: #FFFF;
    }
    
         
    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        color: lightgrey
          text-align: center; 
    }
    
    #progressbar .active {
        color: #3f51b5
    }
    
    #progressbar li {
        list-style-type: none;
        font-size: 15px;
        width: 19%;
        float: left;
        position: relative;
        font-weight: 400
          text-align: center;
    }
    
    #progressbar #account:before {
        font-family: FontAwesome;
        content: "\f13e"
    }
    
    #progressbar #personal:before {
        font-family: FontAwesome;
        content: "\f007"
    }
    
    #progressbar #payment:before {
        font-family: FontAwesome;
        content: "\f030"
    }
    
    #progressbar #confirm:before {
        font-family: FontAwesome;
        content: "\f00c"
    }
    
    #progressbar #done:before {
        font-family: FontAwesome;
        content: "\f00c"
    }
    
    #progressbar li:before {
          text-align: center;
        width: 50px;
        height: 50px;
        line-height: 45px;
        display: block;
        font-size: 20px;
        color: #ffffff;
        background: #a1a1a1;
        border-radius: 50%;
        margin: 0 auto 10px auto;
        padding: 2px
    }
    
    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: #a1a1a1;
        position: absolute;
        left: 0;
        top: 25px;
        z-index: -1
    }
    
    #progressbar li.active:before,
    #progressbar li.active:after {
        background: #3f51b5
    }
    .progress-bars {
        text-align: center;
    }
    .progress {
        
        height: 20px
    }
    
    .progress-bar {
        background-color: #673AB7
    }
    
    .fit-image {
        width: 100%;
        object-fit: cover
    }   
    
    table {
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    }
    
    .ul-custom>li>a {
        margin: 0px;
        font-size: 14px;
        display: block;
        color: white;
        text-align: center;
    }
    
    .padding-tb-30 {
        padding: 0px;
    }
    
    
    @media only screen and (max-width: 768px) {
      /* For mobile phones: */
        .headers h3 {
            margin: 0px;
            font-size: 16px;
            color: #FFF;
        }
         .headers p {
            margin: 0px;
            font-size: 14px;
            color: #FFF;
        }
    }
  </style>
</head>
<body>
  <header >
      <div class="container"> 
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-9 col-12 col-sm-12 col-xs-12 ">
                      <div class="headers"> 
                         <div class="row flex justify-content-center">
                            <div class="col-md-2 col-4 col-sm-4 col-xs-4 text-center self-center ">
                                <img src="<?= base_url();?>../assets/app_setting_upload/img/file_1644480116.png" class="logo-headers" width="100%" style="width: 72%;">
                            </div>
                            <div class="col-md-8 col-8 col-sm-8 col-xs-8 text-left self-center ">
                                <div class="text-white copyright-header">
                                    <h3>SEKOLAH TINGGI AGAMA KRISTEN</h3> 
                                    <p>Arastamar Grimenawa Jayapura</p> 
                                </div>
                            </div>
                            
                            <div class="col-md-2 col-3 col-sm-3 col-xs-3 text-left self-center ">
                                 <ul class="list-unstyled components ul-custom" style="    top: 9px;">
                                    <li>
                                        <img src="<?php echo base_url('assets/img/user.jpg');?>" alt="user">
                                    </li> 
                                    <li>
                                        <h5 class="text-white text-center">
                                            <!--Welcome -->
                                            
                                            <?=$_SESSION['username'];?></h5>
                                    </li> 
                                    <li class="active">
                                        <a href="<?= site_url();?>home/logout">Logout</a>
                                    </li> 
                                </ul>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>    
      </div>
    
  </header>
  <main class="container">
  <h3 class="loginTitle mb-3">Formulir Pendaftaran</h3> 
  
  
  
  
  
  
 
  