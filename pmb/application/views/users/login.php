<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>PMB STAK Arastamar Grimenawa Jayapura</title>
  <!-- CSS BOOTSTRAP -->
  <!--<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">-->
  <!-- CSS CUSTOM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css');?>">
  <!-- FONT AWESOME -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.1/css/bootstrap.min.css" integrity="sha512-T584yQ/tdRR5QwOpfvDfVQUidzfgc2339Lc8uBDtcp/wYu80d7jwBgAxbyMh0a9YM9F8N3tdErpFI8iaGx6x5g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 
  <style>
  
    
    .containerLoginIcon, .bg-login {
      background-image: url(<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->bg_login ?>) !important;
    }

    .title-login {
      text-align: center;
      color: white;
      margin-top: 30px;
      font-size: 16px;
    }

    .nama-kampus-login {
      color: white;
      text-align: center;
      margin-top: 100px;
      font-size: 15px;
      margin-top: 140px;
    }

    .containerLoginIcon img {
      margin-top: -80px;
    }
  
  .bg-navy {
      /*background: #3f51b5;*/
      background-image: linear-gradient(150deg, #3f51b5 32%, #7754af 71%);
  }  
  
   h3 , p {
    color: #FFF; 
}
    
    body {
        overflow-x: hidden;
    }
    
    .shadow {
       box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;
    }
    
    .form-control {
            padding: 19px!important;
    }
    .input-group-text {
        padding-left: 15px!important;
        width: 45px!important;
    }
    
    .nopad {
        padding:0px!important;
    
    }
    
    .nopad-right {
        background-image: url(https://siakad.stakgrimenawa.ac.id/pmb/assets/layourlog1.png);
    }
    
    form { 
        border:none!important;
            padding-right: 100px;
            padding-left: 100px;
    }
    
    form  .form-control {
        background:#e9ecef!important;
        
        border-top-right-radius: 33px!important;
        border-bottom-right-radius: 33px!important;
    }
    
    form .input-group-text {
        border-top-left-radius: 33px!important;
        border-bottom-left-radius: 33px!important; 
    }
    
    .content {
        
    background: #DDD;
    }
    
    .row-flex {
      display: flex;
      flex-wrap: wrap;
    }
    
    .nopad-right {
        background-size: 38vh  74vh;
        background-image: url(https://siakad.stakgrimenawa.ac.id/pmb/assets/layourlog1.png);
        background-repeat: no-repeat; 
        background-color: #FFF;
    }
    
    .card {
        background:none!important;
    }
     
    
    .row-eq-height {
        display: -webkit-box!important;
        display: -webkit-flex!important;
        display: -ms-flexbox!important;
        display: flex!important;
    }
    
    .carousel-item {
       
    background: #7754af;
        height: 70vh;
    }
    
    .center-image {
          display: block;
            margin-left: auto;
            margin-right: auto;
            width: 23%;
            position: relative;
            top: 66px;
    }
    
    .nopad-left {
        padding: 12px!important;
        background: #7754af;
    }
    
    .btn-primary {
    color: #fff;
    background-color: #7754af;
    border-color: #7754af;
}
  </style>
  
  
  
</head>
<body class="bg-logins">
    
    
    
    
    
    <section>
     <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-dark bg-navy">
    <div class="container">
      <a href="#" class="navbar-brand">
        <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" alt="PMB ONLINE UMP" class="brand-image img-circle elevation-3" style="opacity: .8;width:47px;">
        <span class="brand-text font-weight-light">SEKOLAH TINGGI AGAMA KRISTEN   Arastamar Grimenawa Jayapura 
</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="http://online.pmb.ump.ac.id" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="https://siakad.stakgrimenawa.ac.id/pmb/index.php/register" class="nav-link">Pendaftaran Mahasiswa Baru</a>
          </li>
          <!--<li class="nav-item">-->
          <!--  <a href="#" target="_blank" class="nav-link">International Class</a>-->
          <!--</li>-->
          <!--<li class="nav-item">
            <a href="index.php?page=mahad" class="nav-link">Ma'had Imam Malik</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Pendaftaran</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="index.php" class="dropdown-item">Mahasiswa Baru</a></li>
              <li><a href="index.php?page=international" class="dropdown-item">International Student</a></li>
              <li><a href="index.php?page=international" class="dropdown-item">Ma'had Imam Malik</a></li>
            </ul>
          </li>-->
        </ul>
      </div>
    </div>
  </nav>
  <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"  >
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <!--<ol class="breadcrumb float-sm-middle">
              <li class="breadcrumb-item"><a href="#">Selamat Datang Calon Mahasiswa Baru UMP</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>-->
            <center>
              Selamat Datang Calon Mahasiswa Baru SEKOLAH TINGGI AGAMA KRISTEN   Arastamar Grimenawa Jayapura
            </center>
            
            <center>
              <?php if($this->session->flashdata('user_registered')):?>
                <p class="alert alert-success text-center"><?= $this->session->flashdata('user_registered'); ?></p>
              <?php endif;?>
            
              <?php if($this->session->flashdata('login_failed')):?>
                <p class="alert alert-danger text-center"><?= $this->session->flashdata('login_failed'); ?></p>
              <?php endif;?>
            
              <?php if($this->session->flashdata('user_loggedout')):?>
                <p class="alert alert-danger text-center"><?= $this->session->flashdata('user_loggedout'); ?></p>
              <?php endif;?> 
            </center>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="row  ">
          <div class="col-md-12">
            <div class="card" style="background-color: #ddd;">
              <div class="card-body">
                    <!-- Main content -->
                    
                    
                <section class="content">
                  <div class="container-fluid">
                    <div class="row flex row-eq-height justify-content-center">
                        
                      <div class="col-md-5 nopads nopad-left">
                        
                             
                        <!-- Slider -->
                        
                         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item">
                              <img class="d-block w-100" src="https://siakad.stakgrimenawa.ac.id/pmb/assets/loginslid/sd1.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item active">
                              <img class="d-block w-100" src="https://siakad.stakgrimenawa.ac.id/pmb/assets/loginslid/sd2.jpg" alt="Second slide">
                            </div>
                           <div class="carousel-item">
                              <img class="d-block w-100" src="https://siakad.stakgrimenawa.ac.id/pmb/assets/loginslid/sd3.jpg" alt="Third slide">
                            </div> 
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                        
                        <!-- /.nav-tabs-custom -->
                      </div>
                      <!-- /.col -->    
                        
                      <div class="col-md-4 nopad nopad-right">
                       <div class="content-fluid">
                           
                        <!--<div class="card card-warning card-outline bg-navys shadow">-->
                        <!--  <div class="card-body box-profile">-->
                              
                              <img src="https://siakad.stakgrimenawa.ac.id/pmb/assets/layoutv2.png" class="center-image">
                              <h3 class="profile-username text-center">LOGIN</h3>   
                               <!--<h3 class="profile-username text-center">Member's Log in</h3>-->
                                <p class="profile-username text-center">Isi Username dan Password Anda</p>
                                <!--<hr style="color:white">               -->
                               <form action="<?=site_url();?>home/login" class=" " method="POST">
                                <div class="input-group input-group-sm  ">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                                  </div>
                                  <input type="text" class="form-control form-control-lg" id="username" name="username" placeholder="Username" required autofocus>
                                </div>
                                <br>
                                <div class="input-group input-group-sm  ">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
                                  </div>
                                  <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Password" required>
                                </div>
                                <br>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-login btn-sm btn-primary " > <i class="fas fa-sign-in"></i>Log in</button>
                                </div>
                                <br>
                                <!--<p class="text-center margin-t-10">-->
                                <!--  Don't have an account? <a href="<?=site_url();?>register" class="linkRegister">Register Here</a>-->
                                <!--</p>-->
                              </form>
                               <div class="login-texts text-center ">
                                <p class="text-center" style="color: #7f8489;font-size: 12px;">Jika anda lupa password/mengalami masalah <br> silahkan hubungi admin Akademik/PPMB</p>
                              </div>
                              <div class="social-auth-links text-center mb-3">
                                <!--<p>- Atau -</p>-->
                                <!--<a type="button" class="btn btn-xs  btn-warning" href="https://siakad.stakgrimenawa.ac.id/pmb/index.php/register" > <i class="fas fa-user mr-2"></i>&nbsp;&nbsp;Belum Punya Akun? Klik Disini</a>-->
                                <!--<button type="button" class="btn  btn-warning" data-toggle="modal" data-target="#modal-xl"> <i class="fas fa-user mr-2"></i>&nbsp;&nbsp;Belum Punya Akun? Klik Disini</button>
                                <a href="#" class="btn btn-block btn-danger">
                                  <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                                </a>-->
                                </div>
                                <!--<p class="mb-0">-->
                                <!--  </p><center><a href="#" class="btn btn-primary" target="_blank"><i class="fas fa-download mr-2"></i>&nbsp;&nbsp;Buku Panduan PMB Online</a></center>-->
                                <!--<p>-->
                                    
                                <!--</p>-->
                          </div>
                        <!--</div>-->
                        <!--</div>-->
                      </div>
                      <!-- /.col -->
                 
                    </div>
                    <!-- /.row -->
                  </div><!-- /.container-fluid -->
                </section>
                <!-- /.content -->
              </div>
            </div>
          </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <!--<aside class="control-sidebar control-sidebar-dark"> -->
  <!--  <div class="p-3">-->
  <!--    <h5>Title</h5>-->
  <!--    <p>Sidebar content</p>-->
  <!--  </div>-->
  <!--</aside>-->
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer bg-navy">
    <!-- To the right -->
    <!--<div class="float-right d-none d-sm-inline">-->
    <!--  Version 2.0.0-->
    <!--</div>-->
    <!-- Default to the left -->
    <p style=" padding: 12px;"><small><strong>Copyright © 2022 || Penerimaan Mahasiswa Baru SEKOLAH TINGGI AGAMA KRISTEN   Arastamar Grimenawa Jayapura</strong></small></p>
  </footer>
</div>
    </section>
    
    
    
    
    
    
    
    
    
    
    
<?php /***
    
    
  <!-- <header class="h-peserta clearfix">
    <img src="<?= base_url();?>assets/img/logo.png" class="logo-header">
    <p class="text-white copyright-header">SEKOLAH TINGGI THEOLOGIA INJILI ARASTAMAR (SETIA) JAKARTA</p>
  </header> -->
  <?php if($this->session->flashdata('user_registered')):?>
    <p class="alert alert-success text-center"><?= $this->session->flashdata('user_registered'); ?></p>
  <?php endif;?>

  <?php if($this->session->flashdata('login_failed')):?>
    <p class="alert alert-danger text-center"><?= $this->session->flashdata('login_failed'); ?></p>
  <?php endif;?>

  <?php if($this->session->flashdata('user_loggedout')):?>
    <p class="alert alert-danger text-center"><?= $this->session->flashdata('user_loggedout'); ?></p>
  <?php endif;?>
  
  <main class="containerLogin">
    <div class="containerLoginIcon">
        <h4 class="title-login">Formulir Pendaftaran Online</h4>
        <img src="<?php echo "/assets/app_setting_upload/img/".app_setting()['data_setting'][0]->logo ?>" class="imgLoginLogo">
        <h5 class="nama-kampus-login"><?php echo app_setting()['data_setting'][0]->nama_pendek ?> &nbsp; 
								<?php echo app_setting()['data_setting'][0]->nama_instansi ?></h5><br>
    </div>
    <section class="row justify-content-center login">
      <div class="login-text">
        <h3>Member's Log in</h3>
        <p>Isi Username dan Password Anda</p>
      </div>
      <form action="<?=site_url();?>home/login" class="col-sm-10" method="POST">
        <div class="input-group input-group-sm mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-user"></i></div>
          </div>
          <input type="text" class="form-control" id="username" name="username" placeholder="Username" required autofocus>
        </div>
        
        <div class="input-group input-group-sm mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-unlock-alt"></i></div>
          </div>
          <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
        </div>
        <br>
        <button type="submit" class="btn btn-login btn-sm" >Log in</button>
        <p class="text-center margin-t-10">
          Don't have an account? <a href="<?=site_url();?>register" class="linkRegister">Register Here</a>
        </p>
      </form>
      <div class="login-text">
        <p class="text-center">Jika anda lupa password/mengalami masalah silahkan hubungi admin Akademik/PPMB</p>
      </div>
    </section>
  </main>
  
  
  
  ****/ ?>
  
  
   
  
  
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.1/js/bootstrap.min.js" integrity="sha512-UR25UO94eTnCVwjbXozyeVd6ZqpaAE9naiEUBK/A+QDbfSTQFhPGj5lOR6d8tsgbBk84Ggb5A3EkjsOgPRPcKA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  
  
</body>
</html>
