<?php 

    if (!function_exists('app_setting')) {
        function app_setting() {
            $CI = get_instance();
            $CI->load->model('Model_appsetting');
            $d['data_setting'] = $CI->Model_appsetting->get_setting()->result();
            return $d;
        }
    }

?>