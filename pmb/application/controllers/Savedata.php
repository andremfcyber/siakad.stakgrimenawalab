<?php
class Savedata extends CI_Controller{
  /*
   * Programmer : Fitria Wahyuni.S.Pd
   * Keterangan : Controller khusus untuk transakasi penyimpanan data
   */

  public function saveDataPribadi(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $minat = '';
      if(empty($this->input->post('minat'))){
        $minat = $this->input->post('minat11');
      }else{
        $minat = $this->input->post('minat');
      }
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'nama' => $this->input->post('nama'),
        'jk' => $this->input->post('jk'),
        'prodi' => $this->input->post('prodi'),
        'tempat_lahir' => $this->input->post('tempat_lahir'),
        'tgl_lahir' => $this->input->post('tgl_lahir'),
        'kewarganegaraan' => $this->input->post('kewarganegaraan'),
        'alamat' => $this->input->post('alamat'),
        'tlp' => $this->input->post('tlp'),
        'alamat_saudara' => $this->input->post('alamat_saudara'),
        'tlp_saudara' => $this->input->post('tlp_saudara'),
        'pend_terakhir' => $this->input->post('pend_terakhir'),
        'asal_sekolah' => $this->input->post('asal_sekolah'),
        'thn_lulus' => $this->input->post('thn_lulus'),
        'alamat_sekolah' => $this->input->post('alamat_sekolah'),
        'tinggal_dirumah' => $this->input->post('tinggal_dirumah'),
        'marital_stat' => $this->input->post('marital_stat'),
        'jml_anak_L' => ($this->input->post('jml_anak_L') ? $this->input->post('jml_anak_L') : 0),
        'jml_anak_P' => ($this->input->post('jml_anak_P') ? $this->input->post('jml_anak_P') : 0),
        'gereja_anggota' => $this->input->post('gereja_anggota'),
        'alamat_gereja_anggota' => $this->input->post('alamat_gereja_anggota'),
        'ibadah_melayani_gereja' => $this->input->post('ibadah_melayani_gereja'),
        'gereja_ibadah' => $this->input->post('gereja_ibadah'),
        'alamat_gereja_ibadah' => $this->input->post('alamat_gereja_ibadah'),
        'gereja_pelayanan' => $this->input->post('gereja_pelayanan'),
        'pekerjaan' => $this->input->post('pekerjaan'),
        'nama_perusahaan' => $this->input->post('nama_perusahaan'),
        'alamat_perusahaan' => $this->input->post('alamat_perusahaan'),
        'bidang_perusahaan' => $this->input->post('bidang_perusahaan'),
        'minat' => $minat,
        'inggris' => $this->input->post('inggris'),
        'bahasa_asing' => $this->input->post('bahasa_asing'),
        'bakat' => $this->input->post('bakat'),
        'nama_pasangan' => @$this->input->post('nama_pasangan'),
        'tempat_lahir_pasangan' => @$this->input->post('tempat_lahir_pasangan'),
        'tgl_lahir_pasangan' => @$this->input->post('tgl_lahir_pasangan')
      );
      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_pribadi', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_pribadi($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_pribadi($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_keluarga');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_pribadi');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }
  
  public function saveDataKeluarga(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";exit;
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $namaSK = [];
      $jkSK = [];
      $umurSK = [];
      $pendSK = [];
      $agamaSK = [];
      $jmlSaudara = $this->input->post('jml_saudara');
      $index = 0;
      for($i=0; $i<$jmlSaudara; $i++){
        $n = ($this->input->post('nama_sk'.($i+1)) ?: '-');
        $j = ($this->input->post('jk_sk'.($i+1)) ?: '-');
        $u = ($this->input->post('umur_sk'.($i+1)) ?: '-');
        $p = ($this->input->post('pend_sk'.($i+1)) ?: '-');
        $a = ($this->input->post('agama_sk'.($i+1)) ?: '-');
        // echo $n."|".$j."|".$u."|".$p."|".$a."<br>";
        if($n != '-'){
          $namaSK[$index] = $n;
          $jkSK[$index] = $j;
          $umurSK[$index] = $u;
          $pendSK[$index] = $p;
          $agamaSK[$index] = $a;
          $index++;
        }
      }
      // print_r($namaSK);
      // exit;
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'nama_ayah' => $this->input->post('nama_ayah'),
        'umur_ayah' => ($this->input->post('umur_ayah')? $this->input->post('umur_ayah') : 0 ),
        'nama_ibu' => $this->input->post('nama_ibu'),
        'umur_ibu' => ($this->input->post('umur_ibu') ? $this->input->post('umur_ibu'):0),
        'alamat_ortu' => $this->input->post('alamat_ortu'),
        'tlp_ortu' => $this->input->post('tlp_ortu'),
        'pend_ayah' => $this->input->post('pend_ayah'),
        'pend_ibu' => $this->input->post('pend_ibu'),
        'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
        'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
        'agama_ayah' => $this->input->post('agama_ayah'),
        'agama_ibu' => $this->input->post('agama_ibu'),
        'nama_wali' => $this->input->post('nama_wali'),
        'alamat_wali' => $this->input->post('alamat_wali'),
        'tlp_wali' => $this->input->post('tlp_wali'),
        'pekerjaan_wali' => $this->input->post('pekerjaan_wali'),
        'hubungan_wali' => $this->input->post('hubungan_wali'),
        'agama_wali' => $this->input->post('agama_wali'),
        'nama_sk' => ($namaSK ? implode('|',$namaSK) : ""),
        'jk_sk' => ($namaSK ? implode('|',$jkSK) : ""),
        'umur_sk' => ($namaSK ? implode('|',$umurSK) : ""),
        'pend_sk' => ($namaSK ? implode('|',$pendSK) : ""),
        'agama_sk' => ($namaSK ? implode('|',$agamaSK) : ""),
        'thn_pcr' => $this->input->post('thn_pcr'),
        'nama_pcr' => $this->input->post('nama_pcr'),
        'agama_pcr' => $this->input->post('agama_pcr'),
        'hub_pcr' => $this->input->post('hub_pcr'),
        'tanggapan_pcr' => $this->input->post('tanggapan_pcr'),
        'thn_div' => $this->input->post('thn_div'),
        'alasan_div' => $this->input->post('alasan_div'),
        'stat_tp' => $this->input->post('stat_tp'),
        'nama_tp' => @$this->input->post('nama_tp')
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;
      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_keluarga', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_keluarga($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_keluarga($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_pp');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_keluarga');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }

  public function saveDataPp(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $nonfNm = [];
      $nonfLembaga = [];
      $nonfThn = [];
      $nmKerja = [];
      $jabKerja = [];
      $thnKerja = [];
      for($i=0; $i<5; $i++){
        $nonfNm[$i] = ($this->input->post('nonf_nm'.($i+1)) ?: '-');
        $nonfLembaga[$i] = ($this->input->post('nonf_lembaga'.($i+1)) ?: '-');
        $nonfThn[$i] = ($this->input->post('nonf_thn'.($i+1)) ?: '-');
        $nmKerja[$i] = ($this->input->post('nm_kerja'.($i+1)) ?: '-');
        $jabKerja[$i] = ($this->input->post('jab_kerja'.($i+1)) ?: '-');
        $thnKerja[$i] = ($this->input->post('thn_kerja'.($i+1)) ?: '-');
      }
      // print_r($namaSK);
      // exit;
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'tk' => $this->input->post('tk'),
        'tk_thn' => $this->input->post('tk_thn'),
        'sd' => $this->input->post('sd'),
        'sd_thn' => $this->input->post('sd_thn'),
        'smp' => $this->input->post('smp'),
        'smp_thn' => $this->input->post('smp_thn'),
        'sma' => $this->input->post('sma'),
        'sma_thn' => $this->input->post('sma_thn'),
        'lain1_nm' => $this->input->post('lain1_nm'),
        'lain1' => $this->input->post('lain1'),
        'lain1_thn' => $this->input->post('lain1_thn'),
        'lain2_nm' => $this->input->post('lain2_nm'),
        'lain2' => $this->input->post('lain2'),
        'lain2_thn' => $this->input->post('lain2_thn'),
        'nonf_nm' => ($nonfNm[0] ? implode('|',$nonfNm) : ""),
        'nonf_lembaga' => ($nonfLembaga[0] ? implode('|',$nonfLembaga) : ""),
        'nonf_thn' => ($nonfThn[0] ? implode('|',$nonfThn) : ""),
        'nm_kerja' => ($nmKerja[0] ? implode('|',$nmKerja) : ""),
        'jab_kerja' => ($jabKerja[0] ? implode('|',$jabKerja) : ""),
        'thn_kerja' => ($thnKerja[0] ? implode('|',$thnKerja) : "")
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;
      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_pp', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_pp($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_pp($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_rohani');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_pp');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }

  }

  public function saveDataRohani(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $jenisP = [];
      $tempatP = [];
      $thnP = [];
      $index = 0;
      $jmlPelayanan = $this->input->post('jml_pelayanan');
      for($i=0; $i<$jmlPelayanan; $i++){
        $jp = ($this->input->post('jenis_pelayanan'.($i+1)) ?: '-');
        $tmp = ($this->input->post('tempat_pelayanan'.($i+1)) ?: '-');
        $thp = ($this->input->post('thn_pelayanan'.($i+1)) ?: '-');
        // echo $jp."|".$tmp."|".$thp."<br>";exit;

        if($jp != '-'){
          $jenisP[$index] = $jp;
          $tempatP[$index] = $tmp;
          $thnP[$index] = $thp;
        
          $index++;
        }
      }
      // print_r($jenisP);
      // exit;
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'anggota_thn' => $this->input->post('anggota_thn'),
        'baptisA_thn' => $this->input->post('baptisA_thn'),
        'sidi_thn' => $this->input->post('sidi_thn'),
        'baptisD_thn' => $this->input->post('baptisD_thn'),
        'sidi_by' => $this->input->post('sidi_by'),
        'jenis_pelayanan' => ($jenisP ? implode('|',$jenisP) : ""),
        'tempat_pelayanan' => ($jenisP ? implode('|',$tempatP) : ""),
        'thn_pelayanan' => ($jenisP ? implode('|',$thnP) : ""),
        'hambatan_pertumbuhan' => $this->input->post('hambatan_pertumbuhan'),
        'hambatan_pelayanan' => $this->input->post('hambatan_pelayanan'),
        'hambatan_masuk' => $this->input->post('hambatan_masuk'),
        'masalah' => $this->input->post('masalah'),
        'masalah_detail' => $this->input->post('masalah_detail'),
        'jml_baca' => ($this->input->post('jml_baca')?$this->input->post('jml_baca'):0),
        'persembahan_stat' => ($this->input->post('persembahan_stat')?$this->input->post('persembahan_stat'):0),
        'persembahan_alasan' => $this->input->post('persembahan_alasan'),
        'pinjam_stat' => ($this->input->post('pinjam_stat')? $this->input->post('pinjam_stat'):0),
        'hutang' => ($this->input->post('hutang') ? $this->input->post('hutang') : 0),
        'buku' => $this->input->post('buku'),
        'ht_pengaruh' => $this->input->post('ht_pengaruh'),
        'hobby' => $this->input->post('hobby'),
        'olahraga' => $this->input->post('olahraga')
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_rohani', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_rohani($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_rohani($data);
      }

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_lain');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_rohani');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }

  public function saveDataLain(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      $data = array(
        'no_pendaftaran' => $noPendaftaran,
        'harapan' => $this->input->post('harapan'),
        'darimana' => $this->input->post('darimana'),
        'thn_daftar_bef' => ($this->input->post('thn_daftar_bef') ? $this->input->post('thn_daftar_bef') : 0),
        'utusan_stat' => ($this->input->post('utusan_stat')?$this->input->post('utusan_stat'):0),
        'nama_utus' => $this->input->post('nama_utus'),
        'alamat_utus' => $this->input->post('alamat_utus'),
        'tlp_utus' => $this->input->post('tlp_utus'),
        'utus_stat' => ($this->input->post('utus_stat')?$this->input->post('utus_stat'):0),
        'mandiri_stat' => ($this->input->post('mandiri_stat')?$this->input->post('mandiri_stat'):0),
        'nama_biaya' => $this->input->post('nama_biaya'),
        'alamat_biaya' => $this->input->post('alamat_biaya'),
        'tlp_biaya' => $this->input->post('tlp_biaya')
      );
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;

      // Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_lain', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_lain($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_lain($data);
      }

      // Update status done di user biar bisa diliat admin
      // $this->model_saveData->update_done($noPendaftaran);

      if($result){
        if(($this->input->post('done')!== null)){
          redirect('done');
        }else{
          redirect('data_upload');
        }
      }else{
        $message = "Error! Terjadi Kesalahan Saat Input Data";
        redirect('data_upload');
        echo "<script type='text/javascript'>alert('$message');</script>";
      }
    }else{
      redirect('login');
    }
    
  }

  public function saveDataUpload(){
    // echo "<pre>";
    // print_r($this->input->post());
    // echo "</pre>";
    $logged_in = $this->session->userdata('logged_in');
    $noPendaftaran = $this->session->userdata('no_pendaftaran');
    if(!empty($logged_in)){
      if (!is_dir('./assets/userfiles/'.$noPendaftaran)) {
        mkdir('./assets/userfiles/'.$noPendaftaran, 0777, true);
      }
      $config['upload_path'] = './assets/userfiles/'.$noPendaftaran;//configurasi upload path si image yg dipost di create
      $config['allowed_types'] = 'gif|jpg|png|pdf';
      $config['max_size'] = '2048000';
      // $config['max_width'] = '2000';
      // $config['max_height'] = '2000';
      // echo '<pre>';
      // print_r($_FILES);
      // echo '</pre>';exit;
      // echo $config['upload_path'];exit;
      $file_exist = $this->model_data->data_upload($noPendaftaran);
      if(isset($_FILES['file_foto']['name']) && $_FILES['file_foto']['error']==0){
          $file_foto_temp = $_FILES['file_foto']['name'];
          $file_foto = str_replace(' ', '_', $file_foto_temp); 
          $config['file_name'] = $file_foto;

          // $data['file_foto'] = $file_foto;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_foto')){
            // echo $this->upload->display_errors();exit;
            if(!empty($file_exist['file_foto']) || ($file_exist['file_foto'])!=null){
              $data['file_foto'] = $file_exist['file_foto'];
            }else{
              $data['file_foto'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_foto'] = $file_foto;
            // $this->upload->display_errors();exit;

          }
        }
        
        if(isset($_FILES['file_ktp']['name']) && $_FILES['file_ktp']['error']==0){
          $file_ktp_temp = $_FILES['file_ktp']['name'];
          $file_ktp = str_replace(' ', '_', $file_ktp_temp); 
          $config['file_name'] = $file_ktp;

          // $data['file_ktp'] = $file_ktp;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_ktp')){
            if(!empty($file_exist['file_ktp']) || ($file_exist['file_ktp'])!=null){
              $data['file_ktp'] = $file_exist['file_ktp'];
            }else{
              $data['file_ktp'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_ktp'] = $file_ktp;
          }
        }

        if(isset($_FILES['file_ijazah']['name']) && $_FILES['file_ijazah']['error']==0){
          $file_ijazah_temp = $_FILES['file_ijazah']['name'];
          $file_ijazah = str_replace(' ', '_', $file_ijazah_temp); 
          $config['file_name'] = $file_ijazah;

          // $data['file_ijazah'] = $file_ijazah;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_ijazah')){
            if(!empty($file_exist['file_ijazah']) || ($file_exist['file_ijazah'])!=null){
              $data['file_ijazah'] = $file_exist['file_ijazah'];
            }else{
              $data['file_ijazah'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_ijazah'] = $file_ijazah;
          }
        }

        if(isset($_FILES['file_kk']['name']) && $_FILES['file_kk']['error']==0){
          $file_kk_temp = $_FILES['file_kk']['name'];
          $file_kk = str_replace(' ', '_', $file_kk_temp); 
          $config['file_name'] = $file_kk;

          // $data['file_kk'] = $file_kk;
          $this->load->library('upload');
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('file_kk')){
            if(!empty($file_exist['file_kk']) || ($file_exist['file_kk'])!=null){
              $data['file_kk'] = $file_exist['file_kk'];
            }else{
              $data['file_kk'] = NULL;
            }
          }else{
            // $data = array('upload_data' => $this->upload->data());
            $data['file_kk'] = $file_kk;
          }
        }
        $data['no_pendaftaran'] = $noPendaftaran;
      // echo "<pre>";
      // print_r($data);
      // echo "</pre>";exit;

      //Check udah ada datanya atau belom
      $q = $this->db->get_where('pmb.data_upload', array('no_pendaftaran' => $noPendaftaran));
      $row = $q->num_rows();
      if($row > 0){
        $data['tgl_update'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->update_data_upload($data);      
      }else{
        $data['tgl_insert'] = date("Y-m-d H:i:s");
        $result = $this->model_saveData->save_data_upload($data);
      }

      // Update status done di user biar bisa diliat admin
      $this->model_saveData->update_done($noPendaftaran);

    //   if($result){
    //     if(($this->input->post('done')!== null)){
    //       redirect('done');
    //     }else{
    //       redirect('data_upload');
    //     }
    //   }else{
    //     $message = "Error! Terjadi Kesalahan Saat Input Data";
    //     redirect('data_lain');
    //     echo "<script type='text/javascript'>alert('$message');</script>";
    //   }
    // }else{
    //   redirect('login');
    }

      redirect('done');
    
  }
}

?>